var v=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(e,o,n)=>o in e?v(e,o,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[o]=n,p=(e,o)=>{for(var n in o||(o={}))u.call(o,n)&&f(e,n,o[n]);if(d)for(var n of d(o))b.call(o,n)&&f(e,n,o[n]);return e};import{i as x}from"./getStyleSheets.eefadf42.js";import{i as g,k as y,t as I,p as S,l as L,r as s,e as r,f as i,m as t,F as $,s as k,q as j,w as B}from"./index.b9e7554b.js";import"./vendor.7fd510b4.js";var c={name:"pagesAwesome",setup(){const e=g({sheetsIconList:[]}),o=()=>{x.awe().then(n=>e.sheetsIconList=n)};return y(()=>{o()}),p({},I(e))}},J=`.awesome-container .iconfont-row[data-v-d2f15e60] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.awesome-container .iconfont-row .el-col[data-v-d2f15e60]:nth-child(-n+24) {
  display: none;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-d2f15e60] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-d2f15e60] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-d2f15e60] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-d2f15e60] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-d2f15e60] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=B();S("data-v-d2f15e60");const F={class:"awesome-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},A={class:"iconfont-warp-value"},C={class:"iconfont-warp-label mt10"};L();const D=a((e,o,n,G,M,N)=>{const w=s("el-col"),_=s("el-row"),m=s("el-card");return r(),i("div",F,[t(m,{shadow:"hover",header:`fontawesome \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-24}\u4E2A`},{default:a(()=>[t(_,{class:"iconfont-row"},{default:a(()=>[(r(!0),i($,null,k(e.sheetsIconList,(l,h)=>(r(),i(w,{xs:12,sm:8,md:6,lg:4,xl:2,key:h},{default:a(()=>[t("div",q,[t("div",z,[t("div",A,[t("i",{class:[l,"fa"]},null,2)]),t("div",C,j(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});c.render=D,c.__scopeId="data-v-d2f15e60";export default c;
