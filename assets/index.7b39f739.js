import{u as m,c as p,p as _,l as u,r as o,e as r,f as n,m as f,h as l,w as h}from"./index.8eeb5c9c.js";import w from"./index.3f24dc7c.js";import v from"./tagsView.7acca43e.js";import"./vendor.f1659326.js";import"./breadcrumb.e4877893.js";import"./user.2a6f33c5.js";import"./screenfull.25d0aef1.js";import"./userNews.7e28d3c6.js";import"./search.f2139585.js";import"./index.b0ea54a9.js";import"./horizontal.22892b9f.js";import"./subItem.b7bee423.js";import"./contextmenu.22a0dd17.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:a,isTagsview:e}=t.state.themeConfig.themeConfig;return a!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=h("data-v-fde5c75e");_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=b((t,c,a,e,g,I)=>{const i=o("BreadcrumbIndex"),d=o("TagsView");return r(),n("div",j,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
