var o=Object.assign;import{i as s,b1 as n,t as r,p as i,l as d,P as l,S as c,e as f,f as p,w as u,m as t}from"./index.51953939.js";import"./vendor.8f964570.js";var a={name:"layoutFooter",setup(){const e=s({isDelayFooter:!0});return n(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),o({},r(e))}},$=`.layout-footer[data-v-05adff24] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-05adff24] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const _=u("data-v-05adff24");i("data-v-05adff24");const v={class:"layout-footer mt15"},y=t("div",{class:"layout-footer-warp"},[t("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F"),t("div",{class:"mt5"},"Copyright \u6DF1\u5733\u5E02 xxx \u8F6F\u4EF6\u79D1\u6280\u6709\u9650\u516C\u53F8")],-1);d();const m=_((e,h,x,w,g,D)=>l((f(),p("div",v,[y],512)),[[c,e.isDelayFooter]]));a.render=m,a.__scopeId="data-v-05adff24";export default a;
