var l=Object.assign;import{i as h}from"./getStyleSheets.d3c45326.js";import{i as w,k as b,t as m,p as u,l as x,r as t,e as a,f as s,m as o,F as g,s as I,q as y,w as S}from"./index.0859f0ef.js";import"./vendor.8f964570.js";var i={name:"iconfont",setup(){const n=w({sheetsIconList:[]}),r=()=>{h.ali().then(c=>n.sheetsIconList=c)};return b(()=>{r()}),l({},m(n))}},R=`.iconfont-container .iconfont-row[data-v-f1d6ebf8] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.iconfont-container .iconfont-row .iconfont-warp[data-v-f1d6ebf8] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.iconfont-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-f1d6ebf8] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-f1d6ebf8] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-f1d6ebf8] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-f1d6ebf8] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const e=S("data-v-f1d6ebf8");u("data-v-f1d6ebf8");const L={class:"iconfont-container"},$={class:"iconfont-warp"},k={class:"flex-margin"},j={class:"iconfont-warp-value"},B={class:"iconfont-warp-label mt10"};x();const F=e((n,r,c,q,z,C)=>{const d=t("el-col"),p=t("el-row"),_=t("el-card");return a(),s("div",L,[o(_,{shadow:"hover",header:`iconfont \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${n.sheetsIconList.length}\u4E2A`},{default:e(()=>[o(p,{class:"iconfont-row"},{default:e(()=>[(a(!0),s(g,null,I(n.sheetsIconList,(f,v)=>(a(),s(d,{xs:12,sm:8,md:6,lg:4,xl:2,key:v},{default:e(()=>[o("div",$,[o("div",k,[o("div",j,[o("i",{class:[f,"iconfont"]},null,2)]),o("div",B,y(f),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});i.render=F,i.__scopeId="data-v-f1d6ebf8";export default i;
