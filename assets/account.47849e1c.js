var F=Object.defineProperty;var y=Object.prototype.hasOwnProperty;var b=Object.getOwnPropertySymbols,k=Object.prototype.propertyIsEnumerable;var I=(e,n,t)=>n in e?F(e,n,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[n]=t,v=(e,n)=>{for(var t in n||(n={}))y.call(n,t)&&I(e,t,n[t]);if(b)for(var t of b(n))k.call(n,t)&&I(e,t,n[t]);return e};import{x as S,y as V,D as $,U as L,a as A,z as B,t as C,s as N,p as P,d as T,e as u,f as R,h as j,i as o,l as U,q}from"./vendor.9b51066d.js";import{u as D,b as w,L as H,M as z,f as G}from"./index.3bc43889.js";var h=S({name:"login",setup(){const{t:e}=V(),n=D(),t=$(),p=L(),r=A({ruleForm:{userName:"admin",password:"123456",code:"1234"},loading:{signIn:!1}}),f=B(()=>G(new Date)),d=async()=>{r.loading.signIn=!0;let a=[],l=[],m=["admin"],g=["btn.add","btn.del","btn.edit","btn.link"],c=["test"],x=["btn.add","btn.link"];r.ruleForm.userName==="admin"?(a=m,l=g):(a=c,l=x);const _={userName:r.ruleForm.userName,photo:r.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:a,authBtnList:l};w("token",Math.random().toString(36).substr(0)),w("userInfo",_),n.dispatch("userInfos/setUserInfos",_),n.state.themeConfig.themeConfig.isRequestRoutes?(await z(),i()):(await H(),i())},i=()=>{var l;let a=f.value;((l=t.query)==null?void 0:l.redirect)?p.push(t.query.redirect):p.push("/"),setTimeout(()=>{r.loading.signIn=!0;const m=e("message.signInText");N.success(`${a}\uFF0C${m}`)},300)};return v({currentTime:f,onSignIn:d},C(r))}}),E=`.login-content-form[data-v-18fdc2f2] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-18fdc2f2] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-18fdc2f2] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
  user-select: none;
}
.login-content-form .login-content-code .login-content-code-img[data-v-18fdc2f2]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-18fdc2f2] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const s=q();P("data-v-18fdc2f2");const K=o("div",{class:"login-content-code"},[o("span",{class:"login-content-code-img"},"1234")],-1);T();const M=s((e,n,t,p,r,f)=>{const d=u("el-input"),i=u("el-form-item"),a=u("el-col"),l=u("el-row"),m=u("el-button"),g=u("el-form");return R(),j(g,{class:"login-content-form"},{default:s(()=>[o(i,null,{default:s(()=>[o(d,{type:"text",placeholder:e.$t("message.account.accountPlaceholder1"),"prefix-icon":"el-icon-user",modelValue:e.ruleForm.userName,"onUpdate:modelValue":n[1]||(n[1]=c=>e.ruleForm.userName=c),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),o(i,null,{default:s(()=>[o(d,{type:"password",placeholder:e.$t("message.account.accountPlaceholder2"),"prefix-icon":"el-icon-lock",modelValue:e.ruleForm.password,"onUpdate:modelValue":n[2]||(n[2]=c=>e.ruleForm.password=c),autocomplete:"off","show-password":""},null,8,["placeholder","modelValue"])]),_:1}),o(i,null,{default:s(()=>[o(l,{gutter:15},{default:s(()=>[o(a,{span:16},{default:s(()=>[o(d,{type:"text",maxlength:"4",placeholder:e.$t("message.account.accountPlaceholder3"),"prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":n[3]||(n[3]=c=>e.ruleForm.code=c),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),o(a,{span:8},{default:s(()=>[K]),_:1})]),_:1})]),_:1}),o(i,null,{default:s(()=>[o(m,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:s(()=>[o("span",null,U(e.$t("message.account.accountBtnText")),1)]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});h.render=M,h.__scopeId="data-v-18fdc2f2";export default h;
