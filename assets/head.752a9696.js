var i=Object.assign;import{i as r,o as s,a as d,t as p,aD as o,p as l,l as c,e as m,f as u,m as e,q as v,w as f}from"./index.a57f978d.js";import"./vendor.8f964570.js";var a={name:"chartHead",setup(){const t=r({time:{txt:"",fun:0}}),n=()=>{t.time.txt=o(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ"),t.time.fun=window.setInterval(()=>{t.time.txt=o(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ")},1e3)};return s(()=>{n()}),d(()=>{window.clearInterval(t.time.fun)}),i({},p(t))}},D=`.big-data-up[data-v-692a7ee7] {
  height: 55px;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 15px;
  color: var(--color-primary);
  overflow: hidden;
  position: relative;
}
.big-data-up .up-left[data-v-692a7ee7] {
  position: absolute;
}
.big-data-up .up-center[data-v-692a7ee7] {
  width: 100%;
  display: flex;
  justify-content: center;
  font-size: 18px;
  letter-spacing: 5px;
  background-image: -webkit-linear-gradient(left, var(--color-primary), var(--color-primary-light-1) 25%, var(--color-primary) 50%, var(--color-primary-light-1) 75%, var(--color-primary));
  -webkit-text-fill-color: transparent;
  -webkit-background-clip: text;
  background-clip: text;
  background-size: 200% 100%;
  -webkit-animation: masked-animation-data-v-b02d8052 4s linear infinite;
  animation: masked-animation-data-v-b02d8052 4s linear infinite;
  -webkit-box-reflect: below -2px -webkit-gradient(linear, left top, left bottom, from(transparent), to(rgba(255, 255, 255, 0.1)));
  position: relative;
  position: relative;
}
@keyframes masked-animation-692a7ee7 {
0%[data-v-692a7ee7] {
    background-position: 0 0;
}
100%[data-v-692a7ee7] {
    background-position: -100% 0;
}
}
.big-data-up .up-center[data-v-692a7ee7]::after {
  content: "";
  width: 250px;
  position: absolute;
  bottom: -15px;
  left: 50%;
  transform: translateX(-50%);
  border: 1px transparent solid;
  border-image: linear-gradient(to right, var(--color-primary-light-9), var(--color-primary)) 1 10;
}
.big-data-up .up-center span[data-v-692a7ee7] {
  cursor: pointer;
}`;const b=f("data-v-692a7ee7");l("data-v-692a7ee7");const g={class:"big-data-up mb15"},_={class:"up-left"},x=e("i",{class:"el-icon-time mr5"},null,-1),h=e("div",{class:"up-center"},[e("span",null,"\u667A\u6167\u519C\u4E1A\u7CFB\u7EDF\u5E73\u53F0")],-1);c();const w=b((t,n,k,y,Q,S)=>(m(),u("div",g,[e("div",_,[x,e("span",null,v(t.time.txt),1)]),h])));a.render=w,a.__scopeId="data-v-692a7ee7";export default a;
