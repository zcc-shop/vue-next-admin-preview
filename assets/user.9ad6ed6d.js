var L=Object.defineProperty;var E=Object.prototype.hasOwnProperty;var U=Object.getOwnPropertySymbols,P=Object.prototype.propertyIsEnumerable;var N=(e,s,t)=>s in e?L(e,s,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[s]=t,B=(e,s)=>{for(var t in s||(s={}))E.call(s,t)&&N(e,t,s[t]);if(U)for(var t of U(s))P.call(s,t)&&N(e,t,s[t]);return e};import{D as $,R as j,u as D,A as R,i as x,c as y,k as F,g as T,t as M,O,B as q,b4 as A,E as H,bd as V,be as Q,b as G,p as J,l as K,r as m,e as W,f as X,m as n,T as Y,Q as Z,U as ee,C as i,q as b,w as ne}from"./index.b9e7554b.js";import{s as z}from"./screenfull.b93df3f3.js";import se from"./userNews.2cd5638d.js";import oe from"./search.83ae0bb6.js";import"./vendor.7fd510b4.js";var S={name:"layoutBreadcrumbUser",components:{UserNews:se,Search:oe},setup(){const{t:e}=$(),{proxy:s}=G(),t=j(),a=D(),p=R(),c=x({isScreenfull:!1,isShowUserNewsPopover:!1,disabledI18n:!1}),l=y(()=>a.state.userInfos.userInfos),f=y(()=>a.state.themeConfig.themeConfig),g=y(()=>{let{layout:u,isClassicSplitMenu:C}=f.value,d="";return u==="defaults"||u==="classic"&&!C?d=1:d=null,d}),h=()=>{if(!z.isEnabled)return O.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;z.toggle(),c.isScreenfull=!c.isScreenfull},v=()=>{s.mittBus.emit("openSetingsDrawer")},w=u=>{u==="logOut"?q({closeOnClickModal:!1,closeOnPressEscape:!1,title:e("message.user.logOutTitle"),message:e("message.user.logOutMessage"),showCancelButton:!0,confirmButtonText:e("message.user.logOutConfirm"),cancelButtonText:e("message.user.logOutCancel"),beforeClose:(C,d,I)=>{C==="confirm"?(d.confirmButtonLoading=!0,d.confirmButtonText=e("message.user.logOutExit"),setTimeout(()=>{I(),setTimeout(()=>{d.confirmButtonLoading=!1},300)},700)):I()}}).then(()=>{A(),H(),t.push("/login"),setTimeout(()=>{O.success(e("message.user.logOutSuccess"))},300)}).catch(()=>{}):t.push(u)},_=()=>{p.value.openSearch()},r=u=>{V("themeConfig"),f.value.globalI18n=u,Q("themeConfig",f.value),s.$i18n.locale=u,k()},k=()=>{switch(T("themeConfig").globalI18n){case"zh-cn":c.disabledI18n="zh-cn";break;case"en":c.disabledI18n="en";break;case"zh-tw":c.disabledI18n="zh-tw";break}};return F(()=>{T("themeConfig")&&k()}),B({getUserInfos:l,onLayoutSetingClick:v,onHandleCommandClick:w,onScreenfullClick:h,onSearchClick:_,onLanguageChange:r,searchRef:p,layoutUserFlexNum:g},M(c))}},ve=`.layout-navbars-breadcrumb-user[data-v-4977f72c] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.layout-navbars-breadcrumb-user-link[data-v-4977f72c] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-4977f72c] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-4977f72c] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-4977f72c]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-4977f72c] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-4977f72c] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-4977f72c] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-4977f72c] .el-badge__content.is-fixed {
  top: 12px;
}`;const o=ne();J("data-v-4977f72c");const ae={class:"layout-navbars-breadcrumb-user-icon"},te=i("\u7B80\u4F53\u4E2D\u6587"),le=i("English"),re=i("\u7E41\u9AD4\u4E2D\u6587"),ie={class:"layout-navbars-breadcrumb-user-icon"},ue={class:"layout-navbars-breadcrumb-user-link"},ce=n("i",{class:"el-icon-arrow-down el-icon--right"},null,-1);K();const de=o((e,s,t,a,p,c)=>{const l=m("el-dropdown-item"),f=m("el-dropdown-menu"),g=m("el-dropdown"),h=m("el-badge"),v=m("UserNews"),w=m("el-popover"),_=m("Search");return W(),X("div",{class:"layout-navbars-breadcrumb-user",style:{flex:a.layoutUserFlexNum}},[n(g,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:a.onLanguageChange},{dropdown:o(()=>[n(f,null,{default:o(()=>[n(l,{command:"zh-cn",disabled:e.disabledI18n==="zh-cn"},{default:o(()=>[te]),_:1},8,["disabled"]),n(l,{command:"en",disabled:e.disabledI18n==="en"},{default:o(()=>[le]),_:1},8,["disabled"]),n(l,{command:"zh-tw",disabled:e.disabledI18n==="zh-tw"},{default:o(()=>[re]),_:1},8,["disabled"])]),_:1})]),default:o(()=>[n("div",ae,[n("i",{class:["iconfont",e.disabledI18n==="en"?"icon-fuhao-yingwen":"icon-fuhao-zhongwen"],title:e.$t("message.user.title1")},null,10,["title"])])]),_:1},8,["onCommand"]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[1]||(s[1]=(...r)=>a.onSearchClick&&a.onSearchClick(...r))},[n("i",{class:"el-icon-search",title:e.$t("message.user.title2")},null,8,["title"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[2]||(s[2]=(...r)=>a.onLayoutSetingClick&&a.onLayoutSetingClick(...r))},[n("i",{class:"icon-skin iconfont",title:e.$t("message.user.title3")},null,8,["title"])]),n("div",ie,[n(w,{placement:"bottom",trigger:"click",visible:e.isShowUserNewsPopover,"onUpdate:visible":s[4]||(s[4]=r=>e.isShowUserNewsPopover=r),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:o(()=>[n(h,{"is-dot":!0,onClick:s[3]||(s[3]=r=>e.isShowUserNewsPopover=!e.isShowUserNewsPopover)},{default:o(()=>[n("i",{class:"el-icon-bell",title:e.$t("message.user.title4")},null,8,["title"])]),_:1})]),default:o(()=>[n(Y,{name:"el-zoom-in-top"},{default:o(()=>[Z(n(v,null,null,512),[[ee,e.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:s[5]||(s[5]=(...r)=>a.onScreenfullClick&&a.onScreenfullClick(...r))},[n("i",{class:["iconfont",e.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:e.isScreenfull?e.$t("message.user.title5"):e.$t("message.user.title6")},null,10,["title"])]),n(g,{"show-timeout":70,"hide-timeout":50,onCommand:a.onHandleCommandClick},{dropdown:o(()=>[n(f,null,{default:o(()=>[n(l,{command:"/home"},{default:o(()=>[i(b(e.$t("message.user.dropdown1")),1)]),_:1}),n(l,{command:"/personal"},{default:o(()=>[i(b(e.$t("message.user.dropdown2")),1)]),_:1}),n(l,{command:"/404"},{default:o(()=>[i(b(e.$t("message.user.dropdown3")),1)]),_:1}),n(l,{command:"/401"},{default:o(()=>[i(b(e.$t("message.user.dropdown4")),1)]),_:1}),n(l,{divided:"",command:"logOut"},{default:o(()=>[i(b(e.$t("message.user.dropdown5")),1)]),_:1})]),_:1})]),default:o(()=>[n("span",ue,[n("img",{src:a.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),i(" "+b(a.getUserInfos.userName===""?"test":a.getUserInfos.userName)+" ",1),ce])]),_:1},8,["onCommand"]),n(_,{ref:"searchRef"},null,512)],4)});S.render=de,S.__scopeId="data-v-4977f72c";export default S;
