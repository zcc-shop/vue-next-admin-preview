import{u as m}from"./index.e6273694.js";import p from"./index.0f90a45d.js";import _ from"./tagsView.b1c04570.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as b,M as h,F as w}from"./vendor.ce82b265.js";import"./breadcrumb.ea4595a1.js";import"./user.39d6f62f.js";import"./screenfull.f01d3510.js";import"./userNews.6243620a.js";import"./search.8bdeb5ce.js";import"./index.624661b0.js";import"./horizontal.202ce2ac.js";import"./subItem.b5b44a2a.js";import"./contextmenu.81658cf0.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,y)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[b(i),e.setShowTagsView?(r(),n(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
