var L=Object.defineProperty;var z=Object.prototype.hasOwnProperty;var k=Object.getOwnPropertySymbols,$=Object.prototype.propertyIsEnumerable;var y=(e,s,a)=>s in e?L(e,s,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[s]=a,I=(e,s)=>{for(var a in s||(s={}))z.call(s,a)&&y(e,a,s[a]);if(k)for(var a of k(s))$.call(s,a)&&y(e,a,s[a]);return e};import{D as j,R as E,u as P,A as D,i as R,c as U,k as x,g as N,t as q,O as B,B as M,b3 as A,E as F,bc as H,bd as V,b as Q,p as G,l as J,r as c,e as K,f as W,m as n,T as X,Q as Y,U as Z,C as i,q as d,w as ee}from"./index.ef4a75e8.js";import{s as T}from"./screenfull.6e1d84b2.js";import ne from"./userNews.51df2254.js";import se from"./search.ff4a2b0a.js";import"./vendor.5429c63c.js";var C={name:"layoutBreadcrumbUser",components:{UserNews:ne,Search:se},setup(){const{t:e}=j(),{proxy:s}=Q(),a=E(),t=P(),f=D(),u=R({isScreenfull:!1,isShowUserNewsPopover:!1,disabledI18n:!1}),l=U(()=>t.state.userInfos.userInfos),m=U(()=>t.state.themeConfig.themeConfig),g=()=>{if(!T.isEnabled)return B.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;T.toggle(),u.isScreenfull=!u.isScreenfull},p=()=>{s.mittBus.emit("openSetingsDrawer")},h=b=>{b==="logOut"?M({closeOnClickModal:!1,closeOnPressEscape:!1,title:e("message.user.logOutTitle"),message:e("message.user.logOutMessage"),showCancelButton:!0,confirmButtonText:e("message.user.logOutConfirm"),cancelButtonText:e("message.user.logOutCancel"),beforeClose:(O,_,S)=>{O==="confirm"?(_.confirmButtonLoading=!0,_.confirmButtonText=e("message.user.logOutExit"),setTimeout(()=>{S(),setTimeout(()=>{_.confirmButtonLoading=!1},300)},700)):S()}}).then(()=>{A(),F(),a.push("/login"),setTimeout(()=>{B.success(e("message.user.logOutSuccess"))},300)}).catch(()=>{}):a.push(b)},v=()=>{f.value.openSearch()},w=b=>{H("themeConfig"),m.value.globalI18n=b,V("themeConfig",m.value),s.$i18n.locale=b,r()},r=()=>{switch(N("themeConfig").globalI18n){case"zh-cn":u.disabledI18n="zh-cn";break;case"en":u.disabledI18n="en";break;case"zh-tw":u.disabledI18n="zh-tw";break}};return x(()=>{N("themeConfig")&&r()}),I({getUserInfos:l,onLayoutSetingClick:p,onHandleCommandClick:h,onScreenfullClick:g,onSearchClick:v,onLanguageChange:w,searchRef:f},q(u))}},ve=`.layout-navbars-breadcrumb-user[data-v-0cbe26ba] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex: 1;
}
.layout-navbars-breadcrumb-user-link[data-v-0cbe26ba] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-0cbe26ba] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-0cbe26ba] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-0cbe26ba]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-0cbe26ba] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-0cbe26ba] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-0cbe26ba] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-0cbe26ba] .el-badge__content.is-fixed {
  top: 12px;
}`;const o=ee();G("data-v-0cbe26ba");const oe={class:"layout-navbars-breadcrumb-user"},ae={class:"layout-navbars-breadcrumb-user-icon"},te=i("\u7B80\u4F53\u4E2D\u6587"),re=i("English"),le=i("\u7E41\u9AD4\u4E2D\u6587"),ie={class:"layout-navbars-breadcrumb-user-icon"},ue={class:"layout-navbars-breadcrumb-user-link"},ce=n("i",{class:"el-icon-arrow-down el-icon--right"},null,-1);J();const de=o((e,s,a,t,f,u)=>{const l=c("el-dropdown-item"),m=c("el-dropdown-menu"),g=c("el-dropdown"),p=c("el-badge"),h=c("UserNews"),v=c("el-popover"),w=c("Search");return K(),W("div",oe,[n(g,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:t.onLanguageChange},{dropdown:o(()=>[n(m,null,{default:o(()=>[n(l,{command:"zh-cn",disabled:e.disabledI18n==="zh-cn"},{default:o(()=>[te]),_:1},8,["disabled"]),n(l,{command:"en",disabled:e.disabledI18n==="en"},{default:o(()=>[re]),_:1},8,["disabled"]),n(l,{command:"zh-tw",disabled:e.disabledI18n==="zh-tw"},{default:o(()=>[le]),_:1},8,["disabled"])]),_:1})]),default:o(()=>[n("div",ae,[n("i",{class:"iconfont icon-huanjingxingqiu",title:e.$t("message.user.title1")},null,8,["title"])])]),_:1},8,["onCommand"]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[1]||(s[1]=(...r)=>t.onSearchClick&&t.onSearchClick(...r))},[n("i",{class:"el-icon-search",title:e.$t("message.user.title2")},null,8,["title"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[2]||(s[2]=(...r)=>t.onLayoutSetingClick&&t.onLayoutSetingClick(...r))},[n("i",{class:"icon-skin iconfont",title:e.$t("message.user.title3")},null,8,["title"])]),n("div",ie,[n(v,{placement:"bottom",trigger:"click",visible:e.isShowUserNewsPopover,"onUpdate:visible":s[4]||(s[4]=r=>e.isShowUserNewsPopover=r),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:o(()=>[n(p,{"is-dot":!0,onClick:s[3]||(s[3]=r=>e.isShowUserNewsPopover=!e.isShowUserNewsPopover)},{default:o(()=>[n("i",{class:"el-icon-bell",title:e.$t("message.user.title4")},null,8,["title"])]),_:1})]),default:o(()=>[n(X,{name:"el-zoom-in-top"},{default:o(()=>[Y(n(h,null,null,512),[[Z,e.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:s[5]||(s[5]=(...r)=>t.onScreenfullClick&&t.onScreenfullClick(...r))},[n("i",{class:["iconfont",e.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:e.isScreenfull?e.$t("message.user.title5"):e.$t("message.user.title6")},null,10,["title"])]),n(g,{"show-timeout":70,"hide-timeout":50,onCommand:t.onHandleCommandClick},{dropdown:o(()=>[n(m,null,{default:o(()=>[n(l,{command:"/home"},{default:o(()=>[i(d(e.$t("message.user.dropdown1")),1)]),_:1}),n(l,{command:"/personal"},{default:o(()=>[i(d(e.$t("message.user.dropdown2")),1)]),_:1}),n(l,{command:"/404"},{default:o(()=>[i(d(e.$t("message.user.dropdown3")),1)]),_:1}),n(l,{command:"/401"},{default:o(()=>[i(d(e.$t("message.user.dropdown4")),1)]),_:1}),n(l,{divided:"",command:"logOut"},{default:o(()=>[i(d(e.$t("message.user.dropdown5")),1)]),_:1})]),_:1})]),default:o(()=>[n("span",ue,[n("img",{src:t.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),i(" "+d(t.getUserInfos.userName===""?"test":t.getUserInfos.userName)+" ",1),ce])]),_:1},8,["onCommand"]),n(w,{ref:"searchRef"},null,512)])});C.render=de,C.__scopeId="data-v-0cbe26ba";export default C;
