import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as b}from"./index.b9e7554b.js";import h from"./index.48f30d75.js";import w from"./tagsView.6f3fa474.js";import"./vendor.7fd510b4.js";import"./breadcrumb.1e871236.js";import"./user.9ad6ed6d.js";import"./screenfull.b93df3f3.js";import"./userNews.2cd5638d.js";import"./search.83ae0bb6.js";import"./index.c7520fb5.js";import"./horizontal.33704162.js";import"./subItem.ffea3550.js";import"./contextmenu.b4b4428e.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:h,TagsView:w},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=b();_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=v((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
