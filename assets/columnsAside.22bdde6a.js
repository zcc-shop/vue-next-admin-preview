var B=Object.defineProperty;var M=Object.prototype.hasOwnProperty;var v=Object.getOwnPropertySymbols,F=Object.prototype.propertyIsEnumerable;var S=(i,s,n)=>s in i?B(i,s,{enumerable:!0,configurable:!0,writable:!0,value:n}):i[s]=n,h=(i,s)=>{for(var n in s||(s={}))M.call(s,n)&&S(i,n,s[n]);if(v)for(var n of v(s))F.call(s,n)&&S(i,n,s[n]);return i};import{A as k,u as T,x as $,R as D,i as j,c as O,b4 as z,k as H,b5 as q,t as N,n as P,b as U,p as V,l as X,r as E,e as f,f as p,m as c,F as G,s as J,q as L,w as K}from"./index.ef4a75e8.js";import"./vendor.5429c63c.js";var x={name:"layoutColumnsAside",setup(){const i=k([]),s=k(),{proxy:n}=U(),r=T(),g=$(),y=D(),o=j({columnsAsideList:[],liIndex:0,difference:0,routeSplit:[]}),t=O(()=>r.state.themeConfig.themeConfig.columnsAsideStyle),d=e=>{o.liIndex=e,s.value.style.top=`${i.value[e].offsetTop+o.difference}px`},m=(e,l)=>{d(l);let{path:u,redirect:a}=e;a?y.push(a):y.push(u)},_=e=>{P(()=>{d(e)})},C=()=>{o.columnsAsideList=A(r.state.routesList.routesList);const e=b(g.path);_(e.item[0].k),n.mittBus.emit("setSendColumnsChildren",e)},b=e=>{const l=e.split("/");let u={};return o.columnsAsideList.map((a,w)=>{a.path===`/${l[1]}`&&(a.k=w,u.item=[h({},a)],u.children=[h({},a)],a.children&&(u.children=a.children))}),u},A=e=>e.filter(l=>!l.meta.isHide).map(l=>(l=Object.assign({},l),l.children&&(l.children=A(l.children)),l)),I=e=>{o.routeSplit=e.split("/"),o.routeSplit.shift();const l=`/${o.routeSplit[0]}`,u=o.columnsAsideList.find(a=>a.path===l);setTimeout(()=>{_(u.k)},0)};return z(r.state,e=>{if(e.themeConfig.themeConfig.columnsAsideStyle==="columnsRound"?o.difference=3:o.difference=0,e.routesList.routesList.length===o.columnsAsideList.length)return!1;C()}),H(()=>{C()}),q(e=>{I(e.path),n.mittBus.emit("setSendColumnsChildren",b(e.path))}),h({columnsAsideOffsetTopRefs:i,columnsAsideActiveRef:s,onColumnsAsideDown:_,setColumnsAsideStyle:t,onColumnsAsideMenuClick:m},N(o))}},le=`.layout-columns-aside[data-v-138ec090] {
  width: 64px;
  height: 100%;
  background: var(--bg-columnsMenuBar);
}
.layout-columns-aside ul[data-v-138ec090] {
  position: relative;
}
.layout-columns-aside ul li[data-v-138ec090] {
  color: var(--bg-columnsMenuBarColor);
  width: 100%;
  height: 50px;
  text-align: center;
  display: flex;
  cursor: pointer;
  position: relative;
  z-index: 1;
}
.layout-columns-aside ul li .layout-columns-aside-li-box[data-v-138ec090] {
  margin: auto;
}
.layout-columns-aside ul li .layout-columns-aside-li-box .layout-columns-aside-li-box-title[data-v-138ec090] {
  padding-top: 1px;
}
.layout-columns-aside ul li a[data-v-138ec090] {
  text-decoration: none;
  color: var(--bg-columnsMenuBarColor);
}
.layout-columns-aside ul .layout-columns-active[data-v-138ec090] {
  color: #ffffff;
  transition: 0.3s ease-in-out;
}
.layout-columns-aside ul .columns-round[data-v-138ec090], .layout-columns-aside ul .columns-card[data-v-138ec090] {
  background: var(--color-primary);
  color: #ffffff;
  position: absolute;
  left: 50%;
  top: 2px;
  height: 44px;
  width: 58px;
  transform: translateX(-50%);
  z-index: 0;
  transition: 0.3s ease-in-out;
  border-radius: 5px;
}
.layout-columns-aside ul .columns-card[data-v-138ec090] {
  top: 0;
  height: 50px;
  width: 100%;
  border-radius: 0;
}`;const R=K();V("data-v-138ec090");const Q={class:"layout-columns-aside"},W={key:0,class:"layout-columns-aside-li-box"},Y={class:"layout-columns-aside-li-box-title font12"},Z={key:1,class:"layout-columns-aside-li-box"},ee={class:"layout-columns-aside-li-box-title font12"};X();const te=R((i,s,n,r,g,y)=>{const o=E("el-scrollbar");return f(),p("div",Q,[c(o,null,{default:R(()=>[c("ul",null,[(f(!0),p(G,null,J(i.columnsAsideList,(t,d)=>(f(),p("li",{key:d,onClick:m=>r.onColumnsAsideMenuClick(t,d),ref:m=>{m&&(r.columnsAsideOffsetTopRefs[d]=m)},class:{"layout-columns-active":i.liIndex===d},title:t.meta.title},[!t.meta.isLink||t.meta.isLink&&t.meta.isIframe?(f(),p("div",W,[c("i",{class:t.meta.icon},null,2),c("div",Y,L(t.meta.title&&t.meta.title.length>=4?t.meta.title.substr(0,4):t.meta.title),1)])):(f(),p("div",Z,[c("a",{href:t.meta.isLink,target:"_blank"},[c("i",{class:t.meta.icon},null,2),c("div",ee,L(t.meta.title&&t.meta.title.length>=4?t.meta.title.substr(0,4):t.meta.title),1)],8,["href"])]))],10,["onClick","title"]))),128)),c("div",{ref:"columnsAsideActiveRef",class:r.setColumnsAsideStyle},null,2)])]),_:1})])});x.render=te,x.__scopeId="data-v-138ec090";export default x;
