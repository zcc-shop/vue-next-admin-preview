var O=Object.defineProperty;var j=Object.prototype.hasOwnProperty;var N=Object.getOwnPropertySymbols,D=Object.prototype.propertyIsEnumerable;var U=(e,s,t)=>s in e?O(e,s,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[s]=t,z=(e,s)=>{for(var t in s||(s={}))j.call(s,t)&&U(e,t,s[t]);if(N)for(var t of N(s))D.call(s,t)&&U(e,t,s[t]);return e};import{b as E,i as P,u as $,g as B,v as T,N as x,az as F,r as R,aD as M,aE as q}from"./index.85d2a956.js";import{s as L}from"./screenfull.e0775b6b.js";import A from"./userNews.a2c52168.js";import H from"./search.f2b556ce.js";import{a as V,r as G,c as y,j as J,t as K,k as Q,l as W,m as X,q as m,y as Y,z as Z,A as n,T as ee,v as ne,x as se,C as u,D as g,F as oe}from"./vendor.69bd2edd.js";var S={name:"layoutBreadcrumbUser",components:{UserNews:A,Search:H},setup(){const{t:e}=E(),{proxy:s}=Q(),t=P(),a=$(),p=V(),d=G({isScreenfull:!1,isShowUserNewsPopover:!1,disabledI18n:!1}),l=y(()=>a.state.userInfos.userInfos),b=y(()=>a.state.themeConfig.themeConfig),f=y(()=>{let{layout:i,isClassicSplitMenu:C}=b.value,c="";return i==="defaults"||i==="classic"&&!C||i==="columns"?c=1:c=null,c}),h=()=>{if(!L.isEnabled)return T.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;L.toggle(),d.isScreenfull=!d.isScreenfull},v=()=>{s.mittBus.emit("openSetingsDrawer")},w=i=>{i==="logOut"?x({closeOnClickModal:!1,closeOnPressEscape:!1,title:e("message.user.logOutTitle"),message:e("message.user.logOutMessage"),showCancelButton:!0,confirmButtonText:e("message.user.logOutConfirm"),cancelButtonText:e("message.user.logOutCancel"),beforeClose:(C,c,I)=>{C==="confirm"?(c.confirmButtonLoading=!0,c.confirmButtonText=e("message.user.logOutExit"),setTimeout(()=>{I(),setTimeout(()=>{c.confirmButtonLoading=!1},300)},700)):I()}}).then(()=>{F(),R(),t.push("/login"),setTimeout(()=>{T.success(e("message.user.logOutSuccess"))},300)}).catch(()=>{}):t.push(i)},_=()=>{p.value.openSearch()},r=i=>{M("themeConfig"),b.value.globalI18n=i,q("themeConfig",b.value),s.$i18n.locale=i,k()},k=()=>{switch(B("themeConfig").globalI18n){case"zh-cn":d.disabledI18n="zh-cn";break;case"en":d.disabledI18n="en";break;case"zh-tw":d.disabledI18n="zh-tw";break}};return J(()=>{B("themeConfig")&&k()}),z({getUserInfos:l,onLayoutSetingClick:v,onHandleCommandClick:w,onScreenfullClick:h,onSearchClick:_,onLanguageChange:r,searchRef:p,layoutUserFlexNum:f},K(d))}},ve=`.layout-navbars-breadcrumb-user[data-v-12780dc2] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.layout-navbars-breadcrumb-user-link[data-v-12780dc2] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-12780dc2] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-12780dc2] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-12780dc2]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-12780dc2] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-12780dc2] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-12780dc2] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-12780dc2] .el-badge__content.is-fixed {
  top: 12px;
}`;const o=oe();W("data-v-12780dc2");const ae={class:"layout-navbars-breadcrumb-user-icon"},te=u("\u7B80\u4F53\u4E2D\u6587"),le=u("English"),re=u("\u7E41\u9AD4\u4E2D\u6587"),ie={class:"layout-navbars-breadcrumb-user-icon"},ue={class:"layout-navbars-breadcrumb-user-link"},de=n("i",{class:"el-icon-arrow-down el-icon--right"},null,-1);X();const ce=o((e,s,t,a,p,d)=>{const l=m("el-dropdown-item"),b=m("el-dropdown-menu"),f=m("el-dropdown"),h=m("el-badge"),v=m("UserNews"),w=m("el-popover"),_=m("Search");return Y(),Z("div",{class:"layout-navbars-breadcrumb-user",style:{flex:a.layoutUserFlexNum}},[n(f,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:a.onLanguageChange},{dropdown:o(()=>[n(b,null,{default:o(()=>[n(l,{command:"zh-cn",disabled:e.disabledI18n==="zh-cn"},{default:o(()=>[te]),_:1},8,["disabled"]),n(l,{command:"en",disabled:e.disabledI18n==="en"},{default:o(()=>[le]),_:1},8,["disabled"]),n(l,{command:"zh-tw",disabled:e.disabledI18n==="zh-tw"},{default:o(()=>[re]),_:1},8,["disabled"])]),_:1})]),default:o(()=>[n("div",ae,[n("i",{class:["iconfont",e.disabledI18n==="en"?"icon-fuhao-yingwen":"icon-fuhao-zhongwen"],title:e.$t("message.user.title1")},null,10,["title"])])]),_:1},8,["onCommand"]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[1]||(s[1]=(...r)=>a.onSearchClick&&a.onSearchClick(...r))},[n("i",{class:"el-icon-search",title:e.$t("message.user.title2")},null,8,["title"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[2]||(s[2]=(...r)=>a.onLayoutSetingClick&&a.onLayoutSetingClick(...r))},[n("i",{class:"icon-skin iconfont",title:e.$t("message.user.title3")},null,8,["title"])]),n("div",ie,[n(w,{placement:"bottom",trigger:"click",visible:e.isShowUserNewsPopover,"onUpdate:visible":s[4]||(s[4]=r=>e.isShowUserNewsPopover=r),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:o(()=>[n(h,{"is-dot":!0,onClick:s[3]||(s[3]=r=>e.isShowUserNewsPopover=!e.isShowUserNewsPopover)},{default:o(()=>[n("i",{class:"el-icon-bell",title:e.$t("message.user.title4")},null,8,["title"])]),_:1})]),default:o(()=>[n(ee,{name:"el-zoom-in-top"},{default:o(()=>[ne(n(v,null,null,512),[[se,e.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),n("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:s[5]||(s[5]=(...r)=>a.onScreenfullClick&&a.onScreenfullClick(...r))},[n("i",{class:["iconfont",e.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:e.isScreenfull?e.$t("message.user.title5"):e.$t("message.user.title6")},null,10,["title"])]),n(f,{"show-timeout":70,"hide-timeout":50,onCommand:a.onHandleCommandClick},{dropdown:o(()=>[n(b,null,{default:o(()=>[n(l,{command:"/home"},{default:o(()=>[u(g(e.$t("message.user.dropdown1")),1)]),_:1}),n(l,{command:"/personal"},{default:o(()=>[u(g(e.$t("message.user.dropdown2")),1)]),_:1}),n(l,{command:"/404"},{default:o(()=>[u(g(e.$t("message.user.dropdown3")),1)]),_:1}),n(l,{command:"/401"},{default:o(()=>[u(g(e.$t("message.user.dropdown4")),1)]),_:1}),n(l,{divided:"",command:"logOut"},{default:o(()=>[u(g(e.$t("message.user.dropdown5")),1)]),_:1})]),_:1})]),default:o(()=>[n("span",ue,[n("img",{src:a.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),u(" "+g(a.getUserInfos.userName===""?"test":a.getUserInfos.userName)+" ",1),de])]),_:1},8,["onCommand"]),n(_,{ref:"searchRef"},null,512)],4)});S.render=ce,S.__scopeId="data-v-12780dc2";export default S;
