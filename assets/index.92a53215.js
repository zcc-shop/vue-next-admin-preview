import{u as m,c as p,p as _,l as f,r as a,e as r,f as n,m as u,h as l,w as h}from"./index.9af39cde.js";import w from"./index.d869783b.js";import v from"./tagsView.70514234.js";import"./vendor.8f964570.js";import"./breadcrumb.ad903dec.js";import"./user.d4f5dd56.js";import"./screenfull.77def122.js";import"./userNews.70e100da.js";import"./search.fc372af0.js";import"./index.08977137.js";import"./horizontal.1d571f4f.js";import"./subItem.5e44edf9.js";import"./contextmenu.c552e0b2.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h("data-v-fde5c75e");_("data-v-fde5c75e");const x={class:"layout-navbars-container"};f();const g=j((t,c,o,e,b,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",x,[u(d),e.setShowTagsView?(r(),n(i,{key:0})):l("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
