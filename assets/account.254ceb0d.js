var F=Object.assign;import{v as x,u as w,Q as k,i as I,c as y,t as C,D as v,aZ as S,a_ as V,M as A,j as B,E as L,G as N,H as R,p as j,l as E,r as i,e as T,f as $,m as e,w as D}from"./index.d334a43e.js";import"./vendor.8f964570.js";var f=x({name:"login",setup(){const t=w(),s=k(),u=I({ruleForm:{userName:"admin",password:"123456",code:"1234"}}),c=y(()=>B(new Date)),g=()=>{L(),N(),R()},_=()=>{let n=[],r=[],d=["admin"],m=["btn.add","btn.del","btn.edit","btn.link"],p=["test"],a=["btn.add","btn.link"];u.ruleForm.userName==="admin"?(n=d,r=m):(n=p,r=a);const h={userName:u.ruleForm.userName,photo:u.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:n,authBtnList:r};v("token",Math.random().toString(36).substr(0)),v("userInfo",h),t.dispatch("userInfos/setUserInfos",h),t.state.themeConfig.themeConfig.isRequestRoutes?S(b=>{V(b,()=>{l()})}):(g(),l())},l=()=>{let n=c.value;s.push("/"),setTimeout(()=>{A.success(`${n}\uFF0C\u6B22\u8FCE\u56DE\u6765\uFF01`)},300)};return F({currentTime:c,onSignIn:_},C(u))}}),K=`.login-content-form[data-v-4831dc39] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-4831dc39] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-4831dc39] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
}
.login-content-form .login-content-code .login-content-code-img[data-v-4831dc39]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-4831dc39] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const o=D("data-v-4831dc39");j("data-v-4831dc39");const H=e("div",{class:"login-content-code"},[e("span",{class:"login-content-code-img"},"1234")],-1),P=e("span",null,"\u767B \u5F55",-1);E();const U=o((t,s,u,c,g,_)=>{const l=i("el-input"),n=i("el-form-item"),r=i("el-col"),d=i("el-row"),m=i("el-button"),p=i("el-form");return T(),$(p,{class:"login-content-form"},{default:o(()=>[e(n,null,{default:o(()=>[e(l,{type:"text",placeholder:"\u7528\u6237\u540D admin \u6216\u4E0D\u8F93\u5747\u4E3A test","prefix-icon":"el-icon-user",modelValue:t.ruleForm.userName,"onUpdate:modelValue":s[1]||(s[1]=a=>t.ruleForm.userName=a),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),e(n,null,{default:o(()=>[e(l,{type:"password",placeholder:"\u5BC6\u7801\uFF1A123456","prefix-icon":"el-icon-lock",modelValue:t.ruleForm.password,"onUpdate:modelValue":s[2]||(s[2]=a=>t.ruleForm.password=a),autocomplete:"off","show-password":""},null,8,["modelValue"])]),_:1}),e(n,null,{default:o(()=>[e(d,{gutter:15},{default:o(()=>[e(r,{span:16},{default:o(()=>[e(l,{type:"text",maxlength:"4",placeholder:"\u8BF7\u8F93\u5165\u9A8C\u8BC1\u7801","prefix-icon":"el-icon-position",modelValue:t.ruleForm.code,"onUpdate:modelValue":s[3]||(s[3]=a=>t.ruleForm.code=a),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),e(r,{span:8},{default:o(()=>[H]),_:1})]),_:1})]),_:1}),e(n,null,{default:o(()=>[e(m,{type:"primary",class:"login-content-submit",round:"",onClick:t.onSignIn},{default:o(()=>[P]),_:1},8,["onClick"])]),_:1})]),_:1})});f.render=U,f.__scopeId="data-v-4831dc39";export default f;
