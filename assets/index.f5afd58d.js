import{u as m}from"./index.6e60973e.js";import p from"./index.db22d0cb.js";import _ from"./tagsView.6a89af55.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as b,M as h,F as w}from"./vendor.ad170e2b.js";import"./breadcrumb.7b8cbf36.js";import"./user.43e1d741.js";import"./screenfull.d9bbb71f.js";import"./userNews.c4edda8c.js";import"./search.4e08e17d.js";import"./index.a8f0ee53.js";import"./horizontal.88f869fd.js";import"./subItem.bb98103e.js";import"./contextmenu.e96fb587.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,y)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",j,[b(d),e.setShowTagsView?(r(),n(i,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
