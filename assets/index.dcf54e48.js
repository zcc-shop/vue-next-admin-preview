var w=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(e,n,o)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,p=(e,n)=>{for(var o in n||(n={}))u.call(n,o)&&f(e,o,n[o]);if(d)for(var o of d(n))b.call(n,o)&&f(e,o,n[o]);return e};import{i as x}from"./getStyleSheets.222a269b.js";import{r as g,j as y,t as I,l as S,m as L,q as s,y as r,z as l,A as t,L as $,V as j,D as k,F as z}from"./vendor.3f76c63b.js";var i={name:"pagesElement",setup(){const e=g({sheetsIconList:[]}),n=()=>{x.ele().then(o=>e.sheetsIconList=o)};return y(()=>{n()}),p({},I(e))}},H=`.element-container .iconfont-row[data-v-140f7842] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.element-container .iconfont-row .el-col[data-v-140f7842]:nth-last-child(1),
.element-container .iconfont-row .el-col[data-v-140f7842]:nth-last-child(2) {
  display: none;
}
.element-container .iconfont-row .iconfont-warp[data-v-140f7842] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-140f7842] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-140f7842] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-140f7842] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-140f7842] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=z();S("data-v-140f7842");const B={class:"element-container"},D={class:"iconfont-warp"},F={class:"flex-margin"},V={class:"iconfont-warp-value"},q={class:"iconfont-warp-label mt10"};L();const A=a((e,n,o,C,E,G)=>{const _=s("el-col"),h=s("el-row"),m=s("el-card");return r(),l("div",B,[t(m,{shadow:"hover",header:`element plus \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-2}\u4E2A`},{default:a(()=>[t(h,{class:"iconfont-row"},{default:a(()=>[(r(!0),l($,null,j(e.sheetsIconList,(c,v)=>(r(),l(_,{xs:12,sm:8,md:6,lg:4,xl:2,key:v},{default:a(()=>[t("div",D,[t("div",F,[t("div",V,[t("i",{class:c},null,2)]),t("div",q,k(c),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});i.render=A,i.__scopeId="data-v-140f7842";export default i;
