import{u as m}from"./index.4aef2fce.js";import p from"./index.dd107ac5.js";import _ from"./tagsView.cf5059b5.js";import{z as f,p as u,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.a5c5c942.js";import"./breadcrumb.f4ed2c49.js";import"./user.0a7ad65c.js";import"./screenfull.52ff4b74.js";import"./userNews.41103212.js";import"./search.780935a1.js";import"./index.bc492c35.js";import"./horizontal.f5a79227.js";import"./subItem.ef5a92f9.js";import"./contextmenu.31d06cd8.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:f(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=v();u("data-v-fde5c75e");const x={class:"layout-navbars-container"};l();const g=j((t,c,o,e,b,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
