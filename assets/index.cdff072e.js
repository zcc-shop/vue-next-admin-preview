var D=Object.defineProperty;var N=Object.prototype.hasOwnProperty;var F=Object.getOwnPropertySymbols,z=Object.prototype.propertyIsEnumerable;var B=(a,o,i)=>o in a?D(a,o,{enumerable:!0,configurable:!0,writable:!0,value:i}):a[o]=i,w=(a,o)=>{for(var i in o||(o={}))N.call(o,i)&&B(a,i,o[i]);if(F)for(var i of F(o))z.call(o,i)&&B(a,i,o[i]);return a};import{i as C}from"./index.0b65a8e0.js";import{C as u}from"./countUp.min.e806ccee.js";import{i as k,c as L,j as I,k as M,t as j,n as O,b as R,p as S,l as $,r as h,e as c,f as l,m as e,q as r,F as x,s as g,w as H}from"./index.f043429b.js";import"./vendor.f1659326.js";const T=[{title:"\u4ECA\u65E5\u8BBF\u95EE\u4EBA\u6570",titleNum:"123",tip:"\u5728\u573A\u4EBA\u6570",tipNum:"911",color:"#F95959",iconColor:"#F86C6B",icon:"iconfont icon-jinridaiban"},{title:"\u5B9E\u9A8C\u5BA4\u603B\u6570",titleNum:"123",tip:"\u4F7F\u7528\u4E2D",tipNum:"611",color:"#8595F4",iconColor:"#92A1F4",icon:"iconfont icon-AIshiyanshi"},{title:"\u7533\u8BF7\u4EBA\u6570\uFF08\u6708\uFF09",titleNum:"123",tip:"\u901A\u8FC7\u4EBA\u6570",tipNum:"911",color:"#FEBB50",iconColor:"#FDC566",icon:"iconfont icon-shenqingkaiban"}],U=[{icon:"iconfont icon-yangan",label:"\u70DF\u611F",value:"2.1%OBS/M",iconColor:"#F72B3F"},{icon:"iconfont icon-wendu",label:"\u6E29\u5EA6",value:"30\u2103",iconColor:"#91BFF8"},{icon:"iconfont icon-shidu",label:"\u6E7F\u5EA6",value:"57%RH",iconColor:"#88D565"},{icon:"iconfont icon-zaosheng",label:"\u566A\u58F0",value:"57DB",iconColor:"#FBD4A0"}],q=[{time1:"\u4ECA\u5929",time2:"12:20:30",title:"\u66F4\u540D",label:"\u6B63\u5F0F\u66F4\u540D\u4E3A vue-next-admin"},{time1:"02-17",time2:"12:20:30",title:"\u9875\u9762",label:"\u5B8C\u6210\u5BF9\u9996\u9875\u7684\u5F00\u53D1"},{time1:"02-14",time2:"12:20:30",title:"\u9875\u9762",label:"\u65B0\u589E\u4E2A\u4EBA\u4E2D\u5FC3"}];var v={name:"home",setup(){const{proxy:a}=R(),o=k({topCardItemList:T,environmentList:U,activitiesList:q,tableData:{data:[{date:"2016-05-02",name:"1\u53F7\u5B9E\u9A8C\u5BA4",address:"\u70DF\u611F2.1%OBS/M"},{date:"2016-05-04",name:"2\u53F7\u5B9E\u9A8C\u5BA4",address:"\u6E29\u5EA630\u2103"},{date:"2016-05-01",name:"3\u53F7\u5B9E\u9A8C\u5BA4",address:"\u6E7F\u5EA657%RH"}]}}),i=L(()=>I(new Date)),f=()=>{O(()=>{new u("titleNum1",Math.random()*1e4).start(),new u("titleNum2",Math.random()*1e4).start(),new u("titleNum3",Math.random()*1e4).start(),new u("tipNum1",Math.random()*1e3).start(),new u("tipNum2",Math.random()*1e3).start(),new u("tipNum3",Math.random()*1e3).start()})},y=()=>{const m=C(a.$refs.homeLaboratoryRef),d={grid:{top:50,right:20,bottom:30,left:30},tooltip:{trigger:"axis"},legend:{data:["\u9884\u8D2D\u961F\u5217","\u6700\u65B0\u6210\u4EA4\u4EF7"],right:13},xAxis:{data:["\u886C\u886B","\u7F8A\u6BDB\u886B","\u96EA\u7EBA\u886B","\u88E4\u5B50","\u9AD8\u8DDF\u978B","\u889C\u5B50"]},yAxis:[{type:"value",name:"\u4EF7\u683C"}],series:[{name:"\u9884\u8D2D\u961F\u5217",type:"bar",data:[5,20,36,10,10,20]},{name:"\u6700\u65B0\u6210\u4EA4\u4EF7",type:"line",data:[15,20,16,20,30,8]}]};m.setOption(d),window.addEventListener("resize",()=>{m.resize()})},b=()=>{const m=C(a.$refs.homeOvertimeRef),d={grid:{top:50,right:20,bottom:30,left:30},tooltip:{trigger:"axis"},legend:{data:["\u8BA2\u5355\u6570\u91CF","\u8D85\u65F6\u6570\u91CF","\u5728\u7EBF\u6570\u91CF","\u9884\u8B66\u6570\u91CF"],right:13},xAxis:{data:["1\u6708","2\u6708","3\u6708","4\u6708","5\u6708","6\u6708","7\u6708","8\u6708","9\u6708","10\u6708","11\u6708","12\u6708"]},yAxis:[{type:"value",name:"\u6570\u91CF"}],series:[{name:"\u8BA2\u5355\u6570\u91CF",type:"bar",data:[5,20,36,10,10,20,11,13,10,9,17,19]},{name:"\u8D85\u65F6\u6570\u91CF",type:"bar",data:[15,12,26,15,11,16,31,13,5,16,13,15]},{name:"\u5728\u7EBF\u6570\u91CF",type:"line",data:[15,20,16,20,30,8,16,19,12,18,19,14]},{name:"\u9884\u8B66\u6570\u91CF",type:"line",data:[10,10,13,12,15,18,19,10,12,15,11,17]}]};m.setOption(d),window.addEventListener("resize",()=>{m.resize()})};return M(()=>{f(),y(),b()}),w({currentTime:i},j(o))}},Be=`.home-container[data-v-38a3a25e] {
  overflow-x: hidden;
}
.home-container .home-card-item[data-v-38a3a25e] {
  width: 100%;
  height: 103px;
  background: gray;
  border-radius: 4px;
  transition: all ease 0.3s;
}
.home-container .home-card-item[data-v-38a3a25e]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all ease 0.3s;
}
.home-container .home-card-item-box[data-v-38a3a25e] {
  display: flex;
  align-items: center;
  position: relative;
  overflow: hidden;
}
.home-container .home-card-item-box:hover i[data-v-38a3a25e] {
  right: 0px !important;
  bottom: 0px !important;
  transition: all ease 0.3s;
}
.home-container .home-card-item-box i[data-v-38a3a25e] {
  position: absolute;
  right: -10px;
  bottom: -10px;
  font-size: 70px;
  transform: rotate(-30deg);
  transition: all ease 0.3s;
}
.home-container .home-card-item-box .home-card-item-flex[data-v-38a3a25e] {
  padding: 0 20px;
  color: white;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-title[data-v-38a3a25e],
.home-container .home-card-item-box .home-card-item-flex .home-card-item-tip[data-v-38a3a25e] {
  font-size: 13px;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-title-num[data-v-38a3a25e] {
  font-size: 18px;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-tip-num[data-v-38a3a25e] {
  font-size: 13px;
}
.home-container .home-card-first[data-v-38a3a25e] {
  background: white;
  border: 1px solid #ebeef5;
  display: flex;
  align-items: center;
}
.home-container .home-card-first img[data-v-38a3a25e] {
  width: 60px;
  height: 60px;
  border-radius: 100%;
  border: 2px solid var(--color-primary-light-5);
}
.home-container .home-card-first .home-card-first-right[data-v-38a3a25e] {
  flex: 1;
  display: flex;
  flex-direction: column;
}
.home-container .home-card-first .home-card-first-right .home-card-first-right-msg[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}
.home-container .home-monitor[data-v-38a3a25e] {
  height: 200px;
}
.home-container .home-monitor .flex-warp-item[data-v-38a3a25e] {
  width: 50%;
  height: 100px;
  display: flex;
}
.home-container .home-monitor .flex-warp-item .flex-warp-item-box[data-v-38a3a25e] {
  margin: auto;
  height: auto;
  text-align: center;
}
.home-container .home-warning-card[data-v-38a3a25e] {
  height: 292px;
}
.home-container .home-warning-card[data-v-38a3a25e] .el-card {
  height: 100%;
}
.home-container .home-dynamic[data-v-38a3a25e] {
  height: 200px;
}
.home-container .home-dynamic .home-dynamic-item[data-v-38a3a25e] {
  display: flex;
  width: 100%;
  height: 60px;
  overflow: hidden;
}
.home-container .home-dynamic .home-dynamic-item:first-of-type .home-dynamic-item-line i[data-v-38a3a25e] {
  color: orange !important;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-left[data-v-38a3a25e] {
  text-align: right;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-left .home-dynamic-item-left-time2[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-line[data-v-38a3a25e] {
  height: 60px;
  border-right: 2px dashed #dfdfdf;
  margin: 0 20px;
  position: relative;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-line i[data-v-38a3a25e] {
  color: var(--color-primary);
  font-size: 12px;
  position: absolute;
  top: 1px;
  left: -6px;
  transform: rotate(46deg);
  background: white;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right[data-v-38a3a25e] {
  flex: 1;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right .home-dynamic-item-right-title i[data-v-38a3a25e] {
  margin-right: 5px;
  border: 1px solid #dfdfdf;
  width: 20px;
  height: 20px;
  border-radius: 100%;
  padding: 3px 2px 2px;
  text-align: center;
  color: var(--color-primary);
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right .home-dynamic-item-right-label[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}`;const t=H("data-v-38a3a25e");S("data-v-38a3a25e");const G={class:"home-container"},K={class:"home-card-item home-card-first"},Q={class:"flex-margin flex"},V=e("img",{src:"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg"},null,-1),W={class:"home-card-first-right ml15"},Y={class:"flex-margin"},J={class:"home-card-first-right-title"},P=e("div",{class:"home-card-first-right-msg mt5"},"\u8D85\u7EA7\u7BA1\u7406",-1),X={class:"home-card-item-flex"},Z={class:"home-card-item-title pb3"},ee={class:"home-card-item-tip pb3"},te={style:{height:"200px"},ref:"homeLaboratoryRef"},ae={class:"home-monitor"},ne={class:"flex-warp"},oe={class:"flex-warp-item-box"},ie={class:"pl5"},me={class:"mt10"},re={class:"home-dynamic"},de={class:"home-dynamic-item-left"},se={class:"home-dynamic-item-left-time1 mb5"},ce={class:"home-dynamic-item-left-time2"},le=e("div",{class:"home-dynamic-item-line"},[e("i",{class:"iconfont icon-fangkuang"})],-1),ue={class:"home-dynamic-item-right"},he={class:"home-dynamic-item-right-title mb5"},pe=e("i",{class:"el-icon-s-comment"},null,-1),fe={class:"home-dynamic-item-right-label"},_e={style:{height:"200px"},ref:"homeOvertimeRef"};$();const xe=t((a,o,i,f,y,b)=>{const m=h("el-col"),d=h("el-row"),p=h("el-card"),_=h("el-table-column"),A=h("el-table"),E=h("el-scrollbar");return c(),l("div",G,[e(d,{gutter:15},{default:t(()=>[e(m,{sm:6,class:"mb15"},{default:t(()=>[e("div",K,[e("div",Q,[V,e("div",W,[e("div",Y,[e("div",J,r(f.currentTime)+"\uFF0Cadmin\uFF01",1),P])])])])]),_:1}),(c(!0),l(x,null,g(a.topCardItemList,(n,s)=>(c(),l(m,{sm:6,class:"mb15",key:s},{default:t(()=>[e("div",{class:"home-card-item home-card-item-box",style:{background:n.color}},[e("div",X,[e("div",Z,r(n.title),1),e("div",{class:"home-card-item-title-num pb6",id:`titleNum${s+1}`},null,8,["id"]),e("div",ee,r(n.tip),1),e("div",{class:"home-card-item-tip-num",id:`tipNum${s+1}`},null,8,["id"])]),e("i",{class:n.icon,style:{color:n.iconColor}},null,6)],4)]),_:2},1024))),128))]),_:1}),e(d,{gutter:15},{default:t(()=>[e(m,{xs:24,sm:14,md:14,lg:16,xl:16,class:"mb15"},{default:t(()=>[e(p,{shadow:"hover",header:"\u5546\u54C1\u9500\u552E\u60C5\u51B5"},{default:t(()=>[e("div",te,null,512)]),_:1})]),_:1}),e(m,{xs:24,sm:10,md:10,lg:8,xl:8},{default:t(()=>[e(p,{shadow:"hover",header:"\u73AF\u5883\u76D1\u6D4B"},{default:t(()=>[e("div",ae,[e("div",ne,[(c(!0),l(x,null,g(a.environmentList,(n,s)=>(c(),l("div",{class:"flex-warp-item",key:s},[e("div",oe,[e("i",{class:n.icon,style:{color:n.iconColor}},null,6),e("span",ie,r(n.label),1),e("div",me,r(n.value),1)])]))),128))])])]),_:1})]),_:1})]),_:1}),e(d,{gutter:15},{default:t(()=>[e(m,{xs:24,sm:14,md:14,lg:16,xl:16,class:"home-warning-media"},{default:t(()=>[e(p,{shadow:"hover",header:"\u9884\u8B66\u4FE1\u606F",class:"home-warning-card"},{default:t(()=>[e(A,{data:a.tableData.data,style:{width:"100%"},stripe:""},{default:t(()=>[e(_,{prop:"date",label:"\u65F6\u95F4"}),e(_,{prop:"name",label:"\u5B9E\u9A8C\u5BA4\u540D\u79F0"}),e(_,{prop:"address",label:"\u62A5\u8B66\u5185\u5BB9"})]),_:1},8,["data"])]),_:1})]),_:1}),e(m,{xs:24,sm:10,md:10,lg:8,xl:8,class:"home-dynamic-media"},{default:t(()=>[e(p,{shadow:"hover",header:"\u52A8\u6001\u4FE1\u606F"},{default:t(()=>[e("div",re,[e(E,null,{default:t(()=>[(c(!0),l(x,null,g(a.activitiesList,(n,s)=>(c(),l("div",{class:"home-dynamic-item",key:s},[e("div",de,[e("div",se,r(n.time1),1),e("div",ce,r(n.time2),1)]),le,e("div",ue,[e("div",he,[pe,e("span",null,r(n.title),1)]),e("div",fe,r(n.label),1)])]))),128))]),_:1})])]),_:1})]),_:1})]),_:1}),e(d,null,{default:t(()=>[e(m,{xs:24,sm:24,md:24,lg:24,xl:24,class:"mt15"},{default:t(()=>[e(p,{shadow:"hover",header:"\u5C65\u7EA6\u8D85\u65F6\u9884\u8B66"},{default:t(()=>[e("div",_e,null,512)]),_:1})]),_:1})]),_:1})])});v.render=xe,v.__scopeId="data-v-38a3a25e";export default v;
