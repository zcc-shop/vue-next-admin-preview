var F=Object.assign;import{i as B}from"./index.0b65a8e0.js";import{C as c}from"./countUp.min.e806ccee.js";import{i as A,c as E,j as D,k as N,t as z,n as k,b as L,p as I,l as M,r as l,e as d,f as s,m as e,q as o,F as f,s as _,w as j}from"./index.d334a43e.js";import"./vendor.8f964570.js";const O=[{title:"\u4ECA\u65E5\u8BBF\u95EE\u4EBA\u6570",titleNum:"123",tip:"\u5728\u573A\u4EBA\u6570",tipNum:"911",color:"#F95959",iconColor:"#F86C6B",icon:"iconfont icon-jinridaiban"},{title:"\u5B9E\u9A8C\u5BA4\u603B\u6570",titleNum:"123",tip:"\u4F7F\u7528\u4E2D",tipNum:"611",color:"#8595F4",iconColor:"#92A1F4",icon:"iconfont icon-AIshiyanshi"},{title:"\u7533\u8BF7\u4EBA\u6570\uFF08\u6708\uFF09",titleNum:"123",tip:"\u901A\u8FC7\u4EBA\u6570",tipNum:"911",color:"#FEBB50",iconColor:"#FDC566",icon:"iconfont icon-shenqingkaiban"}],R=[{icon:"iconfont icon-yangan",label:"\u70DF\u611F",value:"2.1%OBS/M",iconColor:"#F72B3F"},{icon:"iconfont icon-wendu",label:"\u6E29\u5EA6",value:"30\u2103",iconColor:"#91BFF8"},{icon:"iconfont icon-shidu",label:"\u6E7F\u5EA6",value:"57%RH",iconColor:"#88D565"},{icon:"iconfont icon-zaosheng",label:"\u566A\u58F0",value:"57DB",iconColor:"#FBD4A0"}],S=[{time1:"\u4ECA\u5929",time2:"12:20:30",title:"\u66F4\u540D",label:"\u6B63\u5F0F\u66F4\u540D\u4E3A vue-next-admin"},{time1:"02-17",time2:"12:20:30",title:"\u9875\u9762",label:"\u5B8C\u6210\u5BF9\u9996\u9875\u7684\u5F00\u53D1"},{time1:"02-14",time2:"12:20:30",title:"\u9875\u9762",label:"\u65B0\u589E\u4E2A\u4EBA\u4E2D\u5FC3"}];var x={name:"home",setup(){const{proxy:m}=L(),g=A({topCardItemList:O,environmentList:R,activitiesList:S,tableData:{data:[{date:"2016-05-02",name:"1\u53F7\u5B9E\u9A8C\u5BA4",address:"\u70DF\u611F2.1%OBS/M"},{date:"2016-05-04",name:"2\u53F7\u5B9E\u9A8C\u5BA4",address:"\u6E29\u5EA630\u2103"},{date:"2016-05-01",name:"3\u53F7\u5B9E\u9A8C\u5BA4",address:"\u6E7F\u5EA657%RH"}]}}),v=E(()=>D(new Date)),h=()=>{k(()=>{new c("titleNum1",Math.random()*1e4).start(),new c("titleNum2",Math.random()*1e4).start(),new c("titleNum3",Math.random()*1e4).start(),new c("tipNum1",Math.random()*1e3).start(),new c("tipNum2",Math.random()*1e3).start(),new c("tipNum3",Math.random()*1e3).start()})},y=()=>{const n=B(m.$refs.homeLaboratoryRef),i={grid:{top:50,right:20,bottom:30,left:30},tooltip:{trigger:"axis"},legend:{data:["\u9884\u8D2D\u961F\u5217","\u6700\u65B0\u6210\u4EA4\u4EF7"],right:13},xAxis:{data:["\u886C\u886B","\u7F8A\u6BDB\u886B","\u96EA\u7EBA\u886B","\u88E4\u5B50","\u9AD8\u8DDF\u978B","\u889C\u5B50"]},yAxis:[{type:"value",name:"\u4EF7\u683C"}],series:[{name:"\u9884\u8D2D\u961F\u5217",type:"bar",data:[5,20,36,10,10,20]},{name:"\u6700\u65B0\u6210\u4EA4\u4EF7",type:"line",data:[15,20,16,20,30,8]}]};n.setOption(i),window.addEventListener("resize",()=>{n.resize()})},b=()=>{const n=B(m.$refs.homeOvertimeRef),i={grid:{top:50,right:20,bottom:30,left:30},tooltip:{trigger:"axis"},legend:{data:["\u8BA2\u5355\u6570\u91CF","\u8D85\u65F6\u6570\u91CF","\u5728\u7EBF\u6570\u91CF","\u9884\u8B66\u6570\u91CF"],right:13},xAxis:{data:["1\u6708","2\u6708","3\u6708","4\u6708","5\u6708","6\u6708","7\u6708","8\u6708","9\u6708","10\u6708","11\u6708","12\u6708"]},yAxis:[{type:"value",name:"\u6570\u91CF"}],series:[{name:"\u8BA2\u5355\u6570\u91CF",type:"bar",data:[5,20,36,10,10,20,11,13,10,9,17,19]},{name:"\u8D85\u65F6\u6570\u91CF",type:"bar",data:[15,12,26,15,11,16,31,13,5,16,13,15]},{name:"\u5728\u7EBF\u6570\u91CF",type:"line",data:[15,20,16,20,30,8,16,19,12,18,19,14]},{name:"\u9884\u8B66\u6570\u91CF",type:"line",data:[10,10,13,12,15,18,19,10,12,15,11,17]}]};n.setOption(i),window.addEventListener("resize",()=>{n.resize()})};return N(()=>{h(),y(),b()}),F({currentTime:v},z(g))}},ge=`.home-container[data-v-38a3a25e] {
  overflow-x: hidden;
}
.home-container .home-card-item[data-v-38a3a25e] {
  width: 100%;
  height: 103px;
  background: gray;
  border-radius: 4px;
  transition: all ease 0.3s;
}
.home-container .home-card-item[data-v-38a3a25e]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all ease 0.3s;
}
.home-container .home-card-item-box[data-v-38a3a25e] {
  display: flex;
  align-items: center;
  position: relative;
  overflow: hidden;
}
.home-container .home-card-item-box:hover i[data-v-38a3a25e] {
  right: 0px !important;
  bottom: 0px !important;
  transition: all ease 0.3s;
}
.home-container .home-card-item-box i[data-v-38a3a25e] {
  position: absolute;
  right: -10px;
  bottom: -10px;
  font-size: 70px;
  transform: rotate(-30deg);
  transition: all ease 0.3s;
}
.home-container .home-card-item-box .home-card-item-flex[data-v-38a3a25e] {
  padding: 0 20px;
  color: white;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-title[data-v-38a3a25e],
.home-container .home-card-item-box .home-card-item-flex .home-card-item-tip[data-v-38a3a25e] {
  font-size: 13px;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-title-num[data-v-38a3a25e] {
  font-size: 18px;
}
.home-container .home-card-item-box .home-card-item-flex .home-card-item-tip-num[data-v-38a3a25e] {
  font-size: 13px;
}
.home-container .home-card-first[data-v-38a3a25e] {
  background: white;
  border: 1px solid #ebeef5;
  display: flex;
  align-items: center;
}
.home-container .home-card-first img[data-v-38a3a25e] {
  width: 60px;
  height: 60px;
  border-radius: 100%;
  border: 2px solid var(--color-primary-light-5);
}
.home-container .home-card-first .home-card-first-right[data-v-38a3a25e] {
  flex: 1;
  display: flex;
  flex-direction: column;
}
.home-container .home-card-first .home-card-first-right .home-card-first-right-msg[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}
.home-container .home-monitor[data-v-38a3a25e] {
  height: 200px;
}
.home-container .home-monitor .flex-warp-item[data-v-38a3a25e] {
  width: 50%;
  height: 100px;
  display: flex;
}
.home-container .home-monitor .flex-warp-item .flex-warp-item-box[data-v-38a3a25e] {
  margin: auto;
  height: auto;
  text-align: center;
}
.home-container .home-warning-card[data-v-38a3a25e] {
  height: 292px;
}
.home-container .home-warning-card[data-v-38a3a25e] .el-card {
  height: 100%;
}
.home-container .home-dynamic[data-v-38a3a25e] {
  height: 200px;
}
.home-container .home-dynamic .home-dynamic-item[data-v-38a3a25e] {
  display: flex;
  width: 100%;
  height: 60px;
  overflow: hidden;
}
.home-container .home-dynamic .home-dynamic-item:first-of-type .home-dynamic-item-line i[data-v-38a3a25e] {
  color: orange !important;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-left[data-v-38a3a25e] {
  text-align: right;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-left .home-dynamic-item-left-time2[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-line[data-v-38a3a25e] {
  height: 60px;
  border-right: 2px dashed #dfdfdf;
  margin: 0 20px;
  position: relative;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-line i[data-v-38a3a25e] {
  color: var(--color-primary);
  font-size: 12px;
  position: absolute;
  top: 1px;
  left: -6px;
  transform: rotate(46deg);
  background: white;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right[data-v-38a3a25e] {
  flex: 1;
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right .home-dynamic-item-right-title i[data-v-38a3a25e] {
  margin-right: 5px;
  border: 1px solid #dfdfdf;
  width: 20px;
  height: 20px;
  border-radius: 100%;
  padding: 3px 2px 2px;
  text-align: center;
  color: var(--color-primary);
}
.home-container .home-dynamic .home-dynamic-item .home-dynamic-item-right .home-dynamic-item-right-label[data-v-38a3a25e] {
  font-size: 13px;
  color: gray;
}`;const t=j("data-v-38a3a25e");I("data-v-38a3a25e");const $={class:"home-container"},H={class:"home-card-item home-card-first"},T={class:"flex-margin flex"},U=e("img",{src:"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg"},null,-1),q={class:"home-card-first-right ml15"},G={class:"flex-margin"},K={class:"home-card-first-right-title"},Q=e("div",{class:"home-card-first-right-msg mt5"},"\u8D85\u7EA7\u7BA1\u7406",-1),V={class:"home-card-item-flex"},W={class:"home-card-item-title pb3"},Y={class:"home-card-item-tip pb3"},J={style:{height:"200px"},ref:"homeLaboratoryRef"},P={class:"home-monitor"},X={class:"flex-warp"},Z={class:"flex-warp-item-box"},ee={class:"pl5"},te={class:"mt10"},ae={class:"home-dynamic"},ne={class:"home-dynamic-item-left"},oe={class:"home-dynamic-item-left-time1 mb5"},ie={class:"home-dynamic-item-left-time2"},me=e("div",{class:"home-dynamic-item-line"},[e("i",{class:"iconfont icon-fangkuang"})],-1),re={class:"home-dynamic-item-right"},de={class:"home-dynamic-item-right-title mb5"},se=e("i",{class:"el-icon-s-comment"},null,-1),ce={class:"home-dynamic-item-right-label"},le={style:{height:"200px"},ref:"homeOvertimeRef"};M();const ue=t((m,g,v,h,y,b)=>{const n=l("el-col"),i=l("el-row"),u=l("el-card"),p=l("el-table-column"),w=l("el-table"),C=l("el-scrollbar");return d(),s("div",$,[e(i,{gutter:15},{default:t(()=>[e(n,{sm:6,class:"mb15"},{default:t(()=>[e("div",H,[e("div",T,[U,e("div",q,[e("div",G,[e("div",K,o(h.currentTime)+"\uFF0Cadmin\uFF01",1),Q])])])])]),_:1}),(d(!0),s(f,null,_(m.topCardItemList,(a,r)=>(d(),s(n,{sm:6,class:"mb15",key:r},{default:t(()=>[e("div",{class:"home-card-item home-card-item-box",style:{background:a.color}},[e("div",V,[e("div",W,o(a.title),1),e("div",{class:"home-card-item-title-num pb6",id:`titleNum${r+1}`},null,8,["id"]),e("div",Y,o(a.tip),1),e("div",{class:"home-card-item-tip-num",id:`tipNum${r+1}`},null,8,["id"])]),e("i",{class:a.icon,style:{color:a.iconColor}},null,6)],4)]),_:2},1024))),128))]),_:1}),e(i,{gutter:15},{default:t(()=>[e(n,{xs:24,sm:14,md:14,lg:16,xl:16,class:"mb15"},{default:t(()=>[e(u,{shadow:"hover",header:"\u5546\u54C1\u9500\u552E\u60C5\u51B5"},{default:t(()=>[e("div",J,null,512)]),_:1})]),_:1}),e(n,{xs:24,sm:10,md:10,lg:8,xl:8},{default:t(()=>[e(u,{shadow:"hover",header:"\u73AF\u5883\u76D1\u6D4B"},{default:t(()=>[e("div",P,[e("div",X,[(d(!0),s(f,null,_(m.environmentList,(a,r)=>(d(),s("div",{class:"flex-warp-item",key:r},[e("div",Z,[e("i",{class:a.icon,style:{color:a.iconColor}},null,6),e("span",ee,o(a.label),1),e("div",te,o(a.value),1)])]))),128))])])]),_:1})]),_:1})]),_:1}),e(i,{gutter:15},{default:t(()=>[e(n,{xs:24,sm:14,md:14,lg:16,xl:16,class:"home-warning-media"},{default:t(()=>[e(u,{shadow:"hover",header:"\u9884\u8B66\u4FE1\u606F",class:"home-warning-card"},{default:t(()=>[e(w,{data:m.tableData.data,style:{width:"100%"},stripe:""},{default:t(()=>[e(p,{prop:"date",label:"\u65F6\u95F4"}),e(p,{prop:"name",label:"\u5B9E\u9A8C\u5BA4\u540D\u79F0"}),e(p,{prop:"address",label:"\u62A5\u8B66\u5185\u5BB9"})]),_:1},8,["data"])]),_:1})]),_:1}),e(n,{xs:24,sm:10,md:10,lg:8,xl:8,class:"home-dynamic-media"},{default:t(()=>[e(u,{shadow:"hover",header:"\u52A8\u6001\u4FE1\u606F"},{default:t(()=>[e("div",ae,[e(C,null,{default:t(()=>[(d(!0),s(f,null,_(m.activitiesList,(a,r)=>(d(),s("div",{class:"home-dynamic-item",key:r},[e("div",ne,[e("div",oe,o(a.time1),1),e("div",ie,o(a.time2),1)]),me,e("div",re,[e("div",de,[se,e("span",null,o(a.title),1)]),e("div",ce,o(a.label),1)])]))),128))]),_:1})])]),_:1})]),_:1})]),_:1}),e(i,null,{default:t(()=>[e(n,{xs:24,sm:24,md:24,lg:24,xl:24,class:"mt15"},{default:t(()=>[e(u,{shadow:"hover",header:"\u5C65\u7EA6\u8D85\u65F6\u9884\u8B66"},{default:t(()=>[e("div",le,null,512)]),_:1})]),_:1})]),_:1})])});x.render=ue,x.__scopeId="data-v-38a3a25e";export default x;
