var g=Object.assign;import{Q as S,u as y,A as k,i as x,c as B,t as N,M as w,N as U,a$ as I,C as D,b as F,p as E,l as T,r,e as P,f as A,m as e,T as j,P as L,S as R,B as i,q as O,w as H}from"./index.d334a43e.js";import{s as C}from"./screenfull.77def122.js";import q from"./userNews.5c9ffe51.js";import M from"./search.be172493.js";import"./vendor.8f964570.js";var p={name:"layoutBreadcrumbUser",components:{UserNews:q,Search:M},setup(){const{proxy:t}=F(),o=S(),f=y(),n=k(),c=x({isScreenfull:!1,isShowUserNewsPopover:!1}),b=B(()=>f.state.userInfos.userInfos);return g({getUserInfos:b,onLayoutSetingClick:()=>{t.mittBus.emit("openSetingsDrawer")},onHandleCommandClick:d=>{d==="logOut"?U({closeOnClickModal:!1,closeOnPressEscape:!1,title:"\u63D0\u793A",message:"\u6B64\u64CD\u4F5C\u5C06\u9000\u51FA\u767B\u5F55, \u662F\u5426\u7EE7\u7EED?",showCancelButton:!0,confirmButtonText:"\u786E\u5B9A",cancelButtonText:"\u53D6\u6D88",beforeClose:(m,u,a)=>{m==="confirm"?(u.confirmButtonLoading=!0,u.confirmButtonText="\u9000\u51FA\u4E2D",setTimeout(()=>{a(),setTimeout(()=>{u.confirmButtonLoading=!1},300)},700)):a()}}).then(()=>{I(),D(),o.push("/login"),setTimeout(()=>{w.success("\u5B89\u5168\u9000\u51FA\u6210\u529F\uFF01")},300)}).catch(()=>{}):o.push(d)},onScreenfullClick:()=>{if(!C.isEnabled)return w.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;C.toggle(),c.isScreenfull=!c.isScreenfull},onSearchClick:()=>{n.value.openSearch()},searchRef:n},N(c))}},ie=`.layout-navbars-breadcrumb-user[data-v-4e364833] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex: 1;
}
.layout-navbars-breadcrumb-user-link[data-v-4e364833] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-4e364833] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-4e364833] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-4e364833]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-4e364833] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-badge__content.is-fixed {
  top: 12px;
}`;const s=H("data-v-4e364833");E("data-v-4e364833");const V={class:"layout-navbars-breadcrumb-user"},z=e("i",{class:"el-icon-search",title:"\u83DC\u5355\u641C\u7D22"},null,-1),Q=e("i",{class:"icon-skin iconfont",title:"\u5E03\u5C40\u914D\u7F6E"},null,-1),G={class:"layout-navbars-breadcrumb-user-icon"},J=e("i",{class:"el-icon-bell",title:"\u6D88\u606F"},null,-1),K={class:"layout-navbars-breadcrumb-user-link"},W=e("i",{class:"el-icon-arrow-down el-icon--right"},null,-1),X=i("\u9996\u9875"),Y=i("\u4E2A\u4EBA\u4E2D\u5FC3"),Z=i("404"),$=i("401"),ee=i("\u9000\u51FA\u767B\u5F55");T();const oe=s((t,o,f,n,c,b)=>{const v=r("el-badge"),_=r("UserNews"),h=r("el-popover"),l=r("el-dropdown-item"),d=r("el-dropdown-menu"),m=r("el-dropdown"),u=r("Search");return P(),A("div",V,[e("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:o[1]||(o[1]=(...a)=>n.onSearchClick&&n.onSearchClick(...a))},[z]),e("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:o[2]||(o[2]=(...a)=>n.onLayoutSetingClick&&n.onLayoutSetingClick(...a))},[Q]),e("div",G,[e(h,{placement:"bottom",trigger:"click",visible:t.isShowUserNewsPopover,"onUpdate:visible":o[4]||(o[4]=a=>t.isShowUserNewsPopover=a),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:s(()=>[e(v,{"is-dot":!0,onClick:o[3]||(o[3]=a=>t.isShowUserNewsPopover=!t.isShowUserNewsPopover)},{default:s(()=>[J]),_:1})]),default:s(()=>[e(j,{name:"el-zoom-in-top"},{default:s(()=>[L(e(_,null,null,512),[[R,t.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),e("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:o[5]||(o[5]=(...a)=>n.onScreenfullClick&&n.onScreenfullClick(...a))},[e("i",{class:["iconfont",t.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:t.isScreenfull?"\u5F00\u5168\u5C4F":"\u5173\u5168\u5C4F"},null,10,["title"])]),e(m,{"show-timeout":70,"hide-timeout":50,onCommand:n.onHandleCommandClick},{dropdown:s(()=>[e(d,null,{default:s(()=>[e(l,{command:"/home"},{default:s(()=>[X]),_:1}),e(l,{command:"/personal"},{default:s(()=>[Y]),_:1}),e(l,{command:"/404"},{default:s(()=>[Z]),_:1}),e(l,{command:"/401"},{default:s(()=>[$]),_:1}),e(l,{divided:"",command:"logOut"},{default:s(()=>[ee]),_:1})]),_:1})]),default:s(()=>[e("span",K,[e("img",{src:n.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),i(" "+O(n.getUserInfos.userName===""?"test":n.getUserInfos.userName)+" ",1),W])]),_:1},8,["onCommand"]),e(u,{ref:"searchRef"},null,512)])});p.render=oe,p.__scopeId="data-v-4e364833";export default p;
