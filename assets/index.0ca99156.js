import{u as m}from"./index.7a634872.js";import p from"./index.26faf582.js";import _ from"./tagsView.6a2dc704.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as h,M as w,F as v}from"./vendor.6ad2dd02.js";import"./breadcrumb.7d645e1b.js";import"./user.d2d84e47.js";import"./screenfull.26d2e821.js";import"./userNews.cdc07c20.js";import"./search.8c0d2160.js";import"./index.a97aa27c.js";import"./horizontal.4723bcb8.js";import"./subItem.c2baf03f.js";import"./contextmenu.e2374259.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=v();f("data-v-fde5c75e");const x={class:"layout-navbars-container"};l();const b=j((t,c,o,e,g,y)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",x,[h(d),e.setShowTagsView?(r(),n(i,{key:0})):w("",!0)])});s.render=b,s.__scopeId="data-v-fde5c75e";export default s;
