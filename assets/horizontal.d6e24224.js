var h=Object.assign;import{v as C,x as H,u as I,i as M,c as R,k as B,b1 as A,t as j,n as v,b as D,p as E,l as F,r as i,e as c,f as u,m as o,F as x,s as q,q as g,b2 as N,B as $,b6 as O,w as P}from"./index.0859f0ef.js";import T from"./subItem.2b60b063.js";import"./vendor.8f964570.js";var w=C({name:"navMenuHorizontal",components:{SubItem:T},props:{menuList:{type:Array,default:()=>[]}},setup(r){const{proxy:l}=D(),L=H(),_=I(),d=M({defaultActive:null}),k=R(()=>r.menuList),S=t=>{const n=t.wheelDelta||-t.deltaY*40;l.$refs.elMenuHorizontalScrollRef.$refs.wrap.scrollLeft=l.$refs.elMenuHorizontalScrollRef.$refs.wrap.scrollLeft+n/4},b=()=>{v(()=>{let t=document.querySelector(".el-menu.el-menu--horizontal li.is-active");if(!t)return!1;l.$refs.elMenuHorizontalScrollRef.$refs.wrap.scrollLeft=t.offsetLeft})},m=t=>{const n=t.split("/");_.state.themeConfig.themeConfig.layout==="classic"?d.defaultActive=`/${n[1]}`:d.defaultActive=t},f=t=>t.filter(n=>!n.meta.isHide).map(n=>(n=Object.assign({},n),n.children&&(n.children=f(n.children)),n)),z=t=>{const n=t.split("/");let p={};return f(_.state.routesList.routesList).map((s,y)=>{s.path===`/${n[1]}`&&(s.k=y,p.item=[h({},s)],p.children=[h({},s)],s.children&&(p.children=s.children))}),p},e=t=>{l.mittBus.emit("setSendClassicChildren",z(t))};return B(()=>{b(),m(L.path)}),A(t=>{m(t.path),l.mittBus.emit("onMenuClick")}),h({menuLists:k,onElMenuHorizontalScroll:S,onHorizontalSelect:e},j(d))}}),K=`.el-menu-horizontal-warp[data-v-34aa63d8] {
  flex: 1;
  overflow: hidden;
  margin-right: 30px;
}
.el-menu-horizontal-warp[data-v-34aa63d8] .el-scrollbar__bar.is-vertical {
  display: none;
}
.el-menu-horizontal-warp[data-v-34aa63d8] a {
  width: 100%;
}
.el-menu-horizontal-warp .el-menu.el-menu--horizontal[data-v-34aa63d8] {
  display: flex;
  height: 100%;
  width: 100%;
  box-sizing: border-box;
}`;const a=P("data-v-34aa63d8");E("data-v-34aa63d8");const V={class:"el-menu-horizontal-warp"};F();const W=a((r,l,L,_,d,k)=>{const S=i("SubItem"),b=i("el-submenu"),m=i("el-menu-item"),f=i("el-menu"),z=i("el-scrollbar");return c(),u("div",V,[o(z,{onWheel:O(r.onElMenuHorizontalScroll,["prevent"]),ref:"elMenuHorizontalScrollRef"},{default:a(()=>[o(f,{router:"","default-active":r.defaultActive,"background-color":"transparent",mode:"horizontal",onSelect:r.onHorizontalSelect},{default:a(()=>[(c(!0),u(x,null,q(r.menuLists,e=>(c(),u(x,null,[e.children&&e.children.length>0?(c(),u(b,{index:e.path,key:e.path},{title:a(()=>[o("i",{class:e.meta.icon?e.meta.icon:""},null,2),o("span",null,g(e.meta.title),1)]),default:a(()=>[o(S,{chil:e.children},null,8,["chil"])]),_:2},1032,["index"])):(c(),u(m,{index:e.path,key:e.path},N({_:2},[!e.meta.isLink||e.meta.isLink&&e.meta.isIframe?{name:"title",fn:a(()=>[o("i",{class:e.meta.icon?e.meta.icon:""},null,2),$(" "+g(e.meta.title),1)])}:{name:"title",fn:a(()=>[o("a",{href:e.meta.isLink,target:"_blank"},[o("i",{class:e.meta.icon?e.meta.icon:""},null,2),$(" "+g(e.meta.title),1)],8,["href"])])}]),1032,["index"]))],64))),256))]),_:1},8,["default-active","onSelect"])]),_:1},8,["onWheel"])])});w.render=W,w.__scopeId="data-v-34aa63d8";export default w;
