import{u as m}from"./index.fc7898a5.js";import p from"./index.085529c5.js";import _ from"./tagsView.8d5bb221.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.a5c5c942.js";import"./breadcrumb.16e2fa85.js";import"./user.c566e69d.js";import"./screenfull.52ff4b74.js";import"./userNews.41103212.js";import"./search.6fdeb807.js";import"./index.01ee6d6b.js";import"./horizontal.d751f9a9.js";import"./subItem.ef5a92f9.js";import"./contextmenu.31d06cd8.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=v();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=b((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
