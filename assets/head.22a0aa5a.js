var p=Object.defineProperty;var l=Object.prototype.hasOwnProperty;var o=Object.getOwnPropertySymbols,c=Object.prototype.propertyIsEnumerable;var r=(e,t,a)=>t in e?p(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,s=(e,t)=>{for(var a in t||(t={}))l.call(t,a)&&r(e,a,t[a]);if(o)for(var a of o(t))c.call(t,a)&&r(e,a,t[a]);return e};import{i as m,o as u,a as v,t as b,aD as d,p as f,l as g,e as _,f as x,m as n,q as h,w}from"./index.8eeb5c9c.js";import"./vendor.f1659326.js";var i={name:"chartHead",setup(){const e=m({time:{txt:"",fun:0}}),t=()=>{e.time.txt=d(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ"),e.time.fun=window.setInterval(()=>{e.time.txt=d(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ")},1e3)};return u(()=>{t()}),v(()=>{window.clearInterval(e.time.fun)}),s({},b(e))}},B=`.big-data-up[data-v-692a7ee7] {
  height: 55px;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 15px;
  color: var(--color-primary);
  overflow: hidden;
  position: relative;
}
.big-data-up .up-left[data-v-692a7ee7] {
  position: absolute;
}
.big-data-up .up-center[data-v-692a7ee7] {
  width: 100%;
  display: flex;
  justify-content: center;
  font-size: 18px;
  letter-spacing: 5px;
  background-image: -webkit-linear-gradient(left, var(--color-primary), var(--color-primary-light-1) 25%, var(--color-primary) 50%, var(--color-primary-light-1) 75%, var(--color-primary));
  -webkit-text-fill-color: transparent;
  -webkit-background-clip: text;
  background-clip: text;
  background-size: 200% 100%;
  -webkit-animation: masked-animation-data-v-b02d8052 4s linear infinite;
  animation: masked-animation-data-v-b02d8052 4s linear infinite;
  -webkit-box-reflect: below -2px -webkit-gradient(linear, left top, left bottom, from(transparent), to(rgba(255, 255, 255, 0.1)));
  position: relative;
  position: relative;
}
@keyframes masked-animation-692a7ee7 {
0%[data-v-692a7ee7] {
    background-position: 0 0;
}
100%[data-v-692a7ee7] {
    background-position: -100% 0;
}
}
.big-data-up .up-center[data-v-692a7ee7]::after {
  content: "";
  width: 250px;
  position: absolute;
  bottom: -15px;
  left: 50%;
  transform: translateX(-50%);
  border: 1px transparent solid;
  border-image: linear-gradient(to right, var(--color-primary-light-9), var(--color-primary)) 1 10;
}
.big-data-up .up-center span[data-v-692a7ee7] {
  cursor: pointer;
}`;const k=w("data-v-692a7ee7");f("data-v-692a7ee7");const y={class:"big-data-up mb15"},Q={class:"up-left"},S=n("i",{class:"el-icon-time mr5"},null,-1),Y=n("div",{class:"up-center"},[n("span",null,"\u667A\u6167\u519C\u4E1A\u7CFB\u7EDF\u5E73\u53F0")],-1);g();const I=k((e,t,a,W,D,H)=>(_(),x("div",y,[n("div",Q,[S,n("span",null,h(e.time.txt),1)]),Y])));i.render=I,i.__scopeId="data-v-692a7ee7";export default i;
