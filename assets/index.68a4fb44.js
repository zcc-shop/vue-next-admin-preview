var w=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(e,n,o)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,p=(e,n)=>{for(var o in n||(n={}))u.call(n,o)&&f(e,o,n[o]);if(d)for(var o of d(n))b.call(n,o)&&f(e,o,n[o]);return e};import{i as x}from"./getStyleSheets.aba6ce1e.js";import{a as g,o as y,t as I,p as S,d as L,e as s,f as r,h as l,i as t,F as $,Y as k,l as j,q as B}from"./vendor.a5c5c942.js";var i={name:"pagesElement",setup(){const e=g({sheetsIconList:[]}),n=()=>{x.ele().then(o=>e.sheetsIconList=o)};return y(()=>{n()}),p({},I(e))}},A=`.element-container .iconfont-row[data-v-140f7842] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.element-container .iconfont-row .el-col[data-v-140f7842]:nth-last-child(1),
.element-container .iconfont-row .el-col[data-v-140f7842]:nth-last-child(2) {
  display: none;
}
.element-container .iconfont-row .iconfont-warp[data-v-140f7842] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-140f7842] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-140f7842] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-140f7842] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-140f7842] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=B();S("data-v-140f7842");const F={class:"element-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},C={class:"iconfont-warp-value"},D={class:"iconfont-warp-label mt10"};L();const E=a((e,n,o,G,M,N)=>{const _=s("el-col"),h=s("el-row"),v=s("el-card");return r(),l("div",F,[t(v,{shadow:"hover",header:`element plus \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-2}\u4E2A`},{default:a(()=>[t(h,{class:"iconfont-row"},{default:a(()=>[(r(!0),l($,null,k(e.sheetsIconList,(c,m)=>(r(),l(_,{xs:12,sm:8,md:6,lg:4,xl:2,key:m},{default:a(()=>[t("div",q,[t("div",z,[t("div",C,[t("i",{class:c},null,2)]),t("div",D,j(c),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});i.render=E,i.__scopeId="data-v-140f7842";export default i;
