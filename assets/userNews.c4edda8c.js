var x=Object.defineProperty;var y=Object.prototype.hasOwnProperty;var l=Object.getOwnPropertySymbols,w=Object.prototype.propertyIsEnumerable;var d=(e,n,t)=>n in e?x(e,n,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[n]=t,b=(e,n)=>{for(var t in n||(n={}))y.call(n,t)&&d(e,t,n[t]);if(l)for(var t of l(n))w.call(n,t)&&d(e,t,n[t]);return e};import{r as F,t as h,l as _,m as g,q as f,y as a,z as s,A as r,D as u,M as p,L as C,V as k,F as E}from"./vendor.ad170e2b.js";var c={name:"layoutBreadcrumbUserNews",setup(){const e=F({newsList:[{label:"\u5173\u4E8E\u7248\u672C\u53D1\u5E03\u7684\u901A\u77E5",value:"vue-next-admin\uFF0C\u57FA\u4E8E vue3 + CompositionAPI + typescript + vite + element plus\uFF0C\u6B63\u5F0F\u53D1\u5E03\u65F6\u95F4\uFF1A2021\u5E7402\u670828\u65E5\uFF01",time:"2020-12-08"},{label:"\u5173\u4E8E\u5B66\u4E60\u4EA4\u6D41\u7684\u901A\u77E5",value:"QQ\u7FA4\u53F7\u7801 665452019\uFF0C\u6B22\u8FCE\u5C0F\u4F19\u4F34\u5165\u7FA4\u5B66\u4E60\u4EA4\u6D41\u63A2\u8BA8\uFF01",time:"2020-12-08"}]});return b({onAllReadClick:()=>{e.newsList=[]},onGoToGiteeClick:()=>{window.open("https://gitee.com/lyt-top/vue-next-admin")}},h(e))}},j=`.layout-navbars-breadcrumb-user-news .head-box[data-v-6749a4c0] {
  display: flex;
  border-bottom: 1px solid #ebeef5;
  box-sizing: border-box;
  color: #333333;
  justify-content: space-between;
  height: 35px;
  align-items: center;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6749a4c0] {
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6749a4c0]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news .content-box[data-v-6749a4c0] {
  font-size: 13px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6749a4c0] {
  padding-top: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6749a4c0]:last-of-type {
  padding-bottom: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-msg[data-v-6749a4c0] {
  color: #999999;
  margin-top: 5px;
  margin-bottom: 5px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-time[data-v-6749a4c0] {
  color: #999999;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6749a4c0] {
  height: 35px;
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top: 1px solid #ebeef5;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6749a4c0]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news[data-v-6749a4c0] .el-empty__description p {
  font-size: 13px;
}`;const A=E();_("data-v-6749a4c0");const B={class:"layout-navbars-breadcrumb-user-news"},G={class:"head-box"},L={class:"head-box-title"},D={class:"content-box"},z={class:"content-box-msg"},I={class:"content-box-time"};g();const R=A((e,n,t,i,T,N)=>{const m=f("el-empty");return a(),s("div",B,[r("div",G,[r("div",L,u(e.$t("message.user.newTitle")),1),e.newsList.length>0?(a(),s("div",{key:0,class:"head-box-btn",onClick:n[1]||(n[1]=(...o)=>i.onAllReadClick&&i.onAllReadClick(...o))},u(e.$t("message.user.newBtn")),1)):p("",!0)]),r("div",D,[e.newsList.length>0?(a(!0),s(C,{key:0},k(e.newsList,(o,v)=>(a(),s("div",{class:"content-box-item",key:v},[r("div",null,u(o.label),1),r("div",z,u(o.value),1),r("div",I,u(o.time),1)]))),128)):(a(),s(m,{key:1,description:e.$t("message.user.newDesc")},null,8,["description"]))]),e.newsList.length>0?(a(),s("div",{key:0,class:"foot-box",onClick:n[2]||(n[2]=(...o)=>i.onGoToGiteeClick&&i.onGoToGiteeClick(...o))},u(e.$t("message.user.newGo")),1)):p("",!0)])});c.render=R,c.__scopeId="data-v-6749a4c0";export default c;
