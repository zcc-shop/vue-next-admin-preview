import{U as l,p as d,d as m,e as f,f as c,h as p,i as e,l as n,k as _,q as h}from"./vendor.9b51066d.js";var a={name:"404",setup(){const t=l();return{onGoHome:()=>{t.push("/")}}}},F=`.error[data-v-17a18ba2] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-17a18ba2] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-17a18ba2] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-17a18ba2] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-17a18ba2] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-17a18ba2] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-17a18ba2] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-17a18ba2] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-17a18ba2] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-17a18ba2] {
  width: 100%;
  height: 100%;
}`;const o=h();d("data-v-17a18ba2");const u={class:"error"},v={class:"error-flex"},g={class:"left"},x={class:"left-item"},b=e("div",{class:"left-item-animation left-item-num"},"404",-1),y={class:"left-item-animation left-item-title"},w={class:"left-item-animation left-item-msg"},k={class:"left-item-animation left-item-btn"},$=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/404.png"})],-1);m();const I=o((t,r,S,i,z,B)=>{const s=f("el-button");return c(),p("div",u,[e("div",v,[e("div",g,[e("div",x,[b,e("div",y,n(t.$t("message.notFound.foundTitle")),1),e("div",w,n(t.$t("message.notFound.foundMsg")),1),e("div",k,[e(s,{type:"primary",round:"",onClick:i.onGoHome},{default:o(()=>[_(n(t.$t("message.notFound.foundBtn")),1)]),_:1},8,["onClick"])])])]),$])])});a.render=I,a.__scopeId="data-v-17a18ba2";export default a;
