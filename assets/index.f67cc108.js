import{u as m}from"./index.76040d67.js";import p from"./index.36ae59d7.js";import _ from"./tagsView.8abf2d63.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.9b51066d.js";import"./breadcrumb.d8182939.js";import"./user.4d2751ae.js";import"./screenfull.75a325ae.js";import"./userNews.d597db99.js";import"./search.b0009726.js";import"./index.a0887057.js";import"./horizontal.845a44d4.js";import"./subItem.c2830d89.js";import"./contextmenu.fc2234cb.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=v();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=b((t,c,o,e,g,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",j,[h(d),e.setShowTagsView?(r(),n(i,{key:0})):w("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
