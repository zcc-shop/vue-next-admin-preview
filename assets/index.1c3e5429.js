var x=Object.defineProperty;var f=Object.getOwnPropertySymbols;var u=Object.prototype.hasOwnProperty,y=Object.prototype.propertyIsEnumerable;var w=(s,a,t)=>a in s?x(s,a,{enumerable:!0,configurable:!0,writable:!0,value:t}):s[a]=t,h=(s,a)=>{for(var t in a||(a={}))u.call(a,t)&&w(s,t,a[t]);if(f)for(var t of f(a))y.call(a,t)&&w(s,t,a[t]);return s};import{a as b,t as z,p as k,d as I,e as c,a1 as S,f as v,h as g,i as e,w as o,F as $,Y as D,l as B,q as F,k as r}from"./vendor.f07206bb.js";var d={name:"pagesWaves",setup(){const s=b({});return h({},z(s))}},de=`.preview-container .flex-warp[data-v-355590a0] {
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  margin: 0 -5px;
}
.preview-container .flex-warp .flex-warp-item[data-v-355590a0] {
  padding: 5px;
}
.preview-container .flex-warp .flex-warp-item .flex-warp-item-box[data-v-355590a0] {
  width: 100%;
  height: 100%;
}
.preview-container .waterfall-first[data-v-355590a0] {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(188px, 1fr));
  grid-gap: 0.25em;
  grid-auto-flow: row dense;
  grid-auto-rows: 20px;
}
.preview-container .waterfall-first .waterfall-first-item[data-v-355590a0] {
  width: 100%;
  background: var(--color-primary);
  color: #ffffff;
  transition: all 0.3s ease;
  border-radius: 3px;
}
.preview-container .waterfall-first .waterfall-first-item[data-v-355590a0]:nth-of-type(3n + 1) {
  grid-row: auto/span 5;
}
.preview-container .waterfall-first .waterfall-first-item[data-v-355590a0]:nth-of-type(3n + 2) {
  grid-row: auto/span 6;
}
.preview-container .waterfall-first .waterfall-first-item[data-v-355590a0]:nth-of-type(3n + 3) {
  grid-row: auto/span 8;
}
.preview-container .waterfall-first .waterfall-first-item[data-v-355590a0]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease;
  cursor: pointer;
}`;const n=F();k("data-v-355590a0");const N={class:"preview-container"},V=r('\u53EF\u9009\u53C2\u6570 v-waves=" |light|red|orange|purple|green|teal"'),j={class:"flex-warp"},q={class:"flex-warp-item"},C={class:"flex-warp-item-box"},L=r("\u9ED8\u8BA4\u6548\u679C"),R={class:"flex-warp-item"},T={class:"flex-warp-item-box"},W=r("light \u6548\u679C"),Y={class:"flex-warp-item"},A={class:"flex-warp-item-box"},E=r("red \u6548\u679C"),G={class:"flex-warp-item"},H={class:"flex-warp-item-box"},J=r("orange \u6548\u679C"),K={class:"flex-warp-item"},M={class:"flex-warp-item-box"},O=r("purple \u6548\u679C"),P={class:"flex-warp-item"},Q={class:"flex-warp-item-box"},U=r("green \u6548\u679C"),X={class:"flex-warp-item"},Z={class:"flex-warp-item-box"},ee=r("teal \u6548\u679C"),ae={class:"waterfall-first"},te={class:"w100 h100 flex"},ne={class:"flex-margin"};I();const se=n((s,a,t,ie,oe,re)=>{const m=c("el-row"),l=c("el-button"),p=c("el-card"),i=S("waves");return v(),g("div",N,[e(p,{shadow:"hover",header:"\u6CE2\u6D6A\u6307\u4EE4\u6548\u679C\uFF08v-waves\uFF09\u4F5C\u7528\u4E8E btn"},{default:n(()=>[e(m,{class:"mb10",style:{color:"#808080"}},{default:n(()=>[V]),_:1}),e("div",j,[e("div",q,[e("div",C,[o(e(l,{size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[L]),_:1},512),[[i]])])]),e("div",R,[e("div",T,[o(e(l,{type:"primary",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[W]),_:1},512),[[i,"light"]])])]),e("div",Y,[e("div",A,[o(e(l,{type:"success",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[E]),_:1},512),[[i,"red"]])])]),e("div",G,[e("div",H,[o(e(l,{type:"info",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[J]),_:1},512),[[i,"orange"]])])]),e("div",K,[e("div",M,[o(e(l,{type:"warning",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[O]),_:1},512),[[i,"purple"]])])]),e("div",P,[e("div",Q,[o(e(l,{type:"danger",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[U]),_:1},512),[[i,"green"]])])]),e("div",X,[e("div",Z,[o(e(l,{type:"primary",size:"small",icon:"iconfont icon-bolangnengshiyanchang"},{default:n(()=>[ee]),_:1},512),[[i,"teal"]])])])])]),_:1}),e(p,{shadow:"hover",header:"\u6CE2\u6D6A\u6307\u4EE4\u6548\u679C\uFF08v-waves\uFF09\u4F5C\u7528\u4E8E div",class:"mt15"},{default:n(()=>[e("div",ae,[(v(),g($,null,D(12,_=>o(e("div",{class:"waterfall-first-item",key:_},[e("div",te,[e("span",ne,B(_),1)])],512),[[i]])),64))])]),_:1})])});d.render=se,d.__scopeId="data-v-355590a0";export default d;
