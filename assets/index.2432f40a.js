import{u as m}from"./index.d9a617f7.js";import p from"./index.21111549.js";import _ from"./tagsView.5bb08384.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as b,M as h,F as w}from"./vendor.3f76c63b.js";import"./breadcrumb.4660b8a3.js";import"./user.b81ffbe5.js";import"./screenfull.302381a4.js";import"./userNews.59e9f8bc.js";import"./search.0863ecc2.js";import"./index.b705048a.js";import"./horizontal.78cc7554.js";import"./subItem.355ff8bc.js";import"./contextmenu.ea494988.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,y)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[b(i),e.setShowTagsView?(r(),n(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
