var u=Object.defineProperty;var m=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(o,n,e)=>n in o?u(o,n,{enumerable:!0,configurable:!0,writable:!0,value:e}):o[n]=e,p=(o,n)=>{for(var e in n||(n={}))m.call(n,e)&&f(o,e,n[e]);if(d)for(var e of d(n))b.call(n,e)&&f(o,e,n[e]);return o};import{i as x}from"./getStyleSheets.aba6ce1e.js";import{a as g,o as I,t as y,p as S,d as L,e as s,f as i,h as r,i as a,F as $,Y as k,l as j,q as B}from"./vendor.a5c5c942.js";var c={name:"pagesIocnfont",setup(){const o=g({sheetsIconList:[]}),n=()=>{x.ali().then(e=>o.sheetsIconList=e)};return I(()=>{n()}),p({},y(o))}},E=`.iconfont-container .iconfont-row[data-v-556aa12a] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.iconfont-container .iconfont-row .iconfont-warp[data-v-556aa12a] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.iconfont-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-556aa12a] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-556aa12a] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-556aa12a] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.iconfont-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-556aa12a] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const t=B();S("data-v-556aa12a");const F={class:"iconfont-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},C={class:"iconfont-warp-value"},D={class:"iconfont-warp-label mt10"};L();const G=t((o,n,e,M,N,R)=>{const _=s("el-col"),h=s("el-row"),v=s("el-card");return i(),r("div",F,[a(v,{shadow:"hover",header:`iconfont \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${o.sheetsIconList.length}\u4E2A`},{default:t(()=>[a(h,{class:"iconfont-row"},{default:t(()=>[(i(!0),r($,null,k(o.sheetsIconList,(l,w)=>(i(),r(_,{xs:12,sm:8,md:6,lg:4,xl:2,key:w},{default:t(()=>[a("div",q,[a("div",z,[a("div",C,[a("i",{class:[l,"iconfont"]},null,2)]),a("div",D,j(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});c.render=G,c.__scopeId="data-v-556aa12a";export default c;
