import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as l,h as f,w as h}from"./index.a97d6999.js";import w from"./index.2dc75b2a.js";import v from"./tagsView.68313ea9.js";import"./vendor.9092e083.js";import"./breadcrumb.3d149676.js";import"./user.ad82ec79.js";import"./screenfull.31c31425.js";import"./userNews.00b352e8.js";import"./search.486e69b2.js";import"./index.0887f969.js";import"./horizontal.9216ef9f.js";import"./subItem.1cdc517b.js";import"./contextmenu.69e93402.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h();_("data-v-fde5c75e");const x={class:"layout-navbars-container"};u();const b=j((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[l(i),e.setShowTagsView?(r(),n(d,{key:0})):f("",!0)])});s.render=b,s.__scopeId="data-v-fde5c75e";export default s;
