var d=Object.assign;import{i as m}from"./getStyleSheets.9f9da31e.js";import{i as v,k as h,t as b,p as u,l as x,r as t,e as a,f as s,m as o,F as g,s as y,q as I,w as S}from"./index.9af39cde.js";import"./vendor.8f964570.js";var r={name:"awesome",setup(){const e=v({sheetsIconList:[]}),i=()=>{m.awe().then(c=>e.sheetsIconList=c)};return h(()=>{i()}),d({},b(e))}},R=`.awesome-container .iconfont-row[data-v-596b4114] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.awesome-container .iconfont-row .el-col[data-v-596b4114]:nth-child(-n+24) {
  display: none;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-596b4114] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-596b4114] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-596b4114] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-596b4114] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-596b4114] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const n=S("data-v-596b4114");u("data-v-596b4114");const L={class:"awesome-container"},$={class:"iconfont-warp"},k={class:"flex-margin"},j={class:"iconfont-warp-value"},B={class:"iconfont-warp-label mt10"};x();const F=n((e,i,c,q,z,C)=>{const p=t("el-col"),f=t("el-row"),w=t("el-card");return a(),s("div",L,[o(w,{shadow:"hover",header:`fontawesome \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-24}\u4E2A`},{default:n(()=>[o(f,{class:"iconfont-row"},{default:n(()=>[(a(!0),s(g,null,y(e.sheetsIconList,(l,_)=>(a(),s(p,{xs:12,sm:8,md:6,lg:4,xl:2,key:_},{default:n(()=>[o("div",$,[o("div",k,[o("div",j,[o("i",{class:[l,"fa"]},null,2)]),o("div",B,I(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});r.render=F,r.__scopeId="data-v-596b4114";export default r;
