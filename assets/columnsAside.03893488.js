var w=Object.defineProperty;var B=Object.prototype.hasOwnProperty;var v=Object.getOwnPropertySymbols,M=Object.prototype.propertyIsEnumerable;var S=(e,n,o)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,h=(e,n)=>{for(var o in n||(n={}))B.call(n,o)&&S(e,o,n[o]);if(v)for(var o of v(n))M.call(n,o)&&S(e,o,n[o]);return e};import{A as k,u as F,x as T,R as D,i as j,c as O,b5 as z,k as H,b6 as q,t as N,n as P,b as U,p as V,l as X,r as E,e as f,f as p,m as r,F as G,s as J,q as L,w as K}from"./index.b9e7554b.js";import"./vendor.7fd510b4.js";var b={name:"layoutColumnsAside",setup(){const e=k([]),n=k(),{proxy:o}=U(),d=F(),C=T(),y=D(),l=j({columnsAsideList:[],liIndex:0,difference:0,routeSplit:[]}),s=O(()=>d.state.themeConfig.themeConfig.columnsAsideStyle),c=t=>{l.liIndex=t,n.value.style.top=`${e.value[t].offsetTop+l.difference}px`},m=(t,i)=>{c(i);let{path:u,redirect:a}=t;a?y.push(a):y.push(u)},g=t=>{P(()=>{c(t)})},A=()=>{l.columnsAsideList=x(d.state.routesList.routesList);const t=_(C.path);g(t.item[0].k),o.mittBus.emit("setSendColumnsChildren",t)},_=t=>{const i=t.split("/");let u={};return l.columnsAsideList.map((a,I)=>{a.path===`/${i[1]}`&&(a.k=I,u.item=[h({},a)],u.children=[h({},a)],a.children&&(u.children=a.children))}),u},x=t=>t.filter(i=>!i.meta.isHide).map(i=>(i=Object.assign({},i),i.children&&(i.children=x(i.children)),i)),R=t=>{l.routeSplit=t.split("/"),l.routeSplit.shift();const i=`/${l.routeSplit[0]}`,u=l.columnsAsideList.find(a=>a.path===i);setTimeout(()=>{g(u.k)},0)};return z(d.state,t=>{if(t.themeConfig.themeConfig.columnsAsideStyle==="columnsRound"?l.difference=3:l.difference=0,t.routesList.routesList.length===l.columnsAsideList.length)return!1;A()}),H(()=>{A()}),q(t=>{R(t.path),o.mittBus.emit("setSendColumnsChildren",_(t.path))}),h({columnsAsideOffsetTopRefs:e,columnsAsideActiveRef:n,onColumnsAsideDown:g,setColumnsAsideStyle:s,onColumnsAsideMenuClick:m},N(l))}},le=`.layout-columns-aside[data-v-65e2fa8d] {
  width: 64px;
  height: 100%;
  background: var(--bg-columnsMenuBar);
}
.layout-columns-aside ul[data-v-65e2fa8d] {
  position: relative;
}
.layout-columns-aside ul li[data-v-65e2fa8d] {
  color: var(--bg-columnsMenuBarColor);
  width: 100%;
  height: 50px;
  text-align: center;
  display: flex;
  cursor: pointer;
  position: relative;
  z-index: 1;
}
.layout-columns-aside ul li .layout-columns-aside-li-box[data-v-65e2fa8d] {
  margin: auto;
}
.layout-columns-aside ul li .layout-columns-aside-li-box .layout-columns-aside-li-box-title[data-v-65e2fa8d] {
  padding-top: 1px;
}
.layout-columns-aside ul li a[data-v-65e2fa8d] {
  text-decoration: none;
  color: var(--bg-columnsMenuBarColor);
}
.layout-columns-aside ul .layout-columns-active[data-v-65e2fa8d] {
  color: #ffffff;
  transition: 0.3s ease-in-out;
}
.layout-columns-aside ul .columns-round[data-v-65e2fa8d], .layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  background: var(--color-primary);
  color: #ffffff;
  position: absolute;
  left: 50%;
  top: 2px;
  height: 44px;
  width: 58px;
  transform: translateX(-50%);
  z-index: 0;
  transition: 0.3s ease-in-out;
  border-radius: 5px;
}
.layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  top: 0;
  height: 50px;
  width: 100%;
  border-radius: 0;
}`;const $=K();V("data-v-65e2fa8d");const Q={class:"layout-columns-aside"},W={key:0,class:"layout-columns-aside-li-box"},Y={class:"layout-columns-aside-li-box-title font12"},Z={key:1,class:"layout-columns-aside-li-box"},ee={class:"layout-columns-aside-li-box-title font12"};X();const te=$((e,n,o,d,C,y)=>{const l=E("el-scrollbar");return f(),p("div",Q,[r(l,null,{default:$(()=>[r("ul",null,[(f(!0),p(G,null,J(e.columnsAsideList,(s,c)=>(f(),p("li",{key:c,onClick:m=>d.onColumnsAsideMenuClick(s,c),ref:m=>{m&&(d.columnsAsideOffsetTopRefs[c]=m)},class:{"layout-columns-active":e.liIndex===c},title:e.$t(s.meta.title)},[!s.meta.isLink||s.meta.isLink&&s.meta.isIframe?(f(),p("div",W,[r("i",{class:s.meta.icon},null,2),r("div",Y,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)])):(f(),p("div",Z,[r("a",{href:s.meta.isLink,target:"_blank"},[r("i",{class:s.meta.icon},null,2),r("div",ee,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)],8,["href"])]))],10,["onClick","title"]))),128)),r("div",{ref:"columnsAsideActiveRef",class:d.setColumnsAsideStyle},null,2)])]),_:1})])});b.render=te,b.__scopeId="data-v-65e2fa8d";export default b;
