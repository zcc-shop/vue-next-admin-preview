import{i as d,az as l}from"./index.7a634872.js";import{l as c,m,q as f,y as p,z as _,A as e,D as n,C as h,F as v}from"./vendor.6ad2dd02.js";var o={name:"401",setup(){const t=d();return{onSetAuth:()=>{l(),t.push("/login")}}}},D=`.error[data-v-d583d25c] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-d583d25c] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-d583d25c] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-d583d25c] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-d583d25c] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-d583d25c] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-d583d25c] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-d583d25c] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-d583d25c] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-d583d25c] {
  width: 100%;
  height: 100%;
}`;const r=v();c("data-v-d583d25c");const g={class:"error"},u={class:"error-flex"},x={class:"left"},y={class:"left-item"},b=e("div",{class:"left-item-animation left-item-num"},"401",-1),w={class:"left-item-animation left-item-title"},S={class:"left-item-animation left-item-msg"},A={class:"left-item-animation left-item-btn"},$=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/401.png"})],-1);m();const k=r((t,a,z,i,I,C)=>{const s=f("el-button");return p(),_("div",g,[e("div",u,[e("div",x,[e("div",y,[b,e("div",w,n(t.$t("message.noAccess.accessTitle")),1),e("div",S,n(t.$t("message.noAccess.accessMsg")),1),e("div",A,[e(s,{type:"primary",round:"",onClick:i.onSetAuth},{default:r(()=>[h(n(t.$t("message.noAccess.accessBtn")),1)]),_:1},8,["onClick"])])])]),$])])});o.render=k,o.__scopeId="data-v-d583d25c";export default o;
