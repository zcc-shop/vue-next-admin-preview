var d=Object.defineProperty;var l=Object.prototype.hasOwnProperty;var n=Object.getOwnPropertySymbols,c=Object.prototype.propertyIsEnumerable;var r=(e,t,o)=>t in e?d(e,t,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[t]=o,i=(e,t)=>{for(var o in t||(t={}))l.call(t,o)&&r(e,o,t[o]);if(n)for(var o of n(t))c.call(t,o)&&r(e,o,t[o]);return e};import{i as p,b6 as f,t as u,p as _,l as v,Q as y,U as m,e as h,f as x,w,m as a}from"./index.b9e7554b.js";import"./vendor.7fd510b4.js";var s={name:"layoutFooter",setup(){const e=p({isDelayFooter:!0});return f(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),i({},u(e))}},R=`.layout-footer[data-v-05adff24] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-05adff24] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const b=w();_("data-v-05adff24");const g={class:"layout-footer mt15"},D=a("div",{class:"layout-footer-warp"},[a("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F"),a("div",{class:"mt5"},"Copyright \u6DF1\u5733\u5E02 xxx \u8F6F\u4EF6\u79D1\u6280\u6709\u9650\u516C\u53F8")],-1);v();const F=b((e,t,o,I,S,$)=>y((h(),x("div",g,[D],512)),[[m,e.isDelayFooter]]));s.render=F,s.__scopeId="data-v-05adff24";export default s;
