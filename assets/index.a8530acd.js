import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as b}from"./index.e2750113.js";import h from"./index.5bfbf7dc.js";import w from"./tagsView.b0b92172.js";import"./vendor.5429c63c.js";import"./breadcrumb.776437ca.js";import"./user.6c85e5c2.js";import"./screenfull.6e1d84b2.js";import"./userNews.a421cb5c.js";import"./search.aa4a5bb7.js";import"./index.3d28e9ad.js";import"./horizontal.bdfd16c1.js";import"./subItem.65f53ed9.js";import"./contextmenu.ed8efb84.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:h,TagsView:w},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=b();_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=v((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
