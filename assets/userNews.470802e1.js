var x=Object.defineProperty;var y=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,_=Object.prototype.propertyIsEnumerable;var l=(n,e,t)=>e in n?x(n,e,{enumerable:!0,configurable:!0,writable:!0,value:t}):n[e]=t,b=(n,e)=>{for(var t in e||(e={}))y.call(e,t)&&l(n,t,e[t]);if(d)for(var t of d(e))_.call(e,t)&&l(n,t,e[t]);return n};import{i as w,t as F,p as h,l as f,r as C,e as a,f as s,m as r,h as p,F as g,s as k,q as i,w as E}from"./index.f043429b.js";import"./vendor.f1659326.js";var c={name:"layoutBreadcrumbUserNews",setup(){const n=w({newsList:[{label:"\u5173\u4E8E\u7248\u672C\u53D1\u5E03\u7684\u901A\u77E5",value:"vue-next-admin\uFF0C\u57FA\u4E8E vue3 + CompositionAPI + typescript + vite + element plus\uFF0C\u6B63\u5F0F\u53D1\u5E03\u65F6\u95F4\uFF1A2021\u5E7402\u670828\u65E5\uFF01",time:"2020-12-08"},{label:"\u5173\u4E8E\u5B66\u4E60\u4EA4\u6D41\u7684\u901A\u77E5",value:"QQ\u7FA4\u53F7\u7801 665452019\uFF0C\u6B22\u8FCE\u5C0F\u4F19\u4F34\u5165\u7FA4\u5B66\u4E60\u4EA4\u6D41\u63A2\u8BA8\uFF01",time:"2020-12-08"}]});return b({onAllReadClick:()=>{n.newsList=[]},onGoToGiteeClick:()=>{window.open("https://gitee.com/lyt-top/vue-next-admin")}},F(n))}},V=`.layout-navbars-breadcrumb-user-news .head-box[data-v-6269eeac] {
  display: flex;
  border-bottom: 1px solid #ebeef5;
  box-sizing: border-box;
  color: #333333;
  justify-content: space-between;
  height: 35px;
  align-items: center;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6269eeac] {
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6269eeac]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news .content-box[data-v-6269eeac] {
  font-size: 13px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6269eeac] {
  padding-top: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6269eeac]:last-of-type {
  padding-bottom: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-msg[data-v-6269eeac] {
  color: #999999;
  margin-top: 5px;
  margin-bottom: 5px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-time[data-v-6269eeac] {
  color: #999999;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6269eeac] {
  height: 35px;
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top: 1px solid #ebeef5;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6269eeac]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news[data-v-6269eeac] .el-empty__description p {
  font-size: 13px;
}`;const A=E("data-v-6269eeac");h("data-v-6269eeac");const B={class:"layout-navbars-breadcrumb-user-news"},G={class:"head-box"},L=r("div",{class:"head-box-title"},"\u901A\u77E5",-1),I={class:"content-box"},z={class:"content-box-msg"},D={class:"content-box-time"};f();const R=A((n,e,t,u,j,N)=>{const m=C("el-empty");return a(),s("div",B,[r("div",G,[L,n.newsList.length>0?(a(),s("div",{key:0,class:"head-box-btn",onClick:e[1]||(e[1]=(...o)=>u.onAllReadClick&&u.onAllReadClick(...o))},"\u5168\u90E8\u5DF2\u8BFB")):p("",!0)]),r("div",I,[n.newsList.length>0?(a(!0),s(g,{key:0},k(n.newsList,(o,v)=>(a(),s("div",{class:"content-box-item",key:v},[r("div",null,i(o.label),1),r("div",z,i(o.value),1),r("div",D,i(o.time),1)]))),128)):(a(),s(m,{key:1,description:"\u6682\u65E0\u901A\u77E5"}))]),n.newsList.length>0?(a(),s("div",{key:0,class:"foot-box",onClick:e[2]||(e[2]=(...o)=>u.onGoToGiteeClick&&u.onGoToGiteeClick(...o))},"\u524D\u5F80\u901A\u77E5\u4E2D\u5FC3")):p("",!0)])});c.render=R,c.__scopeId="data-v-6269eeac";export default c;
