import{Q as d,a$ as s,p as l,l as f,r as c,e as m,f as _,m as e,w as p,B as h}from"./index.9af39cde.js";import"./vendor.8f964570.js";var t={name:"401",setup(){const n=d();return{onSetAuth:()=>{s(),n.push("/login")}}}},j=`.error[data-v-2cf8dde6] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-2cf8dde6] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-2cf8dde6] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-2cf8dde6] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-2cf8dde6] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-2cf8dde6] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-2cf8dde6] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-2cf8dde6] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-2cf8dde6] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-2cf8dde6] {
  width: 100%;
  height: 100%;
}`;const o=p("data-v-2cf8dde6");l("data-v-2cf8dde6");const v={class:"error"},x={class:"error-flex"},u={class:"left"},g={class:"left-item"},y=e("div",{class:"left-item-animation left-item-num"},"401",-1),w=e("div",{class:"left-item-animation left-item-title"},"\u60A8\u672A\u88AB\u6388\u6743\uFF0C\u6CA1\u6709\u64CD\u4F5C\u6743\u9650~",-1),b=e("div",{class:"left-item-animation left-item-msg"},"\u8054\u7CFB\u65B9\u5F0F\uFF1A\u52A0QQ\u7FA4\u63A2\u8BA8 665452019",-1),S={class:"left-item-animation left-item-btn"},k=h("\u91CD\u65B0\u6388\u6743"),I=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/401.png"})],-1);f();const $=o((n,r,z,a,A,B)=>{const i=c("el-button");return m(),_("div",v,[e("div",x,[e("div",u,[e("div",g,[y,w,b,e("div",S,[e(i,{type:"primary",round:"",onClick:a.onSetAuth},{default:o(()=>[k]),_:1},8,["onClick"])])])]),I])])});t.render=$,t.__scopeId="data-v-2cf8dde6";export default t;
