import{u as m,c as p,p as _,l as f,r as a,e as r,f as n,m as u,h as l,w as h}from"./index.a4f13c03.js";import w from"./index.9663feee.js";import v from"./tagsView.d5374fa9.js";import"./vendor.9092e083.js";import"./breadcrumb.889b2204.js";import"./user.ddab554d.js";import"./screenfull.31c31425.js";import"./userNews.ccdf665e.js";import"./search.cb6fd856.js";import"./index.94a3a4f4.js";import"./horizontal.48f65ff4.js";import"./subItem.6ed4f97a.js";import"./contextmenu.758d4121.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h();_("data-v-fde5c75e");const x={class:"layout-navbars-container"};f();const g=j((t,c,o,e,b,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[u(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
