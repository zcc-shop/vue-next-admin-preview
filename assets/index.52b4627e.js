import{u as h}from"./index.3bc43889.js";import{z as g,g as d,p as c,d as p,f as r,h as m,i as t,l as u,q as C}from"./vendor.9b51066d.js";var a={name:"layoutLogo",setup(){const{proxy:i}=d(),o=h(),s=g(()=>o.state.themeConfig.themeConfig);return{setShowLogo:g(()=>{let{isCollapse:l,layout:n}=o.state.themeConfig.themeConfig;return!l||n==="classic"||document.body.clientWidth<1e3}),getThemeConfig:s,onThemeConfigChange:()=>{if(o.state.themeConfig.themeConfig.layout==="transverse")return!1;i.mittBus.emit("onMenuClick"),o.state.themeConfig.themeConfig.isCollapse=!o.state.themeConfig.themeConfig.isCollapse}}}},T=`.layout-logo[data-v-36bff5a0] {
  width: 220px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: rgba(0, 21, 41, 0.02) 0px 1px 4px;
  color: var(--color-primary);
  font-size: 16px;
  cursor: pointer;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-logo:hover span[data-v-36bff5a0] {
  color: var(--color-primary-light-2);
}
.layout-logo-medium-img[data-v-36bff5a0] {
  width: 20px;
  margin-right: 5px;
}
.layout-logo-size[data-v-36bff5a0] {
  width: 100%;
  height: 50px;
  display: flex;
  cursor: pointer;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-logo-size-img[data-v-36bff5a0] {
  width: 20px;
  margin: auto;
}
.layout-logo-size:hover img[data-v-36bff5a0] {
  animation: logoAnimation 0.3s ease-in-out;
}`;const y=C();c("data-v-36bff5a0");const v=t("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/logo/logo-mini.svg",class:"layout-logo-medium-img"},null,-1),x=t("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/logo/logo-mini.svg",class:"layout-logo-size-img"},null,-1);p();const _=y((i,o,s,e,f,l)=>e.setShowLogo?(r(),m("div",{key:0,class:"layout-logo",onClick:o[1]||(o[1]=(...n)=>e.onThemeConfigChange&&e.onThemeConfigChange(...n))},[v,t("span",null,u(e.getThemeConfig.globalTitle),1)])):(r(),m("div",{key:1,class:"layout-logo-size",onClick:o[2]||(o[2]=(...n)=>e.onThemeConfigChange&&e.onThemeConfigChange(...n))},[x])));a.render=_,a.__scopeId="data-v-36bff5a0";export default a;
