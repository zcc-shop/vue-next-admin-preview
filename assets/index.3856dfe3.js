import{u as m}from"./index.79428e55.js";import p from"./index.0fa04113.js";import _ from"./tagsView.00e08934.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as b,M as h,F as w}from"./vendor.ad170e2b.js";import"./breadcrumb.5fb0897c.js";import"./user.a4968801.js";import"./screenfull.d9bbb71f.js";import"./userNews.c4edda8c.js";import"./search.e9704ea6.js";import"./index.675a9b47.js";import"./horizontal.9e1b986b.js";import"./subItem.bb98103e.js";import"./contextmenu.e96fb587.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,y)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[b(i),e.setShowTagsView?(r(),n(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
