import{u as m,c as p,p as _,l as f,r as a,e as r,f as n,m as u,h as l,w as h}from"./index.d334a43e.js";import w from"./index.152ee7cb.js";import b from"./tagsView.b88f59c7.js";import"./vendor.8f964570.js";import"./breadcrumb.fabe5ec7.js";import"./user.f074bb88.js";import"./screenfull.77def122.js";import"./userNews.5c9ffe51.js";import"./search.be172493.js";import"./index.69004541.js";import"./horizontal.73ed8a86.js";import"./subItem.2f590c23.js";import"./contextmenu.6f193db6.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:b},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=h("data-v-fde5c75e");_("data-v-fde5c75e");const j={class:"layout-navbars-container"};f();const x=v((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[u(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
