import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as h}from"./index.ef4a75e8.js";import w from"./index.79301b41.js";import v from"./tagsView.311fe0d1.js";import"./vendor.5429c63c.js";import"./breadcrumb.ab88b5ad.js";import"./user.17551714.js";import"./screenfull.6e1d84b2.js";import"./userNews.51df2254.js";import"./search.ff4a2b0a.js";import"./index.f217d049.js";import"./horizontal.d2e9df33.js";import"./subItem.6f87e6d0.js";import"./contextmenu.644924fa.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=h();_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=b((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
