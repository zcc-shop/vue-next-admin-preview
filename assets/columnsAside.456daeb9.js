var B=Object.defineProperty;var w=Object.prototype.hasOwnProperty;var v=Object.getOwnPropertySymbols,M=Object.prototype.propertyIsEnumerable;var S=(e,n,o)=>n in e?B(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,h=(e,n)=>{for(var o in n||(n={}))w.call(n,o)&&S(e,o,n[o]);if(v)for(var o of v(n))M.call(n,o)&&S(e,o,n[o]);return e};import{r as k,D,U as F,a as T,z as j,B as z,o as O,X as H,t as U,n as X,g as q,p as N,d as P,e as V,f,h as p,i as r,F as Y,Y as E,l as L,q as G}from"./vendor.9b51066d.js";import{u as J}from"./index.1414326c.js";var C={name:"layoutColumnsAside",setup(){const e=k([]),n=k(),{proxy:o}=q(),d=J(),b=D(),y=F(),l=T({columnsAsideList:[],liIndex:0,difference:0,routeSplit:[]}),s=j(()=>d.state.themeConfig.themeConfig.columnsAsideStyle),c=t=>{l.liIndex=t,n.value.style.top=`${e.value[t].offsetTop+l.difference}px`},m=(t,a)=>{c(a);let{path:u,redirect:i}=t;i?y.push(i):y.push(u)},g=t=>{X(()=>{c(t)})},A=()=>{l.columnsAsideList=x(d.state.routesList.routesList);const t=_(b.path);g(t.item[0].k),o.mittBus.emit("setSendColumnsChildren",t)},_=t=>{const a=t.split("/");let u={};return l.columnsAsideList.map((i,I)=>{i.path===`/${a[1]}`&&(i.k=I,u.item=[h({},i)],u.children=[h({},i)],i.children&&(u.children=i.children))}),u},x=t=>t.filter(a=>!a.meta.isHide).map(a=>(a=Object.assign({},a),a.children&&(a.children=x(a.children)),a)),R=t=>{l.routeSplit=t.split("/"),l.routeSplit.shift();const a=`/${l.routeSplit[0]}`,u=l.columnsAsideList.find(i=>i.path===a);setTimeout(()=>{g(u.k)},0)};return z(d.state,t=>{if(t.themeConfig.themeConfig.columnsAsideStyle==="columnsRound"?l.difference=3:l.difference=0,t.routesList.routesList.length===l.columnsAsideList.length)return!1;A()}),O(()=>{A()}),H(t=>{R(t.path),o.mittBus.emit("setSendColumnsChildren",_(t.path))}),h({columnsAsideOffsetTopRefs:e,columnsAsideActiveRef:n,onColumnsAsideDown:g,setColumnsAsideStyle:s,onColumnsAsideMenuClick:m},U(l))}},le=`.layout-columns-aside[data-v-65e2fa8d] {
  width: 64px;
  height: 100%;
  background: var(--bg-columnsMenuBar);
}
.layout-columns-aside ul[data-v-65e2fa8d] {
  position: relative;
}
.layout-columns-aside ul li[data-v-65e2fa8d] {
  color: var(--bg-columnsMenuBarColor);
  width: 100%;
  height: 50px;
  text-align: center;
  display: flex;
  cursor: pointer;
  position: relative;
  z-index: 1;
}
.layout-columns-aside ul li .layout-columns-aside-li-box[data-v-65e2fa8d] {
  margin: auto;
}
.layout-columns-aside ul li .layout-columns-aside-li-box .layout-columns-aside-li-box-title[data-v-65e2fa8d] {
  padding-top: 1px;
}
.layout-columns-aside ul li a[data-v-65e2fa8d] {
  text-decoration: none;
  color: var(--bg-columnsMenuBarColor);
}
.layout-columns-aside ul .layout-columns-active[data-v-65e2fa8d] {
  color: #ffffff;
  transition: 0.3s ease-in-out;
}
.layout-columns-aside ul .columns-round[data-v-65e2fa8d], .layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  background: var(--color-primary);
  color: #ffffff;
  position: absolute;
  left: 50%;
  top: 2px;
  height: 44px;
  width: 58px;
  transform: translateX(-50%);
  z-index: 0;
  transition: 0.3s ease-in-out;
  border-radius: 5px;
}
.layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  top: 0;
  height: 50px;
  width: 100%;
  border-radius: 0;
}`;const $=G();N("data-v-65e2fa8d");const K={class:"layout-columns-aside"},Q={key:0,class:"layout-columns-aside-li-box"},W={class:"layout-columns-aside-li-box-title font12"},Z={key:1,class:"layout-columns-aside-li-box"},ee={class:"layout-columns-aside-li-box-title font12"};P();const te=$((e,n,o,d,b,y)=>{const l=V("el-scrollbar");return f(),p("div",K,[r(l,null,{default:$(()=>[r("ul",null,[(f(!0),p(Y,null,E(e.columnsAsideList,(s,c)=>(f(),p("li",{key:c,onClick:m=>d.onColumnsAsideMenuClick(s,c),ref:m=>{m&&(d.columnsAsideOffsetTopRefs[c]=m)},class:{"layout-columns-active":e.liIndex===c},title:e.$t(s.meta.title)},[!s.meta.isLink||s.meta.isLink&&s.meta.isIframe?(f(),p("div",Q,[r("i",{class:s.meta.icon},null,2),r("div",W,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)])):(f(),p("div",Z,[r("a",{href:s.meta.isLink,target:"_blank"},[r("i",{class:s.meta.icon},null,2),r("div",ee,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)],8,["href"])]))],10,["onClick","title"]))),128)),r("div",{ref:"columnsAsideActiveRef",class:d.setColumnsAsideStyle},null,2)])]),_:1})])});C.render=te,C.__scopeId="data-v-65e2fa8d";export default C;
