var z=Object.defineProperty;var j=Object.prototype.hasOwnProperty;var U=Object.getOwnPropertySymbols,x=Object.prototype.propertyIsEnumerable;var N=(e,n,t)=>n in e?z(e,n,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[n]=t,B=(e,n)=>{for(var t in n||(n={}))j.call(n,t)&&N(e,t,n[t]);if(U)for(var t of U(n))x.call(n,t)&&N(e,t,n[t]);return e};import{D as E,R as P,u as $,A as D,i as R,c as y,k as F,g as T,t as M,O,B as q,b3 as A,E as H,bc as V,bd as Q,b as G,p as J,l as K,r as m,e as W,f as X,m as s,T as Y,Q as Z,U as ee,C as i,q as g,w as se}from"./index.e2750113.js";import{s as L}from"./screenfull.6e1d84b2.js";import ne from"./userNews.a421cb5c.js";import oe from"./search.aa4a5bb7.js";import"./vendor.5429c63c.js";var S={name:"layoutBreadcrumbUser",components:{UserNews:ne,Search:oe},setup(){const{t:e}=E(),{proxy:n}=G(),t=P(),a=$(),f=D(),c=R({isScreenfull:!1,isShowUserNewsPopover:!1,disabledI18n:!1}),l=y(()=>a.state.userInfos.userInfos),b=y(()=>a.state.themeConfig.themeConfig),p=y(()=>{let{layout:u,isClassicSplitMenu:C}=b.value,d="";return u==="defaults"||u==="classic"&&!C?d=1:d=null,d}),h=()=>{if(!L.isEnabled)return O.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;L.toggle(),c.isScreenfull=!c.isScreenfull},v=()=>{n.mittBus.emit("openSetingsDrawer")},w=u=>{u==="logOut"?q({closeOnClickModal:!1,closeOnPressEscape:!1,title:e("message.user.logOutTitle"),message:e("message.user.logOutMessage"),showCancelButton:!0,confirmButtonText:e("message.user.logOutConfirm"),cancelButtonText:e("message.user.logOutCancel"),beforeClose:(C,d,I)=>{C==="confirm"?(d.confirmButtonLoading=!0,d.confirmButtonText=e("message.user.logOutExit"),setTimeout(()=>{I(),setTimeout(()=>{d.confirmButtonLoading=!1},300)},700)):I()}}).then(()=>{A(),H(),t.push("/login"),setTimeout(()=>{O.success(e("message.user.logOutSuccess"))},300)}).catch(()=>{}):t.push(u)},_=()=>{f.value.openSearch()},r=u=>{V("themeConfig"),b.value.globalI18n=u,Q("themeConfig",b.value),n.$i18n.locale=u,k()},k=()=>{switch(T("themeConfig").globalI18n){case"zh-cn":c.disabledI18n="zh-cn";break;case"en":c.disabledI18n="en";break;case"zh-tw":c.disabledI18n="zh-tw";break}};return F(()=>{T("themeConfig")&&k()}),B({getUserInfos:l,onLayoutSetingClick:v,onHandleCommandClick:w,onScreenfullClick:h,onSearchClick:_,onLanguageChange:r,searchRef:f,layoutUserFlexNum:p},M(c))}},ve=`.layout-navbars-breadcrumb-user[data-v-007134ec] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.layout-navbars-breadcrumb-user-link[data-v-007134ec] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-007134ec] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-007134ec] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-007134ec]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-007134ec] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-007134ec] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-007134ec] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-007134ec] .el-badge__content.is-fixed {
  top: 12px;
}`;const o=se();J("data-v-007134ec");const ae={class:"layout-navbars-breadcrumb-user-icon"},te=i("\u7B80\u4F53\u4E2D\u6587"),le=i("English"),re=i("\u7E41\u9AD4\u4E2D\u6587"),ie={class:"layout-navbars-breadcrumb-user-icon"},ue={class:"layout-navbars-breadcrumb-user-link"},ce=s("i",{class:"el-icon-arrow-down el-icon--right"},null,-1);K();const de=o((e,n,t,a,f,c)=>{const l=m("el-dropdown-item"),b=m("el-dropdown-menu"),p=m("el-dropdown"),h=m("el-badge"),v=m("UserNews"),w=m("el-popover"),_=m("Search");return W(),X("div",{class:"layout-navbars-breadcrumb-user",style:{flex:a.layoutUserFlexNum}},[s(p,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:a.onLanguageChange},{dropdown:o(()=>[s(b,null,{default:o(()=>[s(l,{command:"zh-cn",disabled:e.disabledI18n==="zh-cn"},{default:o(()=>[te]),_:1},8,["disabled"]),s(l,{command:"en",disabled:e.disabledI18n==="en"},{default:o(()=>[le]),_:1},8,["disabled"]),s(l,{command:"zh-tw",disabled:e.disabledI18n==="zh-tw"},{default:o(()=>[re]),_:1},8,["disabled"])]),_:1})]),default:o(()=>[s("div",ae,[s("i",{class:"iconfont icon-huanjingxingqiu",title:e.$t("message.user.title1")},null,8,["title"])])]),_:1},8,["onCommand"]),s("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:n[1]||(n[1]=(...r)=>a.onSearchClick&&a.onSearchClick(...r))},[s("i",{class:"el-icon-search",title:e.$t("message.user.title2")},null,8,["title"])]),s("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:n[2]||(n[2]=(...r)=>a.onLayoutSetingClick&&a.onLayoutSetingClick(...r))},[s("i",{class:"icon-skin iconfont",title:e.$t("message.user.title3")},null,8,["title"])]),s("div",ie,[s(w,{placement:"bottom",trigger:"click",visible:e.isShowUserNewsPopover,"onUpdate:visible":n[4]||(n[4]=r=>e.isShowUserNewsPopover=r),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:o(()=>[s(h,{"is-dot":!0,onClick:n[3]||(n[3]=r=>e.isShowUserNewsPopover=!e.isShowUserNewsPopover)},{default:o(()=>[s("i",{class:"el-icon-bell",title:e.$t("message.user.title4")},null,8,["title"])]),_:1})]),default:o(()=>[s(Y,{name:"el-zoom-in-top"},{default:o(()=>[Z(s(v,null,null,512),[[ee,e.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),s("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:n[5]||(n[5]=(...r)=>a.onScreenfullClick&&a.onScreenfullClick(...r))},[s("i",{class:["iconfont",e.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:e.isScreenfull?e.$t("message.user.title5"):e.$t("message.user.title6")},null,10,["title"])]),s(p,{"show-timeout":70,"hide-timeout":50,onCommand:a.onHandleCommandClick},{dropdown:o(()=>[s(b,null,{default:o(()=>[s(l,{command:"/home"},{default:o(()=>[i(g(e.$t("message.user.dropdown1")),1)]),_:1}),s(l,{command:"/personal"},{default:o(()=>[i(g(e.$t("message.user.dropdown2")),1)]),_:1}),s(l,{command:"/404"},{default:o(()=>[i(g(e.$t("message.user.dropdown3")),1)]),_:1}),s(l,{command:"/401"},{default:o(()=>[i(g(e.$t("message.user.dropdown4")),1)]),_:1}),s(l,{divided:"",command:"logOut"},{default:o(()=>[i(g(e.$t("message.user.dropdown5")),1)]),_:1})]),_:1})]),default:o(()=>[s("span",ue,[s("img",{src:a.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),i(" "+g(a.getUserInfos.userName===""?"test":a.getUserInfos.userName)+" ",1),ce])]),_:1},8,["onCommand"]),s(_,{ref:"searchRef"},null,512)],4)});S.render=de,S.__scopeId="data-v-007134ec";export default S;
