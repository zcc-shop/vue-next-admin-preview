import{Q as s,p as f,l,r as c,e as d,f as m,m as e,w as _,B as p}from"./index.9af39cde.js";import"./vendor.8f964570.js";var t={name:"404",setup(){const n=s();return{onGoHome:()=>{n.push("/")}}}},$=`.error[data-v-f8fc4360] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-f8fc4360] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-f8fc4360] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-f8fc4360] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-f8fc4360] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-f8fc4360] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-f8fc4360] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-f8fc4360] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-f8fc4360] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-f8fc4360] {
  width: 100%;
  height: 100%;
}`;const o=_("data-v-f8fc4360");f("data-v-f8fc4360");const h={class:"error"},v={class:"error-flex"},x={class:"left"},u={class:"left-item"},g=e("div",{class:"left-item-animation left-item-num"},"404",-1),y=e("div",{class:"left-item-animation left-item-title"},"\u5730\u5740\u8F93\u5165\u9519\u8BEF\uFF0C\u8BF7\u91CD\u65B0\u8F93\u5165\u5730\u5740~",-1),w=e("div",{class:"left-item-animation left-item-msg"},"\u60A8\u53EF\u4EE5\u5148\u68C0\u67E5\u7F51\u5740\uFF0C\u7136\u540E\u91CD\u65B0\u8F93\u5165\u6216\u7ED9\u6211\u4EEC\u53CD\u9988\u95EE\u9898\u3002",-1),b={class:"left-item-animation left-item-btn"},k=p("\u8FD4\u56DE\u9996\u9875"),I=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/404.png"})],-1);l();const z=o((n,r,B,i,C,G)=>{const a=c("el-button");return d(),m("div",h,[e("div",v,[e("div",x,[e("div",u,[g,y,w,e("div",b,[e(a,{type:"primary",round:"",onClick:i.onGoHome},{default:o(()=>[k]),_:1},8,["onClick"])])])]),I])])});t.render=z,t.__scopeId="data-v-f8fc4360";export default t;
