import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as h}from"./index.7bf2b40d.js";import w from"./index.d9385a78.js";import b from"./tagsView.8aecc4dd.js";import"./vendor.5f5309b8.js";import"./breadcrumb.6ad1a571.js";import"./user.9bd15181.js";import"./screenfull.5abc3486.js";import"./userNews.95715758.js";import"./search.ce900898.js";import"./index.133a056c.js";import"./horizontal.b97473de.js";import"./subItem.e2f40d75.js";import"./contextmenu.21fb5fad.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:b},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=h();_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=v((t,c,o,e,g,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",j,[f(d),e.setShowTagsView?(r(),n(i,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
