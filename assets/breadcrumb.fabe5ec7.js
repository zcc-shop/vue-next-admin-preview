var C=Object.assign;import{u as B,x as k,Q as I,i as L,c as T,k as w,b1 as F,t as R,b as M,p as N,l as V,r as v,P as j,S as z,e as s,f as i,m,b7 as D,F as q,s as G,h as _,B as y,q as S,b6 as P,w as Q}from"./index.d334a43e.js";import"./vendor.8f964570.js";var p={name:"layoutBreadcrumb",setup(){const{proxy:u}=M(),r=B(),f=k(),a=I(),e=L({breadcrumbList:[],routeSplit:[],routeSplitFirst:"",routeSplitIndex:1}),h=T(()=>r.state.themeConfig.themeConfig),d=o=>{const{redirect:n,path:g}=o;n?a.push(n):a.push(g)},b=()=>{u.mittBus.emit("onMenuClick"),r.state.themeConfig.themeConfig.isCollapse=!r.state.themeConfig.themeConfig.isCollapse},t=o=>{o.map(n=>{e.routeSplit.map((g,H,x)=>{e.routeSplitFirst===n.path&&(e.routeSplitFirst+=`/${x[e.routeSplitIndex]}`,e.breadcrumbList.push(n),e.routeSplitIndex++,n.children&&t(n.children))})})},c=o=>{if(!r.state.themeConfig.themeConfig.isBreadcrumb)return!1;e.breadcrumbList=[r.state.routesList.routesList[0]],e.routeSplit=o.split("/"),e.routeSplit.shift(),e.routeSplitFirst=`/${e.routeSplit[0]}`,e.routeSplitIndex=1,t(r.state.routesList.routesList)};return w(()=>{c(f.path)}),F(o=>{c(o.path)}),C({onThemeConfigChange:b,getThemeConfig:h,onBreadcrumbClick:d},R(e))}},W=`.layout-navbars-breadcrumb[data-v-da77af00] {
  flex: 1;
  height: inherit;
  display: flex;
  align-items: center;
  padding-left: 15px;
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-icon[data-v-da77af00] {
  cursor: pointer;
  font-size: 18px;
  margin-right: 15px;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-span[data-v-da77af00] {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-iconfont[data-v-da77af00] {
  font-size: 14px;
  margin-right: 5px;
}
.layout-navbars-breadcrumb[data-v-da77af00] .el-breadcrumb__separator {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}`;const l=Q("data-v-da77af00");N("data-v-da77af00");const U={class:"layout-navbars-breadcrumb"},A={key:0,class:"layout-navbars-breadcrumb-span"};V();const E=l((u,r,f,a,e,h)=>{const d=v("el-breadcrumb-item"),b=v("el-breadcrumb");return j((s(),i("div",U,[m("i",{class:["layout-navbars-breadcrumb-icon",a.getThemeConfig.isCollapse?"el-icon-s-unfold":"el-icon-s-fold"],onClick:r[1]||(r[1]=(...t)=>a.onThemeConfigChange&&a.onThemeConfigChange(...t))},null,2),m(b,{class:"layout-navbars-breadcrumb-hide"},{default:l(()=>[m(D,{name:"breadcrumb",mode:"out-in"},{default:l(()=>[(s(!0),i(q,null,G(u.breadcrumbList,(t,c)=>(s(),i(d,{key:t.meta.title},{default:l(()=>[c===u.breadcrumbList.length-1?(s(),i("span",A,[a.getThemeConfig.isBreadcrumbIcon?(s(),i("i",{key:0,class:[t.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):_("",!0),y(S(t.meta.title),1)])):(s(),i("a",{key:1,onClick:P(o=>a.onBreadcrumbClick(t),["prevent"])},[a.getThemeConfig.isBreadcrumbIcon?(s(),i("i",{key:0,class:[t.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):_("",!0),y(S(t.meta.title),1)],8,["onClick"]))]),_:2},1024))),128))]),_:1})]),_:1})],512)),[[z,a.getThemeConfig.isBreadcrumb]])});p.render=E,p.__scopeId="data-v-da77af00";export default p;
