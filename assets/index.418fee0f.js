var i=Object.defineProperty;var l=Object.prototype.hasOwnProperty;var r=Object.getOwnPropertySymbols,c=Object.prototype.propertyIsEnumerable;var n=(e,t,a)=>t in e?i(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,d=(e,t)=>{for(var a in t||(t={}))l.call(t,a)&&n(e,a,t[a]);if(r)for(var a of r(t))c.call(t,a)&&n(e,a,t[a]);return e};import{aA as p}from"./index.85d2a956.js";import{r as u,t as f,l as _,m as v,v as y,x as m,y as h,z as x,F as w,A as o}from"./vendor.69bd2edd.js";var s={name:"layoutFooter",setup(){const e=u({isDelayFooter:!0});return p(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),d({},f(e))}},k=`.layout-footer[data-v-05adff24] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-05adff24] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const F=w();_("data-v-05adff24");const g={class:"layout-footer mt15"},D=o("div",{class:"layout-footer-warp"},[o("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F"),o("div",{class:"mt5"},"Copyright \u6DF1\u5733\u5E02 xxx \u8F6F\u4EF6\u79D1\u6280\u6709\u9650\u516C\u53F8")],-1);v();const I=F((e,t,a,S,$,A)=>y((h(),x("div",g,[D],512)),[[m,e.isDelayFooter]]));s.render=I,s.__scopeId="data-v-05adff24";export default s;
