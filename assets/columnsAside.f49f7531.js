var f=Object.assign;import{A as v,u as I,x as w,Q as B,i as M,c as F,b0 as T,k as $,b1 as D,t as j,n as O,b as z,p as H,l as q,r as N,e as d,f as m,m as i,F as P,s as Q,q as S,w as U}from"./index.9af39cde.js";import"./vendor.8f964570.js";var x={name:"layoutColumnsAside",setup(){const c=v([]),p=v(),{proxy:h}=z(),a=I(),g=w(),y=B(),s=M({columnsAsideList:[],liIndex:0,difference:0,routeSplit:[]}),t=F(()=>a.state.themeConfig.themeConfig.columnsAsideStyle),u=e=>{s.liIndex=e,p.value.style.top=`${c.value[e].offsetTop+s.difference}px`},r=(e,n)=>{u(n);let{path:l,redirect:o}=e;o?y.push(o):y.push(l)},_=e=>{O(()=>{u(e)})},C=()=>{s.columnsAsideList=A(a.state.routesList.routesList);const e=b(g.path);_(e.item[0].k),h.mittBus.emit("setSendColumnsChildren",e)},b=e=>{const n=e.split("/");let l={};return s.columnsAsideList.map((o,R)=>{o.path===`/${n[1]}`&&(o.k=R,l.item=[f({},o)],l.children=[f({},o)],o.children&&(l.children=o.children))}),l},A=e=>e.filter(n=>!n.meta.isHide).map(n=>(n=Object.assign({},n),n.children&&(n.children=A(n.children)),n)),L=e=>{s.routeSplit=e.split("/"),s.routeSplit.shift();const n=`/${s.routeSplit[0]}`,l=s.columnsAsideList.find(o=>o.path===n);setTimeout(()=>{_(l.k)},0)};return T(a.state,e=>{if(e.themeConfig.themeConfig.columnsAsideStyle==="columnsRound"?s.difference=3:s.difference=0,e.routesList.routesList.length===s.columnsAsideList.length)return!1;C()}),$(()=>{C()}),D(e=>{L(e.path),h.mittBus.emit("setSendColumnsChildren",b(e.path))}),f({columnsAsideOffsetTopRefs:c,columnsAsideActiveRef:p,onColumnsAsideDown:_,setColumnsAsideStyle:t,onColumnsAsideMenuClick:r},j(s))}},ee=`.layout-columns-aside[data-v-138ec090] {
  width: 64px;
  height: 100%;
  background: var(--bg-columnsMenuBar);
}
.layout-columns-aside ul[data-v-138ec090] {
  position: relative;
}
.layout-columns-aside ul li[data-v-138ec090] {
  color: var(--bg-columnsMenuBarColor);
  width: 100%;
  height: 50px;
  text-align: center;
  display: flex;
  cursor: pointer;
  position: relative;
  z-index: 1;
}
.layout-columns-aside ul li .layout-columns-aside-li-box[data-v-138ec090] {
  margin: auto;
}
.layout-columns-aside ul li .layout-columns-aside-li-box .layout-columns-aside-li-box-title[data-v-138ec090] {
  padding-top: 1px;
}
.layout-columns-aside ul li a[data-v-138ec090] {
  text-decoration: none;
  color: var(--bg-columnsMenuBarColor);
}
.layout-columns-aside ul .layout-columns-active[data-v-138ec090] {
  color: #ffffff;
  transition: 0.3s ease-in-out;
}
.layout-columns-aside ul .columns-round[data-v-138ec090], .layout-columns-aside ul .columns-card[data-v-138ec090] {
  background: var(--color-primary);
  color: #ffffff;
  position: absolute;
  left: 50%;
  top: 2px;
  height: 44px;
  width: 58px;
  transform: translateX(-50%);
  z-index: 0;
  transition: 0.3s ease-in-out;
  border-radius: 5px;
}
.layout-columns-aside ul .columns-card[data-v-138ec090] {
  top: 0;
  height: 50px;
  width: 100%;
  border-radius: 0;
}`;const k=U("data-v-138ec090");H("data-v-138ec090");const V={class:"layout-columns-aside"},X={key:0,class:"layout-columns-aside-li-box"},E={class:"layout-columns-aside-li-box-title font12"},G={key:1,class:"layout-columns-aside-li-box"},J={class:"layout-columns-aside-li-box-title font12"};q();const K=k((c,p,h,a,g,y)=>{const s=N("el-scrollbar");return d(),m("div",V,[i(s,null,{default:k(()=>[i("ul",null,[(d(!0),m(P,null,Q(c.columnsAsideList,(t,u)=>(d(),m("li",{key:u,onClick:r=>a.onColumnsAsideMenuClick(t,u),ref:r=>{r&&(a.columnsAsideOffsetTopRefs[u]=r)},class:{"layout-columns-active":c.liIndex===u},title:t.meta.title},[!t.meta.isLink||t.meta.isLink&&t.meta.isIframe?(d(),m("div",X,[i("i",{class:t.meta.icon},null,2),i("div",E,S(t.meta.title&&t.meta.title.length>=4?t.meta.title.substr(0,4):t.meta.title),1)])):(d(),m("div",G,[i("a",{href:t.meta.isLink,target:"_blank"},[i("i",{class:t.meta.icon},null,2),i("div",J,S(t.meta.title&&t.meta.title.length>=4?t.meta.title.substr(0,4):t.meta.title),1)],8,["href"])]))],10,["onClick","title"]))),128)),i("div",{ref:"columnsAsideActiveRef",class:a.setColumnsAsideStyle},null,2)])]),_:1})])});x.render=K,x.__scopeId="data-v-138ec090";export default x;
