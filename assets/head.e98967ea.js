var p=Object.defineProperty;var l=Object.prototype.hasOwnProperty;var o=Object.getOwnPropertySymbols,c=Object.prototype.propertyIsEnumerable;var r=(t,e,n)=>e in t?p(t,e,{enumerable:!0,configurable:!0,writable:!0,value:n}):t[e]=n,s=(t,e)=>{for(var n in e||(e={}))l.call(e,n)&&r(t,n,e[n]);if(o)for(var n of o(e))c.call(e,n)&&r(t,n,e[n]);return t};import{i as m,o as u,a as v,t as f,aH as d,p as b,l as g,e as _,f as x,m as a,q as h,w}from"./index.a4f13c03.js";import"./vendor.9092e083.js";var i={name:"chartHead",setup(){const t=m({time:{txt:"",fun:0}}),e=()=>{t.time.txt=d(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ"),t.time.fun=window.setInterval(()=>{t.time.txt=d(new Date,"YYYY-mm-dd HH:MM:SS WWW QQQQ")},1e3)};return u(()=>{e()}),v(()=>{window.clearInterval(t.time.fun)}),s({},f(t))}},B=`.big-data-up[data-v-692a7ee7] {
  height: 55px;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 0 15px;
  color: var(--color-primary);
  overflow: hidden;
  position: relative;
}
.big-data-up .up-left[data-v-692a7ee7] {
  position: absolute;
}
.big-data-up .up-center[data-v-692a7ee7] {
  width: 100%;
  display: flex;
  justify-content: center;
  font-size: 18px;
  letter-spacing: 5px;
  background-image: -webkit-linear-gradient(left, var(--color-primary), var(--color-primary-light-1) 25%, var(--color-primary) 50%, var(--color-primary-light-1) 75%, var(--color-primary));
  -webkit-text-fill-color: transparent;
  -webkit-background-clip: text;
  background-clip: text;
  background-size: 200% 100%;
  -webkit-animation: masked-animation-data-v-b02d8052 4s linear infinite;
  animation: masked-animation-data-v-b02d8052 4s linear infinite;
  -webkit-box-reflect: below -2px -webkit-gradient(linear, left top, left bottom, from(transparent), to(rgba(255, 255, 255, 0.1)));
  position: relative;
  position: relative;
}
@keyframes masked-animation-692a7ee7 {
0% {
    background-position: 0 0;
}
100% {
    background-position: -100% 0;
}
}
.big-data-up .up-center[data-v-692a7ee7]::after {
  content: "";
  width: 250px;
  position: absolute;
  bottom: -15px;
  left: 50%;
  transform: translateX(-50%);
  border: 1px transparent solid;
  border-image: linear-gradient(to right, var(--color-primary-light-9), var(--color-primary)) 1 10;
}
.big-data-up .up-center span[data-v-692a7ee7] {
  cursor: pointer;
}`;const k=w();b("data-v-692a7ee7");const y={class:"big-data-up mb15"},Q={class:"up-left"},S=a("i",{class:"el-icon-time mr5"},null,-1),Y=a("div",{class:"up-center"},[a("span",null,"\u667A\u6167\u519C\u4E1A\u7CFB\u7EDF\u5E73\u53F0")],-1);g();const I=k((t,e,n,H,W,M)=>(_(),x("div",y,[a("div",Q,[S,a("span",null,h(t.time.txt),1)]),Y])));i.render=I,i.__scopeId="data-v-692a7ee7";export default i;
