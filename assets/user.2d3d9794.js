var k=Object.defineProperty;var x=Object.prototype.hasOwnProperty;var g=Object.getOwnPropertySymbols,B=Object.prototype.propertyIsEnumerable;var w=(n,e,s)=>e in n?k(n,e,{enumerable:!0,configurable:!0,writable:!0,value:s}):n[e]=s,C=(n,e)=>{for(var s in e||(e={}))x.call(e,s)&&w(n,s,e[s]);if(g)for(var s of g(e))B.call(e,s)&&w(n,s,e[s]);return n};import{Q as N,u as U,A as I,i as D,c as F,t as E,M as S,N as T,b0 as P,C as A,b as j,p as L,l as R,r as l,e as O,f as H,m as o,T as q,P as M,S as V,B as u,q as z,w as Q}from"./index.f043429b.js";import{s as y}from"./screenfull.25d0aef1.js";import G from"./userNews.470802e1.js";import J from"./search.3f83c3de.js";import"./vendor.f1659326.js";var f={name:"layoutBreadcrumbUser",components:{UserNews:G,Search:J},setup(){const{proxy:n}=j(),e=N(),s=U(),a=I(),d=D({isScreenfull:!1,isShowUserNewsPopover:!1}),b=F(()=>s.state.userInfos.userInfos);return C({getUserInfos:b,onLayoutSetingClick:()=>{n.mittBus.emit("openSetingsDrawer")},onHandleCommandClick:m=>{m==="logOut"?T({closeOnClickModal:!1,closeOnPressEscape:!1,title:"\u63D0\u793A",message:"\u6B64\u64CD\u4F5C\u5C06\u9000\u51FA\u767B\u5F55, \u662F\u5426\u7EE7\u7EED?",showCancelButton:!0,confirmButtonText:"\u786E\u5B9A",cancelButtonText:"\u53D6\u6D88",beforeClose:(p,c,r)=>{p==="confirm"?(c.confirmButtonLoading=!0,c.confirmButtonText="\u9000\u51FA\u4E2D",setTimeout(()=>{r(),setTimeout(()=>{c.confirmButtonLoading=!1},300)},700)):r()}}).then(()=>{P(),A(),e.push("/login"),setTimeout(()=>{S.success("\u5B89\u5168\u9000\u51FA\u6210\u529F\uFF01")},300)}).catch(()=>{}):e.push(m)},onScreenfullClick:()=>{if(!y.isEnabled)return S.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;y.toggle(),d.isScreenfull=!d.isScreenfull},onSearchClick:()=>{a.value.openSearch()},searchRef:a},E(d))}},pe=`.layout-navbars-breadcrumb-user[data-v-4e364833] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex: 1;
}
.layout-navbars-breadcrumb-user-link[data-v-4e364833] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-4e364833] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-4e364833] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-4e364833]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-4e364833] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-4e364833] .el-badge__content.is-fixed {
  top: 12px;
}`;const t=Q("data-v-4e364833");L("data-v-4e364833");const K={class:"layout-navbars-breadcrumb-user"},W=o("i",{class:"el-icon-search",title:"\u83DC\u5355\u641C\u7D22"},null,-1),X=o("i",{class:"icon-skin iconfont",title:"\u5E03\u5C40\u914D\u7F6E"},null,-1),Y={class:"layout-navbars-breadcrumb-user-icon"},Z=o("i",{class:"el-icon-bell",title:"\u6D88\u606F"},null,-1),$={class:"layout-navbars-breadcrumb-user-link"},ee=o("i",{class:"el-icon-arrow-down el-icon--right"},null,-1),oe=u("\u9996\u9875"),ne=u("\u4E2A\u4EBA\u4E2D\u5FC3"),se=u("404"),ae=u("401"),te=u("\u9000\u51FA\u767B\u5F55");R();const re=t((n,e,s,a,d,b)=>{const v=l("el-badge"),_=l("UserNews"),h=l("el-popover"),i=l("el-dropdown-item"),m=l("el-dropdown-menu"),p=l("el-dropdown"),c=l("Search");return O(),H("div",K,[o("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:e[1]||(e[1]=(...r)=>a.onSearchClick&&a.onSearchClick(...r))},[W]),o("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:e[2]||(e[2]=(...r)=>a.onLayoutSetingClick&&a.onLayoutSetingClick(...r))},[X]),o("div",Y,[o(h,{placement:"bottom",trigger:"click",visible:n.isShowUserNewsPopover,"onUpdate:visible":e[4]||(e[4]=r=>n.isShowUserNewsPopover=r),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:t(()=>[o(v,{"is-dot":!0,onClick:e[3]||(e[3]=r=>n.isShowUserNewsPopover=!n.isShowUserNewsPopover)},{default:t(()=>[Z]),_:1})]),default:t(()=>[o(q,{name:"el-zoom-in-top"},{default:t(()=>[M(o(_,null,null,512),[[V,n.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),o("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:e[5]||(e[5]=(...r)=>a.onScreenfullClick&&a.onScreenfullClick(...r))},[o("i",{class:["iconfont",n.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:n.isScreenfull?"\u5F00\u5168\u5C4F":"\u5173\u5168\u5C4F"},null,10,["title"])]),o(p,{"show-timeout":70,"hide-timeout":50,onCommand:a.onHandleCommandClick},{dropdown:t(()=>[o(m,null,{default:t(()=>[o(i,{command:"/home"},{default:t(()=>[oe]),_:1}),o(i,{command:"/personal"},{default:t(()=>[ne]),_:1}),o(i,{command:"/404"},{default:t(()=>[se]),_:1}),o(i,{command:"/401"},{default:t(()=>[ae]),_:1}),o(i,{divided:"",command:"logOut"},{default:t(()=>[te]),_:1})]),_:1})]),default:t(()=>[o("span",$,[o("img",{src:a.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),u(" "+z(a.getUserInfos.userName===""?"test":a.getUserInfos.userName)+" ",1),ee])]),_:1},8,["onCommand"]),o(c,{ref:"searchRef"},null,512)])});f.render=re,f.__scopeId="data-v-4e364833";export default f;
