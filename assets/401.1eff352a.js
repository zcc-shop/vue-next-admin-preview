import{U as d,p as l,d as c,e as m,f,h as p,i as e,l as n,k as _,q as h}from"./vendor.a5c5c942.js";import{c as v}from"./index.4aef2fce.js";var o={name:"401",setup(){const t=d();return{onSetAuth:()=>{v(),t.push("/login")}}}},N=`.error[data-v-d583d25c] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-d583d25c] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-d583d25c] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-d583d25c] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-d583d25c] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-d583d25c] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-d583d25c] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-d583d25c] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-d583d25c] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-d583d25c] {
  width: 100%;
  height: 100%;
}`;const r=h();l("data-v-d583d25c");const g={class:"error"},u={class:"error-flex"},x={class:"left"},y={class:"left-item"},b=e("div",{class:"left-item-animation left-item-num"},"401",-1),w={class:"left-item-animation left-item-title"},S={class:"left-item-animation left-item-msg"},k={class:"left-item-animation left-item-btn"},A=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/401.png"})],-1);c();const $=r((t,a,I,i,z,B)=>{const s=m("el-button");return f(),p("div",g,[e("div",u,[e("div",x,[e("div",y,[b,e("div",w,n(t.$t("message.noAccess.accessTitle")),1),e("div",S,n(t.$t("message.noAccess.accessMsg")),1),e("div",k,[e(s,{type:"primary",round:"",onClick:i.onSetAuth},{default:r(()=>[_(n(t.$t("message.noAccess.accessBtn")),1)]),_:1},8,["onClick"])])])]),A])])});o.render=$,o.__scopeId="data-v-d583d25c";export default o;
