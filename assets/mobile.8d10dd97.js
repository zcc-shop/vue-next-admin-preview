var m=Object.assign;import{v as i,i as f,t as g,p as v,l as x,r as n,e as b,f as V,m as e,w as h,B as w}from"./index.a57f978d.js";import"./vendor.8f964570.js";var d=i({name:"login",setup(){const t=f({ruleForm:{userName:"",code:""}});return m({},g(t))}}),U=`.login-content-form[data-v-ad58d6a4] {
  margin-top: 20px;
}
.login-content-form .login-content-submit[data-v-ad58d6a4] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const o=h("data-v-ad58d6a4");v("data-v-ad58d6a4");const y=w("\u83B7\u53D6\u9A8C\u8BC1\u7801"),F=e("span",null,"\u767B \u5F55",-1);x();const I=o((t,l,N,$,B,S)=>{const s=n("el-input"),a=n("el-form-item"),p=n("el-col"),u=n("el-button"),c=n("el-row"),_=n("el-form");return b(),V(_,{class:"login-content-form"},{default:o(()=>[e(a,null,{default:o(()=>[e(s,{type:"text",placeholder:"\u8BF7\u8F93\u5165\u624B\u673A\u53F7","prefix-icon":"el-icon-user",modelValue:t.ruleForm.userName,"onUpdate:modelValue":l[1]||(l[1]=r=>t.ruleForm.userName=r),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),e(a,null,{default:o(()=>[e(c,{gutter:15},{default:o(()=>[e(p,{span:16},{default:o(()=>[e(s,{type:"text",maxlength:"4",placeholder:"\u8BF7\u8F93\u5165\u9A8C\u8BC1\u7801","prefix-icon":"el-icon-position",modelValue:t.ruleForm.code,"onUpdate:modelValue":l[2]||(l[2]=r=>t.ruleForm.code=r),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),e(p,{span:8},{default:o(()=>[e(u,{class:"login-content-form-btn"},{default:o(()=>[y]),_:1})]),_:1})]),_:1})]),_:1}),e(a,null,{default:o(()=>[e(u,{type:"primary",class:"login-content-submit",round:""},{default:o(()=>[F]),_:1})]),_:1})]),_:1})});d.render=I,d.__scopeId="data-v-ad58d6a4";export default d;
