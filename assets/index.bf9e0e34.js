var d=Object.assign;import{i as m}from"./getStyleSheets.d3c45326.js";import{i as v,k as w,t as u,p as x,l as b,r as t,e as a,f as s,m as e,F as g,s as y,q as I,w as S}from"./index.0859f0ef.js";import"./vendor.8f964570.js";var r={name:"element",setup(){const n=v({sheetsIconList:[]}),c=()=>{m.ele().then(i=>n.sheetsIconList=i)};return w(()=>{c()}),d({},u(n))}},R=`.element-container .iconfont-row[data-v-73f43fcd] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.element-container .iconfont-row .el-col[data-v-73f43fcd]:nth-last-child(1),
.element-container .iconfont-row .el-col[data-v-73f43fcd]:nth-last-child(2) {
  display: none;
}
.element-container .iconfont-row .iconfont-warp[data-v-73f43fcd] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-73f43fcd] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-73f43fcd] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-73f43fcd] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-73f43fcd] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const o=S("data-v-73f43fcd");x("data-v-73f43fcd");const L={class:"element-container"},$={class:"iconfont-warp"},k={class:"flex-margin"},j={class:"iconfont-warp-value"},B={class:"iconfont-warp-label mt10"};b();const F=o((n,c,i,q,z,C)=>{const f=t("el-col"),p=t("el-row"),_=t("el-card");return a(),s("div",L,[e(_,{shadow:"hover",header:`element plus \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${n.sheetsIconList.length-2}\u4E2A`},{default:o(()=>[e(p,{class:"iconfont-row"},{default:o(()=>[(a(!0),s(g,null,y(n.sheetsIconList,(l,h)=>(a(),s(f,{xs:12,sm:8,md:6,lg:4,xl:2,key:h},{default:o(()=>[e("div",$,[e("div",k,[e("div",j,[e("i",{class:l},null,2)]),e("div",B,I(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});r.render=F,r.__scopeId="data-v-73f43fcd";export default r;
