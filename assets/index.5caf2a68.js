import{u as m}from"./index.4b378262.js";import p from"./index.5542e7c5.js";import _ from"./tagsView.676d0325.js";import{z as f,p as u,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.a5c5c942.js";import"./breadcrumb.6ff43a0b.js";import"./user.dcf9e7d3.js";import"./screenfull.52ff4b74.js";import"./userNews.41103212.js";import"./search.00957702.js";import"./index.8e7f12f8.js";import"./horizontal.70819a42.js";import"./subItem.ef5a92f9.js";import"./contextmenu.31d06cd8.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:f(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=v();u("data-v-fde5c75e");const x={class:"layout-navbars-container"};l();const g=j((t,c,o,e,b,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
