var h=Object.defineProperty;var b=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,u=Object.prototype.propertyIsEnumerable;var p=(o,e,n)=>e in o?h(o,e,{enumerable:!0,configurable:!0,writable:!0,value:n}):o[e]=n,f=(o,e)=>{for(var n in e||(e={}))b.call(e,n)&&p(o,n,e[n]);if(d)for(var n of d(e))u.call(e,n)&&p(o,n,e[n]);return o};import{i as x}from"./getStyleSheets.141aa6e8.js";import{i as g,k as y,t as I,p as S,l as L,r as s,e as r,f as i,m as t,F as $,s as k,q as j,w as B}from"./index.f043429b.js";import"./vendor.f1659326.js";var c={name:"awesome",setup(){const o=g({sheetsIconList:[]}),e=()=>{x.awe().then(n=>o.sheetsIconList=n)};return y(()=>{e()}),f({},I(o))}},J=`.awesome-container .iconfont-row[data-v-596b4114] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.awesome-container .iconfont-row .el-col[data-v-596b4114]:nth-child(-n+24) {
  display: none;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-596b4114] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-596b4114] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-596b4114] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-596b4114] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-596b4114] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=B("data-v-596b4114");S("data-v-596b4114");const F={class:"awesome-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},C={class:"iconfont-warp-value"},D={class:"iconfont-warp-label mt10"};L();const G=a((o,e,n,M,N,R)=>{const w=s("el-col"),_=s("el-row"),m=s("el-card");return r(),i("div",F,[t(m,{shadow:"hover",header:`fontawesome \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${o.sheetsIconList.length-24}\u4E2A`},{default:a(()=>[t(_,{class:"iconfont-row"},{default:a(()=>[(r(!0),i($,null,k(o.sheetsIconList,(l,v)=>(r(),i(w,{xs:12,sm:8,md:6,lg:4,xl:2,key:v},{default:a(()=>[t("div",q,[t("div",z,[t("div",C,[t("i",{class:[l,"fa"]},null,2)]),t("div",D,j(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});c.render=G,c.__scopeId="data-v-596b4114";export default c;
