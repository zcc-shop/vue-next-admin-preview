var r=Object.defineProperty;var c=Object.prototype.hasOwnProperty;var n=Object.getOwnPropertySymbols,l=Object.prototype.propertyIsEnumerable;var d=(e,t,o)=>t in e?r(e,t,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[t]=o,i=(e,t)=>{for(var o in t||(t={}))c.call(t,o)&&d(e,o,t[o]);if(n)for(var o of n(t))l.call(t,o)&&d(e,o,t[o]);return e};import{a as p,X as _,t as u,p as v,d as y,w as h,v as f,f as m,h as w,i as a,l as g,q as D}from"./vendor.a5c5c942.js";var s={name:"layoutFooter",setup(){const e=p({isDelayFooter:!0});return _(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),i({},u(e))}},A=`.layout-footer[data-v-6d712d28] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-6d712d28] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const x=D();v("data-v-6d712d28");const F={class:"layout-footer mt15"},I={class:"layout-footer-warp"},S=a("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F",-1),$={class:"mt5"};y();const B=x((e,t,o,k,R,b)=>h((m(),w("div",F,[a("div",I,[S,a("div",$,g(e.$t("message.copyright.one5")),1)])],512)),[[f,e.isDelayFooter]]));s.render=B,s.__scopeId="data-v-6d712d28";export default s;
