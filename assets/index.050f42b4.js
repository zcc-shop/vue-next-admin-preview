var d=Object.defineProperty;var l=Object.prototype.hasOwnProperty;var n=Object.getOwnPropertySymbols,c=Object.prototype.propertyIsEnumerable;var r=(e,t,a)=>t in e?d(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,i=(e,t)=>{for(var a in t||(t={}))l.call(t,a)&&r(e,a,t[a]);if(n)for(var a of n(t))c.call(t,a)&&r(e,a,t[a]);return e};import{i as f,b2 as p,t as u,p as _,l as v,P as y,S as m,e as h,f as x,w,m as o}from"./index.f043429b.js";import"./vendor.f1659326.js";var s={name:"layoutFooter",setup(){const e=f({isDelayFooter:!0});return p(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),i({},u(e))}},R=`.layout-footer[data-v-05adff24] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-05adff24] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const g=w("data-v-05adff24");_("data-v-05adff24");const D={class:"layout-footer mt15"},F=o("div",{class:"layout-footer-warp"},[o("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F"),o("div",{class:"mt5"},"Copyright \u6DF1\u5733\u5E02 xxx \u8F6F\u4EF6\u79D1\u6280\u6709\u9650\u516C\u53F8")],-1);v();const I=g((e,t,a,S,$,b)=>y((h(),x("div",D,[F],512)),[[m,e.isDelayFooter]]));s.render=I,s.__scopeId="data-v-05adff24";export default s;
