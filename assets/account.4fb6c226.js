var x=Object.defineProperty;var F=Object.prototype.hasOwnProperty;var b=Object.getOwnPropertySymbols,y=Object.prototype.propertyIsEnumerable;var I=(e,n,o)=>n in e?x(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,v=(e,n)=>{for(var o in n||(n={}))F.call(n,o)&&I(e,o,n[o]);if(b)for(var o of b(n))y.call(n,o)&&I(e,o,n[o]);return e};import{x as k,y as S,U as V,a as $,z as L,t as A,s as B,p as C,d as N,e as u,f as P,h as T,i as t,l as j,q as R}from"./vendor.a5c5c942.js";import{u as U,b as w,L as D,M as H,f as q}from"./index.4aef2fce.js";var g=k({name:"login",setup(){const{t:e}=S(),n=U(),o=V(),i=$({ruleForm:{userName:"admin",password:"123456",code:"1234"},loading:{signIn:!1}}),d=L(()=>q(new Date)),h=async()=>{i.loading.signIn=!0;let s=[],l=[],m=["admin"],p=["btn.add","btn.del","btn.edit","btn.link"],f=["test"],r=["btn.add","btn.link"];i.ruleForm.userName==="admin"?(s=m,l=p):(s=f,l=r);const _={userName:i.ruleForm.userName,photo:i.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:s,authBtnList:l};w("token",Math.random().toString(36).substr(0)),w("userInfo",_),n.dispatch("userInfos/setUserInfos",_),n.state.themeConfig.themeConfig.isRequestRoutes?(await H(),c()):(await D(),c())},c=()=>{let s=d.value;o.push("/"),setTimeout(()=>{i.loading.signIn=!0;const l=e("message.signInText");B.success(`${s}\uFF0C${l}`)},300)};return v({currentTime:d,onSignIn:h},A(i))}}),W=`.login-content-form[data-v-44b3c7e9] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-44b3c7e9] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-44b3c7e9] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
  user-select: none;
}
.login-content-form .login-content-code .login-content-code-img[data-v-44b3c7e9]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-44b3c7e9] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const a=R();C("data-v-44b3c7e9");const z=t("div",{class:"login-content-code"},[t("span",{class:"login-content-code-img"},"1234")],-1);N();const G=a((e,n,o,i,d,h)=>{const c=u("el-input"),s=u("el-form-item"),l=u("el-col"),m=u("el-row"),p=u("el-button"),f=u("el-form");return P(),T(f,{class:"login-content-form"},{default:a(()=>[t(s,null,{default:a(()=>[t(c,{type:"text",placeholder:e.$t("message.account.accountPlaceholder1"),"prefix-icon":"el-icon-user",modelValue:e.ruleForm.userName,"onUpdate:modelValue":n[1]||(n[1]=r=>e.ruleForm.userName=r),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(c,{type:"password",placeholder:e.$t("message.account.accountPlaceholder2"),"prefix-icon":"el-icon-lock",modelValue:e.ruleForm.password,"onUpdate:modelValue":n[2]||(n[2]=r=>e.ruleForm.password=r),autocomplete:"off","show-password":""},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(m,{gutter:15},{default:a(()=>[t(l,{span:16},{default:a(()=>[t(c,{type:"text",maxlength:"4",placeholder:e.$t("message.account.accountPlaceholder3"),"prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":n[3]||(n[3]=r=>e.ruleForm.code=r),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(l,{span:8},{default:a(()=>[z]),_:1})]),_:1})]),_:1}),t(s,null,{default:a(()=>[t(p,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:a(()=>[t("span",null,j(e.$t("message.account.accountBtnText")),1)]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});g.render=G,g.__scopeId="data-v-44b3c7e9";export default g;
