var v=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(e,o,n)=>o in e?v(e,o,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[o]=n,p=(e,o)=>{for(var n in o||(o={}))u.call(o,n)&&f(e,n,o[n]);if(d)for(var n of d(o))b.call(o,n)&&f(e,n,o[n]);return e};import{i as x}from"./getStyleSheets.9ca082b7.js";import{r as g,j as y,t as I,l as S,m as L,q as s,y as r,z as i,A as t,L as $,V as j,D as k,F as z}from"./vendor.69bd2edd.js";var c={name:"pagesAwesome",setup(){const e=g({sheetsIconList:[]}),o=()=>{x.awe().then(n=>e.sheetsIconList=n)};return y(()=>{o()}),p({},I(e))}},H=`.awesome-container .iconfont-row[data-v-d2f15e60] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.awesome-container .iconfont-row .el-col[data-v-d2f15e60]:nth-child(-n+24) {
  display: none;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-d2f15e60] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-d2f15e60] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-d2f15e60] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-d2f15e60] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-d2f15e60] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=z();S("data-v-d2f15e60");const A={class:"awesome-container"},B={class:"iconfont-warp"},D={class:"flex-margin"},F={class:"iconfont-warp-value"},V={class:"iconfont-warp-label mt10"};L();const q=a((e,o,n,C,G,M)=>{const _=s("el-col"),w=s("el-row"),h=s("el-card");return r(),i("div",A,[t(h,{shadow:"hover",header:`fontawesome \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-24}\u4E2A`},{default:a(()=>[t(w,{class:"iconfont-row"},{default:a(()=>[(r(!0),i($,null,j(e.sheetsIconList,(l,m)=>(r(),i(_,{xs:12,sm:8,md:6,lg:4,xl:2,key:m},{default:a(()=>[t("div",B,[t("div",D,[t("div",F,[t("i",{class:[l,"fa"]},null,2)]),t("div",V,k(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});c.render=q,c.__scopeId="data-v-d2f15e60";export default c;
