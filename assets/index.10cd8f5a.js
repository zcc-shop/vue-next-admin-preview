import{u as m,c as p,p as f,l as _,r as a,e as r,f as n,m as u,h as l,w as h}from"./index.0859f0ef.js";import w from"./index.dfb2dd27.js";import v from"./tagsView.f07cdd3f.js";import"./vendor.8f964570.js";import"./breadcrumb.fe1119f6.js";import"./user.306d7fe0.js";import"./screenfull.77def122.js";import"./userNews.aa3e041f.js";import"./search.1d737a16.js";import"./index.a238ad44.js";import"./horizontal.d6e24224.js";import"./subItem.2b60b063.js";import"./contextmenu.87a9f08c.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h("data-v-fde5c75e");f("data-v-fde5c75e");const x={class:"layout-navbars-container"};_();const g=j((t,c,o,e,b,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",x,[u(d),e.setShowTagsView?(r(),n(i,{key:0})):l("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
