import{u as m}from"./index.85d2a956.js";import p from"./index.bfa25c77.js";import _ from"./tagsView.66fdc10d.js";import{c as u,l as f,m as l,q as a,y as r,z as c,A as b,M as h,F as w}from"./vendor.69bd2edd.js";import"./breadcrumb.af1c8a4e.js";import"./user.fc98c8bc.js";import"./screenfull.e0775b6b.js";import"./userNews.a2c52168.js";import"./search.f2b556ce.js";import"./index.eb6d91cc.js";import"./horizontal.be9fb9e3.js";import"./subItem.4444a1c5.js";import"./contextmenu.99bf673d.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,n,o,e,g,y)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),c("div",j,[b(i),e.setShowTagsView?(r(),c(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
