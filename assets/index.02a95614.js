var w=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var p=(n,e,o)=>e in n?w(n,e,{enumerable:!0,configurable:!0,writable:!0,value:o}):n[e]=o,f=(n,e)=>{for(var o in e||(e={}))u.call(e,o)&&p(n,o,e[o]);if(d)for(var o of d(e))b.call(e,o)&&p(n,o,e[o]);return n};import{i as x}from"./getStyleSheets.aba6ce1e.js";import{a as g,o as y,t as I,p as S,d as L,e as s,f as r,h as c,i as t,F as $,Y as k,l as j,q as B}from"./vendor.a5c5c942.js";var i={name:"pagesElement",setup(){const n=g({sheetsIconList:[]}),e=()=>{x.ele().then(o=>n.sheetsIconList=o)};return y(()=>{e()}),f({},I(n))}},A=`.element-container .iconfont-row[data-v-d9a4330c] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.element-container .iconfont-row .el-col[data-v-d9a4330c]:nth-last-child(1),
.element-container .iconfont-row .el-col[data-v-d9a4330c]:nth-last-child(2) {
  display: none;
}
.element-container .iconfont-row .iconfont-warp[data-v-d9a4330c] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp[data-v-d9a4330c]:hover {
  box-shadow: 0 2px 12px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-d9a4330c] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-d9a4330c] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-d9a4330c] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.element-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-d9a4330c] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const a=B();S("data-v-d9a4330c");const F={class:"element-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},C={class:"iconfont-warp-value"},D={class:"iconfont-warp-label mt10"};L();const E=a((n,e,o,G,M,N)=>{const _=s("el-col"),h=s("el-row"),v=s("el-card");return r(),c("div",F,[t(v,{shadow:"hover",header:`element plus \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${n.sheetsIconList.length-2}\u4E2A`},{default:a(()=>[t(h,{class:"iconfont-row"},{default:a(()=>[(r(!0),c($,null,k(n.sheetsIconList,(l,m)=>(r(),c(_,{xs:12,sm:8,md:6,lg:4,xl:2,key:m},{default:a(()=>[t("div",q,[t("div",z,[t("div",C,[t("i",{class:l},null,2)]),t("div",D,j(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});i.render=E,i.__scopeId="data-v-d9a4330c";export default i;
