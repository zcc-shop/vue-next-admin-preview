var w=Object.defineProperty;var I=Object.prototype.hasOwnProperty;var h=Object.getOwnPropertySymbols,k=Object.prototype.propertyIsEnumerable;var v=(e,n,t)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[n]=t,F=(e,n)=>{for(var t in n||(n={}))I.call(n,t)&&v(e,t,n[t]);if(h)for(var t of h(n))k.call(n,t)&&v(e,t,n[t]);return e};import{v as y,u as S,Q as C,i as V,c as B,t as A,D as b,aZ as L,a_ as N,a$ as j,M as R,j as $,p as D,l as E,r,e as P,f as T,m as o,w as U}from"./index.f043429b.js";import"./vendor.f1659326.js";var g=y({name:"login",setup(){const e=S(),n=C(),t=V({ruleForm:{userName:"admin",password:"123456",code:"1234"},loading:{signIn:!1}}),u=B(()=>$(new Date)),_=()=>{t.loading.signIn=!0;let a=[],l=[],d=["admin"],m=["btn.add","btn.del","btn.edit","btn.link"],p=["test"],f=["btn.add","btn.link"];t.ruleForm.userName==="admin"?(a=d,l=m):(a=p,l=f);const i={userName:t.ruleForm.userName,photo:t.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:a,authBtnList:l};b("token",Math.random().toString(36).substr(0)),b("userInfo",i),e.dispatch("userInfos/setUserInfos",i),e.state.themeConfig.themeConfig.isRequestRoutes?N(x=>{j(x,()=>{c()})}):(L(),c())},c=()=>{let a=u.value;n.push("/"),setTimeout(()=>{t.loading.signIn=!0,R.success(`${a}\uFF0C\u6B22\u8FCE\u56DE\u6765\uFF01`)},300)};return F({currentTime:u,onSignIn:_},A(t))}}),Y=`.login-content-form[data-v-0259d8ad] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-0259d8ad] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-0259d8ad] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
}
.login-content-form .login-content-code .login-content-code-img[data-v-0259d8ad]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-0259d8ad] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const s=U("data-v-0259d8ad");D("data-v-0259d8ad");const H=o("div",{class:"login-content-code"},[o("span",{class:"login-content-code-img"},"1234")],-1),Q=o("span",null,"\u767B \u5F55",-1);E();const G=s((e,n,t,u,_,c)=>{const a=r("el-input"),l=r("el-form-item"),d=r("el-col"),m=r("el-row"),p=r("el-button"),f=r("el-form");return P(),T(f,{class:"login-content-form"},{default:s(()=>[o(l,null,{default:s(()=>[o(a,{type:"text",placeholder:"\u7528\u6237\u540D admin \u6216\u4E0D\u8F93\u5747\u4E3A test","prefix-icon":"el-icon-user",modelValue:e.ruleForm.userName,"onUpdate:modelValue":n[1]||(n[1]=i=>e.ruleForm.userName=i),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),o(l,null,{default:s(()=>[o(a,{type:"password",placeholder:"\u5BC6\u7801\uFF1A123456","prefix-icon":"el-icon-lock",modelValue:e.ruleForm.password,"onUpdate:modelValue":n[2]||(n[2]=i=>e.ruleForm.password=i),autocomplete:"off","show-password":""},null,8,["modelValue"])]),_:1}),o(l,null,{default:s(()=>[o(m,{gutter:15},{default:s(()=>[o(d,{span:16},{default:s(()=>[o(a,{type:"text",maxlength:"4",placeholder:"\u8BF7\u8F93\u5165\u9A8C\u8BC1\u7801","prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":n[3]||(n[3]=i=>e.ruleForm.code=i),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),o(d,{span:8},{default:s(()=>[H]),_:1})]),_:1})]),_:1}),o(l,null,{default:s(()=>[o(p,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:s(()=>[Q]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});g.render=G,g.__scopeId="data-v-0259d8ad";export default g;
