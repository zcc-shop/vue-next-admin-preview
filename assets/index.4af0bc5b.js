var g=Object.defineProperty;var _=Object.prototype.hasOwnProperty;var c=Object.getOwnPropertySymbols,y=Object.prototype.propertyIsEnumerable;var w=(n,a,t)=>a in n?g(n,a,{enumerable:!0,configurable:!0,writable:!0,value:t}):n[a]=t,p=(n,a)=>{for(var t in a||(a={}))_.call(a,t)&&w(n,t,a[t]);if(c)for(var t of c(a))y.call(a,t)&&w(n,t,a[t]);return n};import{a as x,t as b,p as k,d as I,e as S,a1 as $,f as r,h as f,i as e,F as m,Y as u,w as v,l as h,q as D}from"./vendor.9b51066d.js";var o={name:"pagesWaterfall",setup(){const n=x({});return p({},b(n))}},E=`.waterfall-container .waterfall-first[data-v-19cf2eda] {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(188px, 1fr));
  grid-gap: 0.25em;
  grid-auto-flow: row dense;
  grid-auto-rows: 20px;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-19cf2eda] {
  width: 100%;
  background: var(--color-primary);
  color: #ffffff;
  transition: all 0.3s ease;
  border-radius: 3px;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-19cf2eda]:nth-of-type(3n + 1) {
  grid-row: auto/span 5;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-19cf2eda]:nth-of-type(3n + 2) {
  grid-row: auto/span 6;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-19cf2eda]:nth-of-type(3n + 3) {
  grid-row: auto/span 8;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-19cf2eda]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease;
  cursor: pointer;
}
.waterfall-container .waterfall-last[data-v-19cf2eda] {
  display: grid;
  grid-gap: 0.25em;
  grid-auto-flow: row dense;
  grid-auto-rows: minmax(188px, 20vmin);
  grid-template-columns: 1fr;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda] {
  height: 100%;
  background: var(--color-primary);
  color: #ffffff;
  transition: all 0.3s ease;
  border-radius: 3px;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease;
  cursor: pointer;
}
@media (min-width: 576px) {
.waterfall-container .waterfall-last[data-v-19cf2eda] {
    grid-template-columns: repeat(7, 1fr);
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 9) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 8) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 7) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 6) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 5) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 4) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 3) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 2) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(9n + 1) {
    grid-column: auto/span 2;
}
}
@media (min-width: 576px) and (min-width: 1024px) {
.waterfall-container .waterfall-last[data-v-19cf2eda] {
    grid-template-columns: repeat(14, 1fr);
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 15) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 14) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 13) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 12) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 11) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 10) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 9) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 8) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 7) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 6) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 5) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 4) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 3) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 2) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-19cf2eda]:nth-of-type(15n + 1) {
    grid-column: auto/span 2;
}
}`;const s=D();k("data-v-19cf2eda");const B={class:"waterfall-container"},F={class:"waterfall-first"},j={class:"w100 h100 flex"},q={class:"flex-margin"},C={class:"waterfall-last"},L={class:"w100 h100 flex"},N={class:"flex-margin"};I();const R=s((n,a,t,V,W,Y)=>{const i=S("el-card"),d=$("waves");return r(),f("div",B,[e(i,{shadow:"hover",header:"\u7011\u5E03\u5C4F\uFF08\u5E03\u5C40\u4E00\uFF09",class:"mb15"},{default:s(()=>[e("div",F,[(r(),f(m,null,u(30,l=>v(e("div",{class:"waterfall-first-item",key:l},[e("div",j,[e("span",q,h(l),1)])],512),[[d]])),64))])]),_:1}),e(i,{shadow:"hover",header:"\u7011\u5E03\u5C4F\uFF08\u5E03\u5C40\u4E8C\uFF09"},{default:s(()=>[e("div",C,[(r(),f(m,null,u(30,l=>v(e("div",{class:"waterfall-last-item",key:l},[e("div",L,[e("span",N,h(l),1)])],512),[[d,"light"]])),64))])]),_:1})])});o.render=R,o.__scopeId="data-v-19cf2eda";export default o;
