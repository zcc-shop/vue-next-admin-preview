var I=Object.defineProperty;var L=Object.prototype.hasOwnProperty;var C=Object.getOwnPropertySymbols,T=Object.prototype.propertyIsEnumerable;var v=(r,e,a)=>e in r?I(r,e,{enumerable:!0,configurable:!0,writable:!0,value:a}):r[e]=a,_=(r,e)=>{for(var a in e||(e={}))L.call(e,a)&&v(r,a,e[a]);if(C)for(var a of C(e))T.call(e,a)&&v(r,a,e[a]);return r};import{u as w,x as F,Q as R,i as M,c as N,k as V,b2 as j,t as z,b as D,p as q,l as G,r as y,P,S as Q,e as u,f as c,m as p,b8 as U,F as A,s as E,h as S,B as x,q as B,b7 as H,w as J}from"./index.f043429b.js";import"./vendor.f1659326.js";var f={name:"layoutBreadcrumb",setup(){const{proxy:r}=D(),e=w(),a=F(),n=R(),t=M({breadcrumbList:[],routeSplit:[],routeSplitFirst:"",routeSplitIndex:1}),h=N(()=>e.state.themeConfig.themeConfig),b=s=>{const{redirect:i,path:g}=s;i?n.push(i):n.push(g)},m=()=>{r.mittBus.emit("onMenuClick"),e.state.themeConfig.themeConfig.isCollapse=!e.state.themeConfig.themeConfig.isCollapse},o=s=>{s.map(i=>{t.routeSplit.map((g,X,k)=>{t.routeSplitFirst===i.path&&(t.routeSplitFirst+=`/${k[t.routeSplitIndex]}`,t.breadcrumbList.push(i),t.routeSplitIndex++,i.children&&o(i.children))})})},l=s=>{if(!e.state.themeConfig.themeConfig.isBreadcrumb)return!1;t.breadcrumbList=[e.state.routesList.routesList[0]],t.routeSplit=s.split("/"),t.routeSplit.shift(),t.routeSplitFirst=`/${t.routeSplit[0]}`,t.routeSplitIndex=1,o(e.state.routesList.routesList)};return V(()=>{l(a.path)}),j(s=>{l(s.path)}),_({onThemeConfigChange:m,getThemeConfig:h,onBreadcrumbClick:b},z(t))}},ee=`.layout-navbars-breadcrumb[data-v-da77af00] {
  flex: 1;
  height: inherit;
  display: flex;
  align-items: center;
  padding-left: 15px;
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-icon[data-v-da77af00] {
  cursor: pointer;
  font-size: 18px;
  margin-right: 15px;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-span[data-v-da77af00] {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-iconfont[data-v-da77af00] {
  font-size: 14px;
  margin-right: 5px;
}
.layout-navbars-breadcrumb[data-v-da77af00] .el-breadcrumb__separator {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}`;const d=J("data-v-da77af00");q("data-v-da77af00");const K={class:"layout-navbars-breadcrumb"},O={key:0,class:"layout-navbars-breadcrumb-span"};G();const W=d((r,e,a,n,t,h)=>{const b=y("el-breadcrumb-item"),m=y("el-breadcrumb");return P((u(),c("div",K,[p("i",{class:["layout-navbars-breadcrumb-icon",n.getThemeConfig.isCollapse?"el-icon-s-unfold":"el-icon-s-fold"],onClick:e[1]||(e[1]=(...o)=>n.onThemeConfigChange&&n.onThemeConfigChange(...o))},null,2),p(m,{class:"layout-navbars-breadcrumb-hide"},{default:d(()=>[p(U,{name:"breadcrumb",mode:"out-in"},{default:d(()=>[(u(!0),c(A,null,E(r.breadcrumbList,(o,l)=>(u(),c(b,{key:o.meta.title},{default:d(()=>[l===r.breadcrumbList.length-1?(u(),c("span",O,[n.getThemeConfig.isBreadcrumbIcon?(u(),c("i",{key:0,class:[o.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):S("",!0),x(B(o.meta.title),1)])):(u(),c("a",{key:1,onClick:H(s=>n.onBreadcrumbClick(o),["prevent"])},[n.getThemeConfig.isBreadcrumbIcon?(u(),c("i",{key:0,class:[o.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):S("",!0),x(B(o.meta.title),1)],8,["onClick"]))]),_:2},1024))),128))]),_:1})]),_:1})],512)),[[Q,n.getThemeConfig.isBreadcrumb]])});f.render=W,f.__scopeId="data-v-da77af00";export default f;
