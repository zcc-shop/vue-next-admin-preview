var i=Object.defineProperty;var c=Object.prototype.hasOwnProperty;var n=Object.getOwnPropertySymbols,l=Object.prototype.propertyIsEnumerable;var r=(e,t,o)=>t in e?i(e,t,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[t]=o,d=(e,t)=>{for(var o in t||(t={}))c.call(t,o)&&r(e,o,t[o]);if(n)for(var o of n(t))l.call(t,o)&&r(e,o,t[o]);return e};import{az as p}from"./index.0e1be16b.js";import{r as _,t as u,l as v,m as y,v as m,x as f,y as h,z as w,A as a,D as g,F as x}from"./vendor.c148b95a.js";var s={name:"layoutFooter",setup(){const e=_({isDelayFooter:!0});return p(()=>{e.isDelayFooter=!1,setTimeout(()=>{e.isDelayFooter=!0},800)}),d({},u(e))}},M=`.layout-footer[data-v-6d712d28] {
  width: 100%;
  display: flex;
}
.layout-footer-warp[data-v-6d712d28] {
  margin: auto;
  color: #9e9e9e;
  text-align: center;
  animation: logoAnimation 0.3s ease-in-out;
}`;const D=x();v("data-v-6d712d28");const F={class:"layout-footer mt15"},I={class:"layout-footer-warp"},S=a("div",null,"vue-next-admin\uFF0CMade by lyt with \u2764\uFE0F",-1),$={class:"mt5"};y();const b=D((e,t,o,B,j,k)=>m((h(),w("div",F,[a("div",I,[S,a("div",$,g(e.$t("message.copyright.one5")),1)])],512)),[[f,e.isDelayFooter]]));s.render=b,s.__scopeId="data-v-6d712d28";export default s;
