var P=Object.defineProperty;var j=Object.prototype.hasOwnProperty;var N=Object.getOwnPropertySymbols,D=Object.prototype.propertyIsEnumerable;var B=(e,o,l)=>o in e?P(e,o,{enumerable:!0,configurable:!0,writable:!0,value:l}):e[o]=l,O=(e,o)=>{for(var l in o||(o={}))j.call(o,l)&&B(e,l,o[l]);if(N)for(var l of N(o))D.call(o,l)&&B(e,l,o[l]);return e};import{y as R,U as F,r as q,a as H,z as _,o as V,t as A,s as T,O as G,g as J,p as K,d as Q,e as f,f as W,h as X,i as s,k as i,l as c,T as Y,w as Z,v as x,q as ee}from"./vendor.a5c5c942.js";import{s as $}from"./screenfull.52ff4b74.js";import{u as se,g as y,c as ne,d as oe,e as L,s as E}from"./index.4aef2fce.js";import ae from"./userNews.41103212.js";import le from"./search.780935a1.js";var k={name:"layoutBreadcrumbUser",components:{UserNews:ae,Search:le},setup(){const{t:e}=R(),{proxy:o}=J(),l=F(),a=se(),p=q(),d=H({isScreenfull:!1,isShowUserNewsPopover:!1,disabledI18n:"zh-cn",disabledSize:"small"}),t=_(()=>a.state.userInfos.userInfos),m=_(()=>a.state.themeConfig.themeConfig),g=_(()=>{let{layout:r,isClassicSplitMenu:S}=m.value,b="";return r==="defaults"||r==="classic"&&!S||r==="columns"?b=1:b=null,b}),h=()=>{if(!$.isEnabled)return T.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;$.toggle(),d.isScreenfull=!d.isScreenfull},v=()=>{o.mittBus.emit("openSetingsDrawer")},w=r=>{r==="logOut"?G({closeOnClickModal:!1,closeOnPressEscape:!1,title:e("message.user.logOutTitle"),message:e("message.user.logOutMessage"),showCancelButton:!0,confirmButtonText:e("message.user.logOutConfirm"),cancelButtonText:e("message.user.logOutCancel"),beforeClose:(S,b,U)=>{S==="confirm"?(b.confirmButtonLoading=!0,b.confirmButtonText=e("message.user.logOutExit"),setTimeout(()=>{U(),setTimeout(()=>{b.confirmButtonLoading=!1},300)},700)):U()}}).then(()=>{ne(),oe(),l.push("/login"),setTimeout(()=>{T.success(e("message.user.logOutSuccess"))},300)}).catch(()=>{}):l.push(r)},C=()=>{p.value.openSearch()},u=r=>{L("themeConfig"),m.value.globalComponentSize=r,E("themeConfig",m.value),o.$ELEMENT.size=r,I()},M=r=>{L("themeConfig"),m.value.globalI18n=r,E("themeConfig",m.value),o.$i18n.locale=r,z()},z=()=>{switch(y("themeConfig").globalI18n){case"zh-cn":d.disabledI18n="zh-cn";break;case"en":d.disabledI18n="en";break;case"zh-tw":d.disabledI18n="zh-tw";break}},I=()=>{switch(y("themeConfig").globalComponentSize){case"default":d.disabledSize="default";break;case"medium":d.disabledSize="medium";break;case"small":d.disabledSize="small";break;case"mini":d.disabledSize="mini";break}};return V(()=>{y("themeConfig")&&(z(),I())}),O({getUserInfos:t,onLayoutSetingClick:v,onHandleCommandClick:w,onScreenfullClick:h,onSearchClick:C,onComponentSizeChange:u,onLanguageChange:M,searchRef:p,layoutUserFlexNum:g},A(d))}},Se=`.layout-navbars-breadcrumb-user[data-v-183ee15a] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.layout-navbars-breadcrumb-user-link[data-v-183ee15a] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-183ee15a] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-183ee15a] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-183ee15a]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-183ee15a] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-183ee15a] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-183ee15a] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-183ee15a] .el-badge__content.is-fixed {
  top: 12px;
}`;const n=ee();K("data-v-183ee15a");const te={class:"layout-navbars-breadcrumb-user-icon"},ie={class:"layout-navbars-breadcrumb-user-icon"},re=i("\u7B80\u4F53\u4E2D\u6587"),de=i("English"),ue=i("\u7E41\u9AD4\u4E2D\u6587"),ce={class:"layout-navbars-breadcrumb-user-icon"},me={class:"layout-navbars-breadcrumb-user-link"},be=s("i",{class:"el-icon-arrow-down el-icon--right"},null,-1);Q();const fe=n((e,o,l,a,p,d)=>{const t=f("el-dropdown-item"),m=f("el-dropdown-menu"),g=f("el-dropdown"),h=f("el-badge"),v=f("UserNews"),w=f("el-popover"),C=f("Search");return W(),X("div",{class:"layout-navbars-breadcrumb-user",style:{flex:a.layoutUserFlexNum}},[s(g,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:a.onComponentSizeChange},{dropdown:n(()=>[s(m,null,{default:n(()=>[s(t,{command:"default",disabled:e.disabledSize==="default"},{default:n(()=>[i(c(e.$t("message.user.dropdownDefault")),1)]),_:1},8,["disabled"]),s(t,{command:"medium",disabled:e.disabledSize==="medium"},{default:n(()=>[i(c(e.$t("message.user.dropdownMedium")),1)]),_:1},8,["disabled"]),s(t,{command:"small",disabled:e.disabledSize==="small"},{default:n(()=>[i(c(e.$t("message.user.dropdownSmall")),1)]),_:1},8,["disabled"]),s(t,{command:"mini",disabled:e.disabledSize==="mini"},{default:n(()=>[i(c(e.$t("message.user.dropdownMini")),1)]),_:1},8,["disabled"])]),_:1})]),default:n(()=>[s("div",te,[s("i",{class:"iconfont icon-ziti",title:e.$t("message.user.title0")},null,8,["title"])])]),_:1},8,["onCommand"]),s(g,{"show-timeout":70,"hide-timeout":50,trigger:"click",onCommand:a.onLanguageChange},{dropdown:n(()=>[s(m,null,{default:n(()=>[s(t,{command:"zh-cn",disabled:e.disabledI18n==="zh-cn"},{default:n(()=>[re]),_:1},8,["disabled"]),s(t,{command:"en",disabled:e.disabledI18n==="en"},{default:n(()=>[de]),_:1},8,["disabled"]),s(t,{command:"zh-tw",disabled:e.disabledI18n==="zh-tw"},{default:n(()=>[ue]),_:1},8,["disabled"])]),_:1})]),default:n(()=>[s("div",ie,[s("i",{class:["iconfont",e.disabledI18n==="en"?"icon-fuhao-yingwen":"icon-fuhao-zhongwen"],title:e.$t("message.user.title1")},null,10,["title"])])]),_:1},8,["onCommand"]),s("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:o[1]||(o[1]=(...u)=>a.onSearchClick&&a.onSearchClick(...u))},[s("i",{class:"el-icon-search",title:e.$t("message.user.title2")},null,8,["title"])]),s("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:o[2]||(o[2]=(...u)=>a.onLayoutSetingClick&&a.onLayoutSetingClick(...u))},[s("i",{class:"icon-skin iconfont",title:e.$t("message.user.title3")},null,8,["title"])]),s("div",ce,[s(w,{placement:"bottom",trigger:"click",visible:e.isShowUserNewsPopover,"onUpdate:visible":o[4]||(o[4]=u=>e.isShowUserNewsPopover=u),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:n(()=>[s(h,{"is-dot":!0,onClick:o[3]||(o[3]=u=>e.isShowUserNewsPopover=!e.isShowUserNewsPopover)},{default:n(()=>[s("i",{class:"el-icon-bell",title:e.$t("message.user.title4")},null,8,["title"])]),_:1})]),default:n(()=>[s(Y,{name:"el-zoom-in-top"},{default:n(()=>[Z(s(v,null,null,512),[[x,e.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),s("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:o[5]||(o[5]=(...u)=>a.onScreenfullClick&&a.onScreenfullClick(...u))},[s("i",{class:["iconfont",e.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:e.isScreenfull?e.$t("message.user.title5"):e.$t("message.user.title6")},null,10,["title"])]),s(g,{"show-timeout":70,"hide-timeout":50,onCommand:a.onHandleCommandClick},{dropdown:n(()=>[s(m,null,{default:n(()=>[s(t,{command:"/home"},{default:n(()=>[i(c(e.$t("message.user.dropdown1")),1)]),_:1}),s(t,{command:"/personal"},{default:n(()=>[i(c(e.$t("message.user.dropdown2")),1)]),_:1}),s(t,{command:"/404"},{default:n(()=>[i(c(e.$t("message.user.dropdown3")),1)]),_:1}),s(t,{command:"/401"},{default:n(()=>[i(c(e.$t("message.user.dropdown4")),1)]),_:1}),s(t,{divided:"",command:"logOut"},{default:n(()=>[i(c(e.$t("message.user.dropdown5")),1)]),_:1})]),_:1})]),default:n(()=>[s("span",me,[s("img",{src:a.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),i(" "+c(a.getUserInfos.userName===""?"test":a.getUserInfos.userName)+" ",1),be])]),_:1},8,["onCommand"]),s(C,{ref:"searchRef"},null,512)],4)});k.render=fe,k.__scopeId="data-v-183ee15a";export default k;
