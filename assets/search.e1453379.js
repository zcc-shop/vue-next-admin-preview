var p=Object.assign;import{v as S,A as g,u as _,Q as V,i as v,t as y,n as b,p as L,l as B,r as w,e as k,f as x,m as i,B as C,q as I,w as R}from"./index.51953939.js";import"./vendor.8f964570.js";var m=S({name:"layoutBreadcrumbSearch",setup(){const o=g(),s=_(),u=V(),t=v({isShowSearch:!1,menuQuery:"",tagsViewList:[]}),f=()=>{t.menuQuery="",t.isShowSearch=!0,a(),b(()=>{o.value.focus()})},l=()=>{t.isShowSearch=!1},d=(e,n)=>{let r=e?t.tagsViewList.filter(c(e)):t.tagsViewList;n(r)},c=e=>n=>n.path.toLowerCase().indexOf(e.toLowerCase())>-1||n.meta.title.toLowerCase().indexOf(e.toLowerCase())>-1,a=()=>{if(t.tagsViewList.length>0)return!1;s.state.tagsViewRoutes.tagsViewRoutes.map(e=>{e.meta.isHide||t.tagsViewList.push(p({},e))})};return p({layoutMenuAutocompleteRef:o,openSearch:f,closeSearch:l,menuSearch:d,onHandleSelect:e=>{let{path:n,redirect:r}=e;e.meta.isLink&&!e.meta.isIframe?window.open(e.meta.isLink):r?u.push(r):u.push(n),l()},onSearchBlur:()=>{l()}},y(t))}}),N=`.layout-search-dialog[data-v-63d5bde2] .el-dialog {
  box-shadow: unset !important;
  border-radius: 0 !important;
  background: rgba(0, 0, 0, 0.5);
}
.layout-search-dialog[data-v-63d5bde2] .el-autocomplete {
  width: 560px;
  position: absolute;
  top: 100px;
  left: 50%;
  transform: translateX(-50%);
}`;const h=R("data-v-63d5bde2");L("data-v-63d5bde2");const Q={class:"layout-search-dialog"};B();const H=h((o,s,u,t,f,l)=>{const d=w("el-autocomplete"),c=w("el-dialog");return k(),x("div",Q,[i(c,{modelValue:o.isShowSearch,"onUpdate:modelValue":s[2]||(s[2]=a=>o.isShowSearch=a),width:"300px","destroy-on-close":"",modal:!1,fullscreen:"","show-close":!1},{default:h(()=>[i(d,{modelValue:o.menuQuery,"onUpdate:modelValue":s[1]||(s[1]=a=>o.menuQuery=a),"fetch-suggestions":o.menuSearch,placeholder:"\u83DC\u5355\u641C\u7D22\uFF1A\u652F\u6301\u4E2D\u6587\u3001\u8DEF\u7531\u8DEF\u5F84","prefix-icon":"el-icon-search",ref:"layoutMenuAutocompleteRef",onSelect:o.onHandleSelect,onBlur:o.onSearchBlur},{default:h(({item:a})=>[i("div",null,[i("i",{class:[a.meta.icon,"mr10"]},null,2),C(I(a.meta.title),1)])]),_:1},8,["modelValue","fetch-suggestions","onSelect","onBlur"])]),_:1},8,["modelValue"])])});m.render=H,m.__scopeId="data-v-63d5bde2";export default m;
