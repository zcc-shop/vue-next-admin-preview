import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as h}from"./index.f043429b.js";import w from"./index.a6a96c9b.js";import v from"./tagsView.2602c069.js";import"./vendor.f1659326.js";import"./breadcrumb.1fecc687.js";import"./user.2d3d9794.js";import"./screenfull.25d0aef1.js";import"./userNews.470802e1.js";import"./search.3f83c3de.js";import"./index.742de463.js";import"./horizontal.f1588990.js";import"./subItem.acddfd5d.js";import"./contextmenu.24b0cbf7.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h("data-v-fde5c75e");_("data-v-fde5c75e");const x={class:"layout-navbars-container"};u();const b=j((t,c,o,e,g,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",x,[f(d),e.setShowTagsView?(r(),n(i,{key:0})):l("",!0)])});s.render=b,s.__scopeId="data-v-fde5c75e";export default s;
