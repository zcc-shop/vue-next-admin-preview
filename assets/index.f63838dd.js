import{u as m}from"./index.ead4f915.js";import p from"./index.8fb974cd.js";import _ from"./tagsView.73612b66.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as b,E as h,q as w}from"./vendor.9b51066d.js";import"./breadcrumb.d32c6c2d.js";import"./user.729b5d04.js";import"./screenfull.75a325ae.js";import"./userNews.d597db99.js";import"./search.b61e76e4.js";import"./index.a225b6a3.js";import"./horizontal.fb2ed951.js";import"./subItem.c2830d89.js";import"./contextmenu.fc2234cb.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,I)=>{const d=a("BreadcrumbIndex"),i=a("TagsView");return r(),n("div",j,[b(d),e.setShowTagsView?(r(),n(i,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
