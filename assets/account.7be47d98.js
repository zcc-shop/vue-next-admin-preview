var x=Object.defineProperty;var k=Object.prototype.hasOwnProperty;var b=Object.getOwnPropertySymbols,y=Object.prototype.propertyIsEnumerable;var I=(e,n,o)=>n in e?x(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,v=(e,n)=>{for(var o in n||(n={}))k.call(n,o)&&I(e,o,n[o]);if(b)for(var o of b(n))y.call(n,o)&&I(e,o,n[o]);return e};import{v as S,D as V,u as $,R as B,i as C,c as A,t as L,G as F,b1 as N,b2 as P,b3 as R,O as T,j,p as D,l as U,r as u,e as G,f as H,m as t,q,w as E}from"./index.a97d6999.js";import"./vendor.9092e083.js";var g=S({name:"login",setup(){const{t:e}=V(),n=$(),o=B(),r=C({ruleForm:{userName:"admin",password:"123456",code:"1234"},loading:{signIn:!1}}),d=A(()=>j(new Date)),h=()=>{r.loading.signIn=!0;let s=[],l=[],m=["admin"],p=["btn.add","btn.del","btn.edit","btn.link"],f=["test"],i=["btn.add","btn.link"];r.ruleForm.userName==="admin"?(s=m,l=p):(s=f,l=i);const _={userName:r.ruleForm.userName,photo:r.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:s,authBtnList:l};F("token",Math.random().toString(36).substr(0)),F("userInfo",_),n.dispatch("userInfos/setUserInfos",_),n.state.themeConfig.themeConfig.isRequestRoutes?P(w=>{R(w,()=>{c()})}):(N(),c())},c=()=>{let s=d.value;o.push("/"),setTimeout(()=>{r.loading.signIn=!0;const l=e("message.signInText");T.success(`${s}\uFF0C${l}`)},300)};return v({currentTime:d,onSignIn:h},L(r))}}),M=`.login-content-form[data-v-1919aef7] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-1919aef7] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-1919aef7] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
  user-select: none;
}
.login-content-form .login-content-code .login-content-code-img[data-v-1919aef7]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-1919aef7] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const a=E();D("data-v-1919aef7");const K=t("div",{class:"login-content-code"},[t("span",{class:"login-content-code-img"},"1234")],-1);U();const Q=a((e,n,o,r,d,h)=>{const c=u("el-input"),s=u("el-form-item"),l=u("el-col"),m=u("el-row"),p=u("el-button"),f=u("el-form");return G(),H(f,{class:"login-content-form"},{default:a(()=>[t(s,null,{default:a(()=>[t(c,{type:"text",placeholder:e.$t("message.account.accountPlaceholder1"),"prefix-icon":"el-icon-user",modelValue:e.ruleForm.userName,"onUpdate:modelValue":n[1]||(n[1]=i=>e.ruleForm.userName=i),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(c,{type:"password",placeholder:e.$t("message.account.accountPlaceholder2"),"prefix-icon":"el-icon-lock",modelValue:e.ruleForm.password,"onUpdate:modelValue":n[2]||(n[2]=i=>e.ruleForm.password=i),autocomplete:"off","show-password":""},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(m,{gutter:15},{default:a(()=>[t(l,{span:16},{default:a(()=>[t(c,{type:"text",maxlength:"4",placeholder:e.$t("message.account.accountPlaceholder3"),"prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":n[3]||(n[3]=i=>e.ruleForm.code=i),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(l,{span:8},{default:a(()=>[K]),_:1})]),_:1})]),_:1}),t(s,null,{default:a(()=>[t(p,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:a(()=>[t("span",null,q(e.$t("message.account.accountBtnText")),1)]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});g.render=Q,g.__scopeId="data-v-1919aef7";export default g;
