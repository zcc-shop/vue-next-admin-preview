var v=Object.defineProperty;var h=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,g=Object.prototype.propertyIsEnumerable;var w=(n,a,t)=>a in n?v(n,a,{enumerable:!0,configurable:!0,writable:!0,value:t}):n[a]=t,c=(n,a)=>{for(var t in a||(a={}))h.call(a,t)&&w(n,t,a[t]);if(d)for(var t of d(a))g.call(a,t)&&w(n,t,a[t]);return n};import{i as y,t as _,p as x,l as b,r as k,e as r,f as o,m as l,F as p,s as m,q as u,w as I}from"./index.b9e7554b.js";import"./vendor.7fd510b4.js";var s={name:"pagesWaterfall",setup(){const n=y({});return c({},_(n))}},A=`.waterfall-container .waterfall-first[data-v-084d8e90] {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(188px, 1fr));
  grid-gap: 0.25em;
  grid-auto-flow: row dense;
  grid-auto-rows: 20px;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90] {
  width: 100%;
  background: var(--color-primary);
  color: #ffffff;
  transition: all 0.3s ease;
  border-radius: 3px;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90]:nth-of-type(3n + 1) {
  grid-row: auto/span 5;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90]:nth-of-type(3n + 2) {
  grid-row: auto/span 6;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90]:nth-of-type(3n + 3) {
  grid-row: auto/span 8;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease;
  cursor: pointer;
}
.waterfall-container .waterfall-first .waterfall-first-item[data-v-084d8e90]:active {
  opacity: 0.5;
}
.waterfall-container .waterfall-last[data-v-084d8e90] {
  display: grid;
  grid-gap: 0.25em;
  grid-auto-flow: row dense;
  grid-auto-rows: minmax(188px, 20vmin);
  grid-template-columns: 1fr;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90] {
  height: 100%;
  background: var(--color-primary);
  color: #ffffff;
  transition: all 0.3s ease;
  border-radius: 3px;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:hover {
  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease;
  cursor: pointer;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:active {
  opacity: 0.5;
}
@media (min-width: 576px) {
.waterfall-container .waterfall-last[data-v-084d8e90] {
    grid-template-columns: repeat(7, 1fr);
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 9) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 8) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 7) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 6) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 5) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 4) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 3) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 2) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(9n + 1) {
    grid-column: auto/span 2;
}
}
@media (min-width: 576px) and (min-width: 1024px) {
.waterfall-container .waterfall-last[data-v-084d8e90] {
    grid-template-columns: repeat(14, 1fr);
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 15) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 14) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 13) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 12) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 11) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 10) {
    grid-column: auto/span 2;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 9) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 8) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 7) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 6) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 5) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 4) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 3) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 2) {
    grid-column: auto/span 3;
}
.waterfall-container .waterfall-last .waterfall-last-item[data-v-084d8e90]:nth-of-type(15n + 1) {
    grid-column: auto/span 2;
}
}`;const i=I();x("data-v-084d8e90");const S={class:"waterfall-container"},$={class:"waterfall-first"},j={class:"w100 h100 flex"},B={class:"flex-margin"},F={class:"waterfall-last"},q={class:"w100 h100 flex"},C={class:"flex-margin"};b();const D=i((n,a,t,L,N,R)=>{const f=k("el-card");return r(),o("div",S,[l(f,{shadow:"hover",header:"\u7011\u5E03\u5C4F\uFF08\u5E03\u5C40\u4E00\uFF09",class:"mb15"},{default:i(()=>[l("div",$,[(r(),o(p,null,m(30,e=>l("div",{class:"waterfall-first-item",key:e},[l("div",j,[l("span",B,u(e),1)])])),64))])]),_:1}),l(f,{shadow:"hover",header:"\u7011\u5E03\u5C4F\uFF08\u5E03\u5C40\u4E8C\uFF09"},{default:i(()=>[l("div",F,[(r(),o(p,null,m(30,e=>l("div",{class:"waterfall-last-item",key:e},[l("div",q,[l("span",C,u(e),1)])])),64))])]),_:1})])});s.render=D,s.__scopeId="data-v-084d8e90";export default s;
