import{u as m}from"./index.3bc43889.js";import p from"./index.6e08cc18.js";import _ from"./tagsView.7122d143.js";import{z as u,p as f,d as l,e as a,f as r,h as c,i as h,E as w,q as v}from"./vendor.9b51066d.js";import"./breadcrumb.5faca1cc.js";import"./user.d75e227c.js";import"./screenfull.75a325ae.js";import"./userNews.d597db99.js";import"./search.19715788.js";import"./index.52b4627e.js";import"./horizontal.8cfc007d.js";import"./subItem.c2830d89.js";import"./contextmenu.fc2234cb.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=v();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=b((t,n,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),c("div",j,[h(i),e.setShowTagsView?(r(),c(d,{key:0})):w("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
