var bt=Object.assign;import{i as jt,t as Vt,n as Ne,p as $t,l as Gt,r as Z,e as Ft,f as Qt,m as x,w as qt,B as xt,A as Se}from"./index.9af39cde.js";import"./vendor.8f964570.js";/*!
 * Cropper.js v1.5.11
 * https://fengyuanchen.github.io/cropperjs
 *
 * Copyright 2015-present Chen Fengyuan
 * Released under the MIT license
 *
 * Date: 2021-02-17T11:53:27.572Z
 */function dt(a){return typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?dt=function(t){return typeof t}:dt=function(t){return t&&typeof Symbol=="function"&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},dt(a)}function Re(a,t){if(!(a instanceof t))throw new TypeError("Cannot call a class as a function")}function Zt(a,t){for(var i=0;i<t.length;i++){var e=t[i];e.enumerable=e.enumerable||!1,e.configurable=!0,"value"in e&&(e.writable=!0),Object.defineProperty(a,e.key,e)}}function Ie(a,t,i){return t&&Zt(a.prototype,t),i&&Zt(a,i),a}function ke(a,t,i){return t in a?Object.defineProperty(a,t,{value:i,enumerable:!0,configurable:!0,writable:!0}):a[t]=i,a}function Kt(a,t){var i=Object.keys(a);if(Object.getOwnPropertySymbols){var e=Object.getOwnPropertySymbols(a);t&&(e=e.filter(function(o){return Object.getOwnPropertyDescriptor(a,o).enumerable})),i.push.apply(i,e)}return i}function Jt(a){for(var t=1;t<arguments.length;t++){var i=arguments[t]!=null?arguments[t]:{};t%2?Kt(Object(i),!0).forEach(function(e){ke(a,e,i[e])}):Object.getOwnPropertyDescriptors?Object.defineProperties(a,Object.getOwnPropertyDescriptors(i)):Kt(Object(i)).forEach(function(e){Object.defineProperty(a,e,Object.getOwnPropertyDescriptor(i,e))})}return a}function te(a){return Be(a)||ze(a)||Le(a)||He()}function Be(a){if(Array.isArray(a))return yt(a)}function ze(a){if(typeof Symbol!="undefined"&&Symbol.iterator in Object(a))return Array.from(a)}function Le(a,t){if(!!a){if(typeof a=="string")return yt(a,t);var i=Object.prototype.toString.call(a).slice(8,-1);if(i==="Object"&&a.constructor&&(i=a.constructor.name),i==="Map"||i==="Set")return Array.from(a);if(i==="Arguments"||/^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(i))return yt(a,t)}}function yt(a,t){(t==null||t>a.length)&&(t=a.length);for(var i=0,e=new Array(t);i<t;i++)e[i]=a[i];return e}function He(){throw new TypeError(`Invalid attempt to spread non-iterable instance.
In order to be iterable, non-array objects must have a [Symbol.iterator]() method.`)}var ft=typeof window!="undefined"&&typeof window.document!="undefined",L=ft?window:{},Dt=ft&&L.document.documentElement?"ontouchstart"in L.document.documentElement:!1,Mt=ft?"PointerEvent"in L:!1,y="cropper",Et="all",ee="crop",ie="move",ae="zoom",F="e",Q="w",K="s",W="n",at="ne",rt="nw",nt="se",ot="sw",At="".concat(y,"-crop"),re="".concat(y,"-disabled"),N="".concat(y,"-hidden"),ne="".concat(y,"-hide"),Xe="".concat(y,"-invisible"),ut="".concat(y,"-modal"),Ct="".concat(y,"-move"),st="".concat(y,"Action"),gt="".concat(y,"Preview"),_t="crop",oe="move",se="none",Tt="crop",Ot="cropend",Nt="cropmove",St="cropstart",he="dblclick",Pe=Dt?"touchstart":"mousedown",Ye=Dt?"touchmove":"mousemove",We=Dt?"touchend touchcancel":"mouseup",ce=Mt?"pointerdown":Pe,pe=Mt?"pointermove":Ye,le=Mt?"pointerup pointercancel":We,de="ready",fe="resize",ue="wheel",Rt="zoom",ge="image/jpeg",Ue=/^e|w|s|n|se|sw|ne|nw|all|crop|move|zoom$/,je=/^data:/,Ve=/^data:image\/jpeg;base64,/,$e=/^img|canvas$/i,me=200,ve=100,we={viewMode:0,dragMode:_t,initialAspectRatio:NaN,aspectRatio:NaN,data:null,preview:"",responsive:!0,restore:!0,checkCrossOrigin:!0,checkOrientation:!0,modal:!0,guides:!0,center:!0,highlight:!0,background:!0,autoCrop:!0,autoCropArea:.8,movable:!0,rotatable:!0,scalable:!0,zoomable:!0,zoomOnTouch:!0,zoomOnWheel:!0,wheelZoomRatio:.1,cropBoxMovable:!0,cropBoxResizable:!0,toggleDragModeOnDblclick:!0,minCanvasWidth:0,minCanvasHeight:0,minCropBoxWidth:0,minCropBoxHeight:0,minContainerWidth:me,minContainerHeight:ve,ready:null,cropstart:null,cropmove:null,cropend:null,crop:null,zoom:null},Ge='<div class="cropper-container" touch-action="none"><div class="cropper-wrap-box"><div class="cropper-canvas"></div></div><div class="cropper-drag-box"></div><div class="cropper-crop-box"><span class="cropper-view-box"></span><span class="cropper-dashed dashed-h"></span><span class="cropper-dashed dashed-v"></span><span class="cropper-center"></span><span class="cropper-face"></span><span class="cropper-line line-e" data-cropper-action="e"></span><span class="cropper-line line-n" data-cropper-action="n"></span><span class="cropper-line line-w" data-cropper-action="w"></span><span class="cropper-line line-s" data-cropper-action="s"></span><span class="cropper-point point-e" data-cropper-action="e"></span><span class="cropper-point point-n" data-cropper-action="n"></span><span class="cropper-point point-w" data-cropper-action="w"></span><span class="cropper-point point-s" data-cropper-action="s"></span><span class="cropper-point point-ne" data-cropper-action="ne"></span><span class="cropper-point point-nw" data-cropper-action="nw"></span><span class="cropper-point point-sw" data-cropper-action="sw"></span><span class="cropper-point point-se" data-cropper-action="se"></span></div></div>',Fe=Number.isNaN||L.isNaN;function u(a){return typeof a=="number"&&!Fe(a)}var be=function(t){return t>0&&t<Infinity};function It(a){return typeof a=="undefined"}function q(a){return dt(a)==="object"&&a!==null}var Qe=Object.prototype.hasOwnProperty;function J(a){if(!q(a))return!1;try{var t=a.constructor,i=t.prototype;return t&&i&&Qe.call(i,"isPrototypeOf")}catch(e){return!1}}function S(a){return typeof a=="function"}var qe=Array.prototype.slice;function xe(a){return Array.from?Array.from(a):qe.call(a)}function E(a,t){return a&&S(t)&&(Array.isArray(a)||u(a.length)?xe(a).forEach(function(i,e){t.call(a,i,e,a)}):q(a)&&Object.keys(a).forEach(function(i){t.call(a,a[i],i,a)})),a}var D=Object.assign||function(t){for(var i=arguments.length,e=new Array(i>1?i-1:0),o=1;o<i;o++)e[o-1]=arguments[o];return q(t)&&e.length>0&&e.forEach(function(r){q(r)&&Object.keys(r).forEach(function(n){t[n]=r[n]})}),t},Ze=/\.\d*(?:0|9){12}\d*$/;function tt(a){var t=arguments.length>1&&arguments[1]!==void 0?arguments[1]:1e11;return Ze.test(a)?Math.round(a*t)/t:a}var Ke=/^width|height|left|top|marginLeft|marginTop$/;function U(a,t){var i=a.style;E(t,function(e,o){Ke.test(o)&&u(e)&&(e="".concat(e,"px")),i[o]=e})}function Je(a,t){return a.classList?a.classList.contains(t):a.className.indexOf(t)>-1}function T(a,t){if(!!t){if(u(a.length)){E(a,function(e){T(e,t)});return}if(a.classList){a.classList.add(t);return}var i=a.className.trim();i?i.indexOf(t)<0&&(a.className="".concat(i," ").concat(t)):a.className=t}}function H(a,t){if(!!t){if(u(a.length)){E(a,function(i){H(i,t)});return}if(a.classList){a.classList.remove(t);return}a.className.indexOf(t)>=0&&(a.className=a.className.replace(t,""))}}function et(a,t,i){if(!!t){if(u(a.length)){E(a,function(e){et(e,t,i)});return}i?T(a,t):H(a,t)}}var ti=/([a-z\d])([A-Z])/g;function kt(a){return a.replace(ti,"$1-$2").toLowerCase()}function Bt(a,t){return q(a[t])?a[t]:a.dataset?a.dataset[t]:a.getAttribute("data-".concat(kt(t)))}function ht(a,t,i){q(i)?a[t]=i:a.dataset?a.dataset[t]=i:a.setAttribute("data-".concat(kt(t)),i)}function ei(a,t){if(q(a[t]))try{delete a[t]}catch(i){a[t]=void 0}else if(a.dataset)try{delete a.dataset[t]}catch(i){a.dataset[t]=void 0}else a.removeAttribute("data-".concat(kt(t)))}var ye=/\s\s*/,De=function(){var a=!1;if(ft){var t=!1,i=function(){},e=Object.defineProperty({},"once",{get:function(){return a=!0,t},set:function(r){t=r}});L.addEventListener("test",i,e),L.removeEventListener("test",i,e)}return a}();function B(a,t,i){var e=arguments.length>3&&arguments[3]!==void 0?arguments[3]:{},o=i;t.trim().split(ye).forEach(function(r){if(!De){var n=a.listeners;n&&n[r]&&n[r][i]&&(o=n[r][i],delete n[r][i],Object.keys(n[r]).length===0&&delete n[r],Object.keys(n).length===0&&delete a.listeners)}a.removeEventListener(r,o,e)})}function k(a,t,i){var e=arguments.length>3&&arguments[3]!==void 0?arguments[3]:{},o=i;t.trim().split(ye).forEach(function(r){if(e.once&&!De){var n=a.listeners,s=n===void 0?{}:n;o=function(){delete s[r][i],a.removeEventListener(r,o,e);for(var p=arguments.length,c=new Array(p),h=0;h<p;h++)c[h]=arguments[h];i.apply(a,c)},s[r]||(s[r]={}),s[r][i]&&a.removeEventListener(r,s[r][i],e),s[r][i]=o,a.listeners=s}a.addEventListener(r,o,e)})}function it(a,t,i){var e;return S(Event)&&S(CustomEvent)?e=new CustomEvent(t,{detail:i,bubbles:!0,cancelable:!0}):(e=document.createEvent("CustomEvent"),e.initCustomEvent(t,!0,!0,i)),a.dispatchEvent(e)}function Me(a){var t=a.getBoundingClientRect();return{left:t.left+(window.pageXOffset-document.documentElement.clientLeft),top:t.top+(window.pageYOffset-document.documentElement.clientTop)}}var zt=L.location,ii=/^(\w+:)\/\/([^:/?#]*):?(\d*)/i;function Ee(a){var t=a.match(ii);return t!==null&&(t[1]!==zt.protocol||t[2]!==zt.hostname||t[3]!==zt.port)}function Ae(a){var t="timestamp=".concat(new Date().getTime());return a+(a.indexOf("?")===-1?"?":"&")+t}function ct(a){var t=a.rotate,i=a.scaleX,e=a.scaleY,o=a.translateX,r=a.translateY,n=[];u(o)&&o!==0&&n.push("translateX(".concat(o,"px)")),u(r)&&r!==0&&n.push("translateY(".concat(r,"px)")),u(t)&&t!==0&&n.push("rotate(".concat(t,"deg)")),u(i)&&i!==1&&n.push("scaleX(".concat(i,")")),u(e)&&e!==1&&n.push("scaleY(".concat(e,")"));var s=n.length?n.join(" "):"none";return{WebkitTransform:s,msTransform:s,transform:s}}function ai(a){var t=Jt({},a),i=0;return E(a,function(e,o){delete t[o],E(t,function(r){var n=Math.abs(e.startX-r.startX),s=Math.abs(e.startY-r.startY),l=Math.abs(e.endX-r.endX),p=Math.abs(e.endY-r.endY),c=Math.sqrt(n*n+s*s),h=Math.sqrt(l*l+p*p),d=(h-c)/c;Math.abs(d)>Math.abs(i)&&(i=d)})}),i}function mt(a,t){var i=a.pageX,e=a.pageY,o={endX:i,endY:e};return t?o:Jt({startX:i,startY:e},o)}function ri(a){var t=0,i=0,e=0;return E(a,function(o){var r=o.startX,n=o.startY;t+=r,i+=n,e+=1}),t/=e,i/=e,{pageX:t,pageY:i}}function j(a){var t=a.aspectRatio,i=a.height,e=a.width,o=arguments.length>1&&arguments[1]!==void 0?arguments[1]:"contain",r=be(e),n=be(i);if(r&&n){var s=i*t;o==="contain"&&s>e||o==="cover"&&s<e?i=e/t:e=i*t}else r?i=e/t:n&&(e=i*t);return{width:e,height:i}}function ni(a){var t=a.width,i=a.height,e=a.degree;if(e=Math.abs(e)%180,e===90)return{width:i,height:t};var o=e%90*Math.PI/180,r=Math.sin(o),n=Math.cos(o),s=t*n+i*r,l=t*r+i*n;return e>90?{width:l,height:s}:{width:s,height:l}}function oi(a,t,i,e){var o=t.aspectRatio,r=t.naturalWidth,n=t.naturalHeight,s=t.rotate,l=s===void 0?0:s,p=t.scaleX,c=p===void 0?1:p,h=t.scaleY,d=h===void 0?1:h,m=i.aspectRatio,g=i.naturalWidth,b=i.naturalHeight,v=e.fillColor,A=v===void 0?"transparent":v,_=e.imageSmoothingEnabled,M=_===void 0?!0:_,X=e.imageSmoothingQuality,R=X===void 0?"low":X,f=e.maxWidth,w=f===void 0?Infinity:f,C=e.maxHeight,I=C===void 0?Infinity:C,P=e.minWidth,V=P===void 0?0:P,$=e.minHeight,Y=$===void 0?0:$,z=document.createElement("canvas"),O=z.getContext("2d"),G=j({aspectRatio:m,width:w,height:I}),lt=j({aspectRatio:m,width:V,height:Y},"cover"),vt=Math.min(G.width,Math.max(lt.width,g)),wt=Math.min(G.height,Math.max(lt.height,b)),Pt=j({aspectRatio:o,width:w,height:I}),Yt=j({aspectRatio:o,width:V,height:Y},"cover"),Wt=Math.min(Pt.width,Math.max(Yt.width,r)),Ut=Math.min(Pt.height,Math.max(Yt.height,n)),Te=[-Wt/2,-Ut/2,Wt,Ut];return z.width=tt(vt),z.height=tt(wt),O.fillStyle=A,O.fillRect(0,0,vt,wt),O.save(),O.translate(vt/2,wt/2),O.rotate(l*Math.PI/180),O.scale(c,d),O.imageSmoothingEnabled=M,O.imageSmoothingQuality=R,O.drawImage.apply(O,[a].concat(te(Te.map(function(Oe){return Math.floor(tt(Oe))})))),O.restore(),z}var Ce=String.fromCharCode;function si(a,t,i){var e="";i+=t;for(var o=t;o<i;o+=1)e+=Ce(a.getUint8(o));return e}var hi=/^data:.*,/;function ci(a){var t=a.replace(hi,""),i=atob(t),e=new ArrayBuffer(i.length),o=new Uint8Array(e);return E(o,function(r,n){o[n]=i.charCodeAt(n)}),e}function pi(a,t){for(var i=[],e=8192,o=new Uint8Array(a);o.length>0;)i.push(Ce.apply(null,xe(o.subarray(0,e)))),o=o.subarray(e);return"data:".concat(t,";base64,").concat(btoa(i.join("")))}function li(a){var t=new DataView(a),i;try{var e,o,r;if(t.getUint8(0)===255&&t.getUint8(1)===216)for(var n=t.byteLength,s=2;s+1<n;){if(t.getUint8(s)===255&&t.getUint8(s+1)===225){o=s;break}s+=1}if(o){var l=o+4,p=o+10;if(si(t,l,4)==="Exif"){var c=t.getUint16(p);if(e=c===18761,(e||c===19789)&&t.getUint16(p+2,e)===42){var h=t.getUint32(p+4,e);h>=8&&(r=p+h)}}}if(r){var d=t.getUint16(r,e),m,g;for(g=0;g<d;g+=1)if(m=r+g*12+2,t.getUint16(m,e)===274){m+=8,i=t.getUint16(m,e),t.setUint16(m,1,e);break}}}catch(b){i=1}return i}function di(a){var t=0,i=1,e=1;switch(a){case 2:i=-1;break;case 3:t=-180;break;case 4:e=-1;break;case 5:t=90,e=-1;break;case 6:t=90;break;case 7:t=90,i=-1;break;case 8:t=-90;break}return{rotate:t,scaleX:i,scaleY:e}}var fi={render:function(){this.initContainer(),this.initCanvas(),this.initCropBox(),this.renderCanvas(),this.cropped&&this.renderCropBox()},initContainer:function(){var t=this.element,i=this.options,e=this.container,o=this.cropper,r=Number(i.minContainerWidth),n=Number(i.minContainerHeight);T(o,N),H(t,N);var s={width:Math.max(e.offsetWidth,r>=0?r:me),height:Math.max(e.offsetHeight,n>=0?n:ve)};this.containerData=s,U(o,{width:s.width,height:s.height}),T(t,N),H(o,N)},initCanvas:function(){var t=this.containerData,i=this.imageData,e=this.options.viewMode,o=Math.abs(i.rotate)%180==90,r=o?i.naturalHeight:i.naturalWidth,n=o?i.naturalWidth:i.naturalHeight,s=r/n,l=t.width,p=t.height;t.height*s>t.width?e===3?l=t.height*s:p=t.width/s:e===3?p=t.width/s:l=t.height*s;var c={aspectRatio:s,naturalWidth:r,naturalHeight:n,width:l,height:p};this.canvasData=c,this.limited=e===1||e===2,this.limitCanvas(!0,!0),c.width=Math.min(Math.max(c.width,c.minWidth),c.maxWidth),c.height=Math.min(Math.max(c.height,c.minHeight),c.maxHeight),c.left=(t.width-c.width)/2,c.top=(t.height-c.height)/2,c.oldLeft=c.left,c.oldTop=c.top,this.initialCanvasData=D({},c)},limitCanvas:function(t,i){var e=this.options,o=this.containerData,r=this.canvasData,n=this.cropBoxData,s=e.viewMode,l=r.aspectRatio,p=this.cropped&&n;if(t){var c=Number(e.minCanvasWidth)||0,h=Number(e.minCanvasHeight)||0;s>1?(c=Math.max(c,o.width),h=Math.max(h,o.height),s===3&&(h*l>c?c=h*l:h=c/l)):s>0&&(c?c=Math.max(c,p?n.width:0):h?h=Math.max(h,p?n.height:0):p&&(c=n.width,h=n.height,h*l>c?c=h*l:h=c/l));var d=j({aspectRatio:l,width:c,height:h});c=d.width,h=d.height,r.minWidth=c,r.minHeight=h,r.maxWidth=Infinity,r.maxHeight=Infinity}if(i)if(s>(p?0:1)){var m=o.width-r.width,g=o.height-r.height;r.minLeft=Math.min(0,m),r.minTop=Math.min(0,g),r.maxLeft=Math.max(0,m),r.maxTop=Math.max(0,g),p&&this.limited&&(r.minLeft=Math.min(n.left,n.left+(n.width-r.width)),r.minTop=Math.min(n.top,n.top+(n.height-r.height)),r.maxLeft=n.left,r.maxTop=n.top,s===2&&(r.width>=o.width&&(r.minLeft=Math.min(0,m),r.maxLeft=Math.max(0,m)),r.height>=o.height&&(r.minTop=Math.min(0,g),r.maxTop=Math.max(0,g))))}else r.minLeft=-r.width,r.minTop=-r.height,r.maxLeft=o.width,r.maxTop=o.height},renderCanvas:function(t,i){var e=this.canvasData,o=this.imageData;if(i){var r=ni({width:o.naturalWidth*Math.abs(o.scaleX||1),height:o.naturalHeight*Math.abs(o.scaleY||1),degree:o.rotate||0}),n=r.width,s=r.height,l=e.width*(n/e.naturalWidth),p=e.height*(s/e.naturalHeight);e.left-=(l-e.width)/2,e.top-=(p-e.height)/2,e.width=l,e.height=p,e.aspectRatio=n/s,e.naturalWidth=n,e.naturalHeight=s,this.limitCanvas(!0,!1)}(e.width>e.maxWidth||e.width<e.minWidth)&&(e.left=e.oldLeft),(e.height>e.maxHeight||e.height<e.minHeight)&&(e.top=e.oldTop),e.width=Math.min(Math.max(e.width,e.minWidth),e.maxWidth),e.height=Math.min(Math.max(e.height,e.minHeight),e.maxHeight),this.limitCanvas(!1,!0),e.left=Math.min(Math.max(e.left,e.minLeft),e.maxLeft),e.top=Math.min(Math.max(e.top,e.minTop),e.maxTop),e.oldLeft=e.left,e.oldTop=e.top,U(this.canvas,D({width:e.width,height:e.height},ct({translateX:e.left,translateY:e.top}))),this.renderImage(t),this.cropped&&this.limited&&this.limitCropBox(!0,!0)},renderImage:function(t){var i=this.canvasData,e=this.imageData,o=e.naturalWidth*(i.width/i.naturalWidth),r=e.naturalHeight*(i.height/i.naturalHeight);D(e,{width:o,height:r,left:(i.width-o)/2,top:(i.height-r)/2}),U(this.image,D({width:e.width,height:e.height},ct(D({translateX:e.left,translateY:e.top},e)))),t&&this.output()},initCropBox:function(){var t=this.options,i=this.canvasData,e=t.aspectRatio||t.initialAspectRatio,o=Number(t.autoCropArea)||.8,r={width:i.width,height:i.height};e&&(i.height*e>i.width?r.height=r.width/e:r.width=r.height*e),this.cropBoxData=r,this.limitCropBox(!0,!0),r.width=Math.min(Math.max(r.width,r.minWidth),r.maxWidth),r.height=Math.min(Math.max(r.height,r.minHeight),r.maxHeight),r.width=Math.max(r.minWidth,r.width*o),r.height=Math.max(r.minHeight,r.height*o),r.left=i.left+(i.width-r.width)/2,r.top=i.top+(i.height-r.height)/2,r.oldLeft=r.left,r.oldTop=r.top,this.initialCropBoxData=D({},r)},limitCropBox:function(t,i){var e=this.options,o=this.containerData,r=this.canvasData,n=this.cropBoxData,s=this.limited,l=e.aspectRatio;if(t){var p=Number(e.minCropBoxWidth)||0,c=Number(e.minCropBoxHeight)||0,h=s?Math.min(o.width,r.width,r.width+r.left,o.width-r.left):o.width,d=s?Math.min(o.height,r.height,r.height+r.top,o.height-r.top):o.height;p=Math.min(p,o.width),c=Math.min(c,o.height),l&&(p&&c?c*l>p?c=p/l:p=c*l:p?c=p/l:c&&(p=c*l),d*l>h?d=h/l:h=d*l),n.minWidth=Math.min(p,h),n.minHeight=Math.min(c,d),n.maxWidth=h,n.maxHeight=d}i&&(s?(n.minLeft=Math.max(0,r.left),n.minTop=Math.max(0,r.top),n.maxLeft=Math.min(o.width,r.left+r.width)-n.width,n.maxTop=Math.min(o.height,r.top+r.height)-n.height):(n.minLeft=0,n.minTop=0,n.maxLeft=o.width-n.width,n.maxTop=o.height-n.height))},renderCropBox:function(){var t=this.options,i=this.containerData,e=this.cropBoxData;(e.width>e.maxWidth||e.width<e.minWidth)&&(e.left=e.oldLeft),(e.height>e.maxHeight||e.height<e.minHeight)&&(e.top=e.oldTop),e.width=Math.min(Math.max(e.width,e.minWidth),e.maxWidth),e.height=Math.min(Math.max(e.height,e.minHeight),e.maxHeight),this.limitCropBox(!1,!0),e.left=Math.min(Math.max(e.left,e.minLeft),e.maxLeft),e.top=Math.min(Math.max(e.top,e.minTop),e.maxTop),e.oldLeft=e.left,e.oldTop=e.top,t.movable&&t.cropBoxMovable&&ht(this.face,st,e.width>=i.width&&e.height>=i.height?ie:Et),U(this.cropBox,D({width:e.width,height:e.height},ct({translateX:e.left,translateY:e.top}))),this.cropped&&this.limited&&this.limitCanvas(!0,!0),this.disabled||this.output()},output:function(){this.preview(),it(this.element,Tt,this.getData())}},ui={initPreview:function(){var t=this.element,i=this.crossOrigin,e=this.options.preview,o=i?this.crossOriginUrl:this.url,r=t.alt||"The image to preview",n=document.createElement("img");if(i&&(n.crossOrigin=i),n.src=o,n.alt=r,this.viewBox.appendChild(n),this.viewBoxImage=n,!!e){var s=e;typeof e=="string"?s=t.ownerDocument.querySelectorAll(e):e.querySelector&&(s=[e]),this.previews=s,E(s,function(l){var p=document.createElement("img");ht(l,gt,{width:l.offsetWidth,height:l.offsetHeight,html:l.innerHTML}),i&&(p.crossOrigin=i),p.src=o,p.alt=r,p.style.cssText='display:block;width:100%;height:auto;min-width:0!important;min-height:0!important;max-width:none!important;max-height:none!important;image-orientation:0deg!important;"',l.innerHTML="",l.appendChild(p)})}},resetPreview:function(){E(this.previews,function(t){var i=Bt(t,gt);U(t,{width:i.width,height:i.height}),t.innerHTML=i.html,ei(t,gt)})},preview:function(){var t=this.imageData,i=this.canvasData,e=this.cropBoxData,o=e.width,r=e.height,n=t.width,s=t.height,l=e.left-i.left-t.left,p=e.top-i.top-t.top;!this.cropped||this.disabled||(U(this.viewBoxImage,D({width:n,height:s},ct(D({translateX:-l,translateY:-p},t)))),E(this.previews,function(c){var h=Bt(c,gt),d=h.width,m=h.height,g=d,b=m,v=1;o&&(v=d/o,b=r*v),r&&b>m&&(v=m/r,g=o*v,b=m),U(c,{width:g,height:b}),U(c.getElementsByTagName("img")[0],D({width:n*v,height:s*v},ct(D({translateX:-l*v,translateY:-p*v},t))))}))}},gi={bind:function(){var t=this.element,i=this.options,e=this.cropper;S(i.cropstart)&&k(t,St,i.cropstart),S(i.cropmove)&&k(t,Nt,i.cropmove),S(i.cropend)&&k(t,Ot,i.cropend),S(i.crop)&&k(t,Tt,i.crop),S(i.zoom)&&k(t,Rt,i.zoom),k(e,ce,this.onCropStart=this.cropStart.bind(this)),i.zoomable&&i.zoomOnWheel&&k(e,ue,this.onWheel=this.wheel.bind(this),{passive:!1,capture:!0}),i.toggleDragModeOnDblclick&&k(e,he,this.onDblclick=this.dblclick.bind(this)),k(t.ownerDocument,pe,this.onCropMove=this.cropMove.bind(this)),k(t.ownerDocument,le,this.onCropEnd=this.cropEnd.bind(this)),i.responsive&&k(window,fe,this.onResize=this.resize.bind(this))},unbind:function(){var t=this.element,i=this.options,e=this.cropper;S(i.cropstart)&&B(t,St,i.cropstart),S(i.cropmove)&&B(t,Nt,i.cropmove),S(i.cropend)&&B(t,Ot,i.cropend),S(i.crop)&&B(t,Tt,i.crop),S(i.zoom)&&B(t,Rt,i.zoom),B(e,ce,this.onCropStart),i.zoomable&&i.zoomOnWheel&&B(e,ue,this.onWheel,{passive:!1,capture:!0}),i.toggleDragModeOnDblclick&&B(e,he,this.onDblclick),B(t.ownerDocument,pe,this.onCropMove),B(t.ownerDocument,le,this.onCropEnd),i.responsive&&B(window,fe,this.onResize)}},mi={resize:function(){if(!this.disabled){var t=this.options,i=this.container,e=this.containerData,o=i.offsetWidth/e.width;if(o!==1||i.offsetHeight!==e.height){var r,n;t.restore&&(r=this.getCanvasData(),n=this.getCropBoxData()),this.render(),t.restore&&(this.setCanvasData(E(r,function(s,l){r[l]=s*o})),this.setCropBoxData(E(n,function(s,l){n[l]=s*o})))}}},dblclick:function(){this.disabled||this.options.dragMode===se||this.setDragMode(Je(this.dragBox,At)?oe:_t)},wheel:function(t){var i=this,e=Number(this.options.wheelZoomRatio)||.1,o=1;this.disabled||(t.preventDefault(),!this.wheeling&&(this.wheeling=!0,setTimeout(function(){i.wheeling=!1},50),t.deltaY?o=t.deltaY>0?1:-1:t.wheelDelta?o=-t.wheelDelta/120:t.detail&&(o=t.detail>0?1:-1),this.zoom(-o*e,t)))},cropStart:function(t){var i=t.buttons,e=t.button;if(!(this.disabled||(t.type==="mousedown"||t.type==="pointerdown"&&t.pointerType==="mouse")&&(u(i)&&i!==1||u(e)&&e!==0||t.ctrlKey))){var o=this.options,r=this.pointers,n;t.changedTouches?E(t.changedTouches,function(s){r[s.identifier]=mt(s)}):r[t.pointerId||0]=mt(t),Object.keys(r).length>1&&o.zoomable&&o.zoomOnTouch?n=ae:n=Bt(t.target,st),!!Ue.test(n)&&it(this.element,St,{originalEvent:t,action:n})!==!1&&(t.preventDefault(),this.action=n,this.cropping=!1,n===ee&&(this.cropping=!0,T(this.dragBox,ut)))}},cropMove:function(t){var i=this.action;if(!(this.disabled||!i)){var e=this.pointers;t.preventDefault(),it(this.element,Nt,{originalEvent:t,action:i})!==!1&&(t.changedTouches?E(t.changedTouches,function(o){D(e[o.identifier]||{},mt(o,!0))}):D(e[t.pointerId||0]||{},mt(t,!0)),this.change(t))}},cropEnd:function(t){if(!this.disabled){var i=this.action,e=this.pointers;t.changedTouches?E(t.changedTouches,function(o){delete e[o.identifier]}):delete e[t.pointerId||0],!!i&&(t.preventDefault(),Object.keys(e).length||(this.action=""),this.cropping&&(this.cropping=!1,et(this.dragBox,ut,this.cropped&&this.options.modal)),it(this.element,Ot,{originalEvent:t,action:i}))}}},vi={change:function(t){var i=this.options,e=this.canvasData,o=this.containerData,r=this.cropBoxData,n=this.pointers,s=this.action,l=i.aspectRatio,p=r.left,c=r.top,h=r.width,d=r.height,m=p+h,g=c+d,b=0,v=0,A=o.width,_=o.height,M=!0,X;!l&&t.shiftKey&&(l=h&&d?h/d:1),this.limited&&(b=r.minLeft,v=r.minTop,A=b+Math.min(o.width,e.width,e.left+e.width),_=v+Math.min(o.height,e.height,e.top+e.height));var R=n[Object.keys(n)[0]],f={x:R.endX-R.startX,y:R.endY-R.startY},w=function(I){switch(I){case F:m+f.x>A&&(f.x=A-m);break;case Q:p+f.x<b&&(f.x=b-p);break;case W:c+f.y<v&&(f.y=v-c);break;case K:g+f.y>_&&(f.y=_-g);break}};switch(s){case Et:p+=f.x,c+=f.y;break;case F:if(f.x>=0&&(m>=A||l&&(c<=v||g>=_))){M=!1;break}w(F),h+=f.x,h<0&&(s=Q,h=-h,p-=h),l&&(d=h/l,c+=(r.height-d)/2);break;case W:if(f.y<=0&&(c<=v||l&&(p<=b||m>=A))){M=!1;break}w(W),d-=f.y,c+=f.y,d<0&&(s=K,d=-d,c-=d),l&&(h=d*l,p+=(r.width-h)/2);break;case Q:if(f.x<=0&&(p<=b||l&&(c<=v||g>=_))){M=!1;break}w(Q),h-=f.x,p+=f.x,h<0&&(s=F,h=-h,p-=h),l&&(d=h/l,c+=(r.height-d)/2);break;case K:if(f.y>=0&&(g>=_||l&&(p<=b||m>=A))){M=!1;break}w(K),d+=f.y,d<0&&(s=W,d=-d,c-=d),l&&(h=d*l,p+=(r.width-h)/2);break;case at:if(l){if(f.y<=0&&(c<=v||m>=A)){M=!1;break}w(W),d-=f.y,c+=f.y,h=d*l}else w(W),w(F),f.x>=0?m<A?h+=f.x:f.y<=0&&c<=v&&(M=!1):h+=f.x,f.y<=0?c>v&&(d-=f.y,c+=f.y):(d-=f.y,c+=f.y);h<0&&d<0?(s=ot,d=-d,h=-h,c-=d,p-=h):h<0?(s=rt,h=-h,p-=h):d<0&&(s=nt,d=-d,c-=d);break;case rt:if(l){if(f.y<=0&&(c<=v||p<=b)){M=!1;break}w(W),d-=f.y,c+=f.y,h=d*l,p+=r.width-h}else w(W),w(Q),f.x<=0?p>b?(h-=f.x,p+=f.x):f.y<=0&&c<=v&&(M=!1):(h-=f.x,p+=f.x),f.y<=0?c>v&&(d-=f.y,c+=f.y):(d-=f.y,c+=f.y);h<0&&d<0?(s=nt,d=-d,h=-h,c-=d,p-=h):h<0?(s=at,h=-h,p-=h):d<0&&(s=ot,d=-d,c-=d);break;case ot:if(l){if(f.x<=0&&(p<=b||g>=_)){M=!1;break}w(Q),h-=f.x,p+=f.x,d=h/l}else w(K),w(Q),f.x<=0?p>b?(h-=f.x,p+=f.x):f.y>=0&&g>=_&&(M=!1):(h-=f.x,p+=f.x),f.y>=0?g<_&&(d+=f.y):d+=f.y;h<0&&d<0?(s=at,d=-d,h=-h,c-=d,p-=h):h<0?(s=nt,h=-h,p-=h):d<0&&(s=rt,d=-d,c-=d);break;case nt:if(l){if(f.x>=0&&(m>=A||g>=_)){M=!1;break}w(F),h+=f.x,d=h/l}else w(K),w(F),f.x>=0?m<A?h+=f.x:f.y>=0&&g>=_&&(M=!1):h+=f.x,f.y>=0?g<_&&(d+=f.y):d+=f.y;h<0&&d<0?(s=rt,d=-d,h=-h,c-=d,p-=h):h<0?(s=ot,h=-h,p-=h):d<0&&(s=at,d=-d,c-=d);break;case ie:this.move(f.x,f.y),M=!1;break;case ae:this.zoom(ai(n),t),M=!1;break;case ee:if(!f.x||!f.y){M=!1;break}X=Me(this.cropper),p=R.startX-X.left,c=R.startY-X.top,h=r.minWidth,d=r.minHeight,f.x>0?s=f.y>0?nt:at:f.x<0&&(p-=h,s=f.y>0?ot:rt),f.y<0&&(c-=d),this.cropped||(H(this.cropBox,N),this.cropped=!0,this.limited&&this.limitCropBox(!0,!0));break}M&&(r.width=h,r.height=d,r.left=p,r.top=c,this.action=s,this.renderCropBox()),E(n,function(C){C.startX=C.endX,C.startY=C.endY})}},wi={crop:function(){return this.ready&&!this.cropped&&!this.disabled&&(this.cropped=!0,this.limitCropBox(!0,!0),this.options.modal&&T(this.dragBox,ut),H(this.cropBox,N),this.setCropBoxData(this.initialCropBoxData)),this},reset:function(){return this.ready&&!this.disabled&&(this.imageData=D({},this.initialImageData),this.canvasData=D({},this.initialCanvasData),this.cropBoxData=D({},this.initialCropBoxData),this.renderCanvas(),this.cropped&&this.renderCropBox()),this},clear:function(){return this.cropped&&!this.disabled&&(D(this.cropBoxData,{left:0,top:0,width:0,height:0}),this.cropped=!1,this.renderCropBox(),this.limitCanvas(!0,!0),this.renderCanvas(),H(this.dragBox,ut),T(this.cropBox,N)),this},replace:function(t){var i=arguments.length>1&&arguments[1]!==void 0?arguments[1]:!1;return!this.disabled&&t&&(this.isImg&&(this.element.src=t),i?(this.url=t,this.image.src=t,this.ready&&(this.viewBoxImage.src=t,E(this.previews,function(e){e.getElementsByTagName("img")[0].src=t}))):(this.isImg&&(this.replaced=!0),this.options.data=null,this.uncreate(),this.load(t))),this},enable:function(){return this.ready&&this.disabled&&(this.disabled=!1,H(this.cropper,re)),this},disable:function(){return this.ready&&!this.disabled&&(this.disabled=!0,T(this.cropper,re)),this},destroy:function(){var t=this.element;return t[y]?(t[y]=void 0,this.isImg&&this.replaced&&(t.src=this.originalUrl),this.uncreate(),this):this},move:function(t){var i=arguments.length>1&&arguments[1]!==void 0?arguments[1]:t,e=this.canvasData,o=e.left,r=e.top;return this.moveTo(It(t)?t:o+Number(t),It(i)?i:r+Number(i))},moveTo:function(t){var i=arguments.length>1&&arguments[1]!==void 0?arguments[1]:t,e=this.canvasData,o=!1;return t=Number(t),i=Number(i),this.ready&&!this.disabled&&this.options.movable&&(u(t)&&(e.left=t,o=!0),u(i)&&(e.top=i,o=!0),o&&this.renderCanvas(!0)),this},zoom:function(t,i){var e=this.canvasData;return t=Number(t),t<0?t=1/(1-t):t=1+t,this.zoomTo(e.width*t/e.naturalWidth,null,i)},zoomTo:function(t,i,e){var o=this.options,r=this.canvasData,n=r.width,s=r.height,l=r.naturalWidth,p=r.naturalHeight;if(t=Number(t),t>=0&&this.ready&&!this.disabled&&o.zoomable){var c=l*t,h=p*t;if(it(this.element,Rt,{ratio:t,oldRatio:n/l,originalEvent:e})===!1)return this;if(e){var d=this.pointers,m=Me(this.cropper),g=d&&Object.keys(d).length?ri(d):{pageX:e.pageX,pageY:e.pageY};r.left-=(c-n)*((g.pageX-m.left-r.left)/n),r.top-=(h-s)*((g.pageY-m.top-r.top)/s)}else J(i)&&u(i.x)&&u(i.y)?(r.left-=(c-n)*((i.x-r.left)/n),r.top-=(h-s)*((i.y-r.top)/s)):(r.left-=(c-n)/2,r.top-=(h-s)/2);r.width=c,r.height=h,this.renderCanvas(!0)}return this},rotate:function(t){return this.rotateTo((this.imageData.rotate||0)+Number(t))},rotateTo:function(t){return t=Number(t),u(t)&&this.ready&&!this.disabled&&this.options.rotatable&&(this.imageData.rotate=t%360,this.renderCanvas(!0,!0)),this},scaleX:function(t){var i=this.imageData.scaleY;return this.scale(t,u(i)?i:1)},scaleY:function(t){var i=this.imageData.scaleX;return this.scale(u(i)?i:1,t)},scale:function(t){var i=arguments.length>1&&arguments[1]!==void 0?arguments[1]:t,e=this.imageData,o=!1;return t=Number(t),i=Number(i),this.ready&&!this.disabled&&this.options.scalable&&(u(t)&&(e.scaleX=t,o=!0),u(i)&&(e.scaleY=i,o=!0),o&&this.renderCanvas(!0,!0)),this},getData:function(){var t=arguments.length>0&&arguments[0]!==void 0?arguments[0]:!1,i=this.options,e=this.imageData,o=this.canvasData,r=this.cropBoxData,n;if(this.ready&&this.cropped){n={x:r.left-o.left,y:r.top-o.top,width:r.width,height:r.height};var s=e.width/e.naturalWidth;if(E(n,function(c,h){n[h]=c/s}),t){var l=Math.round(n.y+n.height),p=Math.round(n.x+n.width);n.x=Math.round(n.x),n.y=Math.round(n.y),n.width=p-n.x,n.height=l-n.y}}else n={x:0,y:0,width:0,height:0};return i.rotatable&&(n.rotate=e.rotate||0),i.scalable&&(n.scaleX=e.scaleX||1,n.scaleY=e.scaleY||1),n},setData:function(t){var i=this.options,e=this.imageData,o=this.canvasData,r={};if(this.ready&&!this.disabled&&J(t)){var n=!1;i.rotatable&&u(t.rotate)&&t.rotate!==e.rotate&&(e.rotate=t.rotate,n=!0),i.scalable&&(u(t.scaleX)&&t.scaleX!==e.scaleX&&(e.scaleX=t.scaleX,n=!0),u(t.scaleY)&&t.scaleY!==e.scaleY&&(e.scaleY=t.scaleY,n=!0)),n&&this.renderCanvas(!0,!0);var s=e.width/e.naturalWidth;u(t.x)&&(r.left=t.x*s+o.left),u(t.y)&&(r.top=t.y*s+o.top),u(t.width)&&(r.width=t.width*s),u(t.height)&&(r.height=t.height*s),this.setCropBoxData(r)}return this},getContainerData:function(){return this.ready?D({},this.containerData):{}},getImageData:function(){return this.sized?D({},this.imageData):{}},getCanvasData:function(){var t=this.canvasData,i={};return this.ready&&E(["left","top","width","height","naturalWidth","naturalHeight"],function(e){i[e]=t[e]}),i},setCanvasData:function(t){var i=this.canvasData,e=i.aspectRatio;return this.ready&&!this.disabled&&J(t)&&(u(t.left)&&(i.left=t.left),u(t.top)&&(i.top=t.top),u(t.width)?(i.width=t.width,i.height=t.width/e):u(t.height)&&(i.height=t.height,i.width=t.height*e),this.renderCanvas(!0)),this},getCropBoxData:function(){var t=this.cropBoxData,i;return this.ready&&this.cropped&&(i={left:t.left,top:t.top,width:t.width,height:t.height}),i||{}},setCropBoxData:function(t){var i=this.cropBoxData,e=this.options.aspectRatio,o,r;return this.ready&&this.cropped&&!this.disabled&&J(t)&&(u(t.left)&&(i.left=t.left),u(t.top)&&(i.top=t.top),u(t.width)&&t.width!==i.width&&(o=!0,i.width=t.width),u(t.height)&&t.height!==i.height&&(r=!0,i.height=t.height),e&&(o?i.height=i.width/e:r&&(i.width=i.height*e)),this.renderCropBox()),this},getCroppedCanvas:function(){var t=arguments.length>0&&arguments[0]!==void 0?arguments[0]:{};if(!this.ready||!window.HTMLCanvasElement)return null;var i=this.canvasData,e=oi(this.image,this.imageData,i,t);if(!this.cropped)return e;var o=this.getData(),r=o.x,n=o.y,s=o.width,l=o.height,p=e.width/Math.floor(i.naturalWidth);p!==1&&(r*=p,n*=p,s*=p,l*=p);var c=s/l,h=j({aspectRatio:c,width:t.maxWidth||Infinity,height:t.maxHeight||Infinity}),d=j({aspectRatio:c,width:t.minWidth||0,height:t.minHeight||0},"cover"),m=j({aspectRatio:c,width:t.width||(p!==1?e.width:s),height:t.height||(p!==1?e.height:l)}),g=m.width,b=m.height;g=Math.min(h.width,Math.max(d.width,g)),b=Math.min(h.height,Math.max(d.height,b));var v=document.createElement("canvas"),A=v.getContext("2d");v.width=tt(g),v.height=tt(b),A.fillStyle=t.fillColor||"transparent",A.fillRect(0,0,g,b);var _=t.imageSmoothingEnabled,M=_===void 0?!0:_,X=t.imageSmoothingQuality;A.imageSmoothingEnabled=M,X&&(A.imageSmoothingQuality=X);var R=e.width,f=e.height,w=r,C=n,I,P,V,$,Y,z;w<=-s||w>R?(w=0,I=0,V=0,Y=0):w<=0?(V=-w,w=0,I=Math.min(R,s+w),Y=I):w<=R&&(V=0,I=Math.min(s,R-w),Y=I),I<=0||C<=-l||C>f?(C=0,P=0,$=0,z=0):C<=0?($=-C,C=0,P=Math.min(f,l+C),z=P):C<=f&&($=0,P=Math.min(l,f-C),z=P);var O=[w,C,I,P];if(Y>0&&z>0){var G=g/s;O.push(V*G,$*G,Y*G,z*G)}return A.drawImage.apply(A,[e].concat(te(O.map(function(lt){return Math.floor(tt(lt))})))),v},setAspectRatio:function(t){var i=this.options;return!this.disabled&&!It(t)&&(i.aspectRatio=Math.max(0,t)||NaN,this.ready&&(this.initCropBox(),this.cropped&&this.renderCropBox())),this},setDragMode:function(t){var i=this.options,e=this.dragBox,o=this.face;if(this.ready&&!this.disabled){var r=t===_t,n=i.movable&&t===oe;t=r||n?t:se,i.dragMode=t,ht(e,st,t),et(e,At,r),et(e,Ct,n),i.cropBoxMovable||(ht(o,st,t),et(o,At,r),et(o,Ct,n))}return this}},bi=L.Cropper,_e=function(){function a(t){var i=arguments.length>1&&arguments[1]!==void 0?arguments[1]:{};if(Re(this,a),!t||!$e.test(t.tagName))throw new Error("The first argument is required and must be an <img> or <canvas> element.");this.element=t,this.options=D({},we,J(i)&&i),this.cropped=!1,this.disabled=!1,this.pointers={},this.ready=!1,this.reloading=!1,this.replaced=!1,this.sized=!1,this.sizing=!1,this.init()}return Ie(a,[{key:"init",value:function(){var i=this.element,e=i.tagName.toLowerCase(),o;if(!i[y]){if(i[y]=this,e==="img"){if(this.isImg=!0,o=i.getAttribute("src")||"",this.originalUrl=o,!o)return;o=i.src}else e==="canvas"&&window.HTMLCanvasElement&&(o=i.toDataURL());this.load(o)}}},{key:"load",value:function(i){var e=this;if(!!i){this.url=i,this.imageData={};var o=this.element,r=this.options;if(!r.rotatable&&!r.scalable&&(r.checkOrientation=!1),!r.checkOrientation||!window.ArrayBuffer){this.clone();return}if(je.test(i)){Ve.test(i)?this.read(ci(i)):this.clone();return}var n=new XMLHttpRequest,s=this.clone.bind(this);this.reloading=!0,this.xhr=n,n.onabort=s,n.onerror=s,n.ontimeout=s,n.onprogress=function(){n.getResponseHeader("content-type")!==ge&&n.abort()},n.onload=function(){e.read(n.response)},n.onloadend=function(){e.reloading=!1,e.xhr=null},r.checkCrossOrigin&&Ee(i)&&o.crossOrigin&&(i=Ae(i)),n.open("GET",i,!0),n.responseType="arraybuffer",n.withCredentials=o.crossOrigin==="use-credentials",n.send()}}},{key:"read",value:function(i){var e=this.options,o=this.imageData,r=li(i),n=0,s=1,l=1;if(r>1){this.url=pi(i,ge);var p=di(r);n=p.rotate,s=p.scaleX,l=p.scaleY}e.rotatable&&(o.rotate=n),e.scalable&&(o.scaleX=s,o.scaleY=l),this.clone()}},{key:"clone",value:function(){var i=this.element,e=this.url,o=i.crossOrigin,r=e;this.options.checkCrossOrigin&&Ee(e)&&(o||(o="anonymous"),r=Ae(e)),this.crossOrigin=o,this.crossOriginUrl=r;var n=document.createElement("img");o&&(n.crossOrigin=o),n.src=r||e,n.alt=i.alt||"The image to crop",this.image=n,n.onload=this.start.bind(this),n.onerror=this.stop.bind(this),T(n,ne),i.parentNode.insertBefore(n,i.nextSibling)}},{key:"start",value:function(){var i=this,e=this.image;e.onload=null,e.onerror=null,this.sizing=!0;var o=L.navigator&&/(?:iPad|iPhone|iPod).*?AppleWebKit/i.test(L.navigator.userAgent),r=function(p,c){D(i.imageData,{naturalWidth:p,naturalHeight:c,aspectRatio:p/c}),i.initialImageData=D({},i.imageData),i.sizing=!1,i.sized=!0,i.build()};if(e.naturalWidth&&!o){r(e.naturalWidth,e.naturalHeight);return}var n=document.createElement("img"),s=document.body||document.documentElement;this.sizingImage=n,n.onload=function(){r(n.width,n.height),o||s.removeChild(n)},n.src=e.src,o||(n.style.cssText="left:0;max-height:none!important;max-width:none!important;min-height:0!important;min-width:0!important;opacity:0;position:absolute;top:0;z-index:-1;",s.appendChild(n))}},{key:"stop",value:function(){var i=this.image;i.onload=null,i.onerror=null,i.parentNode.removeChild(i),this.image=null}},{key:"build",value:function(){if(!(!this.sized||this.ready)){var i=this.element,e=this.options,o=this.image,r=i.parentNode,n=document.createElement("div");n.innerHTML=Ge;var s=n.querySelector(".".concat(y,"-container")),l=s.querySelector(".".concat(y,"-canvas")),p=s.querySelector(".".concat(y,"-drag-box")),c=s.querySelector(".".concat(y,"-crop-box")),h=c.querySelector(".".concat(y,"-face"));this.container=r,this.cropper=s,this.canvas=l,this.dragBox=p,this.cropBox=c,this.viewBox=s.querySelector(".".concat(y,"-view-box")),this.face=h,l.appendChild(o),T(i,N),r.insertBefore(s,i.nextSibling),this.isImg||H(o,ne),this.initPreview(),this.bind(),e.initialAspectRatio=Math.max(0,e.initialAspectRatio)||NaN,e.aspectRatio=Math.max(0,e.aspectRatio)||NaN,e.viewMode=Math.max(0,Math.min(3,Math.round(e.viewMode)))||0,T(c,N),e.guides||T(c.getElementsByClassName("".concat(y,"-dashed")),N),e.center||T(c.getElementsByClassName("".concat(y,"-center")),N),e.background&&T(s,"".concat(y,"-bg")),e.highlight||T(h,Xe),e.cropBoxMovable&&(T(h,Ct),ht(h,st,Et)),e.cropBoxResizable||(T(c.getElementsByClassName("".concat(y,"-line")),N),T(c.getElementsByClassName("".concat(y,"-point")),N)),this.render(),this.ready=!0,this.setDragMode(e.dragMode),e.autoCrop&&this.crop(),this.setData(e.data),S(e.ready)&&k(i,de,e.ready,{once:!0}),it(i,de)}}},{key:"unbuild",value:function(){!this.ready||(this.ready=!1,this.unbind(),this.resetPreview(),this.cropper.parentNode.removeChild(this.cropper),H(this.element,N))}},{key:"uncreate",value:function(){this.ready?(this.unbuild(),this.ready=!1,this.cropped=!1):this.sizing?(this.sizingImage.onload=null,this.sizing=!1,this.sized=!1):this.reloading?(this.xhr.onabort=null,this.xhr.abort()):this.image&&this.stop()}}],[{key:"noConflict",value:function(){return window.Cropper=bi,a}},{key:"setDefaults",value:function(i){D(we,J(i)&&i)}}]),a}();D(_e.prototype,fi,ui,gi,mi,vi,wi);var Wi=`/*!
 * Cropper.js v1.5.11
 * https://fengyuanchen.github.io/cropperjs
 *
 * Copyright 2015-present Chen Fengyuan
 * Released under the MIT license
 *
 * Date: 2021-02-17T11:53:21.992Z
 */

.cropper-container {
  direction: ltr;
  font-size: 0;
  line-height: 0;
  position: relative;
  -ms-touch-action: none;
  touch-action: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.cropper-container img {
  display: block;
  height: 100%;
  image-orientation: 0deg;
  max-height: none !important;
  max-width: none !important;
  min-height: 0 !important;
  min-width: 0 !important;
  width: 100%;
}

.cropper-wrap-box,
.cropper-canvas,
.cropper-drag-box,
.cropper-crop-box,
.cropper-modal {
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
}

.cropper-wrap-box,
.cropper-canvas {
  overflow: hidden;
}

.cropper-drag-box {
  background-color: #fff;
  opacity: 0;
}

.cropper-modal {
  background-color: #000;
  opacity: 0.5;
}

.cropper-view-box {
  display: block;
  height: 100%;
  outline: 1px solid #39f;
  outline-color: rgba(51, 153, 255, 0.75);
  overflow: hidden;
  width: 100%;
}

.cropper-dashed {
  border: 0 dashed #eee;
  display: block;
  opacity: 0.5;
  position: absolute;
}

.cropper-dashed.dashed-h {
  border-bottom-width: 1px;
  border-top-width: 1px;
  height: calc(100% / 3);
  left: 0;
  top: calc(100% / 3);
  width: 100%;
}

.cropper-dashed.dashed-v {
  border-left-width: 1px;
  border-right-width: 1px;
  height: 100%;
  left: calc(100% / 3);
  top: 0;
  width: calc(100% / 3);
}

.cropper-center {
  display: block;
  height: 0;
  left: 50%;
  opacity: 0.75;
  position: absolute;
  top: 50%;
  width: 0;
}

.cropper-center::before,
.cropper-center::after {
  background-color: #eee;
  content: ' ';
  display: block;
  position: absolute;
}

.cropper-center::before {
  height: 1px;
  left: -3px;
  top: 0;
  width: 7px;
}

.cropper-center::after {
  height: 7px;
  left: 0;
  top: -3px;
  width: 1px;
}

.cropper-face,
.cropper-line,
.cropper-point {
  display: block;
  height: 100%;
  opacity: 0.1;
  position: absolute;
  width: 100%;
}

.cropper-face {
  background-color: #fff;
  left: 0;
  top: 0;
}

.cropper-line {
  background-color: #39f;
}

.cropper-line.line-e {
  cursor: ew-resize;
  right: -3px;
  top: 0;
  width: 5px;
}

.cropper-line.line-n {
  cursor: ns-resize;
  height: 5px;
  left: 0;
  top: -3px;
}

.cropper-line.line-w {
  cursor: ew-resize;
  left: -3px;
  top: 0;
  width: 5px;
}

.cropper-line.line-s {
  bottom: -3px;
  cursor: ns-resize;
  height: 5px;
  left: 0;
}

.cropper-point {
  background-color: #39f;
  height: 5px;
  opacity: 0.75;
  width: 5px;
}

.cropper-point.point-e {
  cursor: ew-resize;
  margin-top: -3px;
  right: -3px;
  top: 50%;
}

.cropper-point.point-n {
  cursor: ns-resize;
  left: 50%;
  margin-left: -3px;
  top: -3px;
}

.cropper-point.point-w {
  cursor: ew-resize;
  left: -3px;
  margin-top: -3px;
  top: 50%;
}

.cropper-point.point-s {
  bottom: -3px;
  cursor: s-resize;
  left: 50%;
  margin-left: -3px;
}

.cropper-point.point-ne {
  cursor: nesw-resize;
  right: -3px;
  top: -3px;
}

.cropper-point.point-nw {
  cursor: nwse-resize;
  left: -3px;
  top: -3px;
}

.cropper-point.point-sw {
  bottom: -3px;
  cursor: nesw-resize;
  left: -3px;
}

.cropper-point.point-se {
  bottom: -3px;
  cursor: nwse-resize;
  height: 20px;
  opacity: 1;
  right: -3px;
  width: 20px;
}

@media (min-width: 768px) {
  .cropper-point.point-se {
    height: 15px;
    width: 15px;
  }
}

@media (min-width: 992px) {
  .cropper-point.point-se {
    height: 10px;
    width: 10px;
  }
}

@media (min-width: 1200px) {
  .cropper-point.point-se {
    height: 5px;
    opacity: 0.75;
    width: 5px;
  }
}

.cropper-point.point-se::before {
  background-color: #39f;
  bottom: -50%;
  content: ' ';
  display: block;
  height: 200%;
  opacity: 0;
  position: absolute;
  right: -50%;
  width: 200%;
}

.cropper-invisible {
  opacity: 0;
}

.cropper-bg {
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAAA3NCSVQICAjb4U/gAAAABlBMVEXMzMz////TjRV2AAAACXBIWXMAAArrAAAK6wGCiw1aAAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M26LyyjAAAABFJREFUCJlj+M/AgBVhF/0PAH6/D/HkDxOGAAAAAElFTkSuQmCC');
}

.cropper-hide {
  display: block;
  height: 0;
  position: absolute;
  width: 0;
}

.cropper-hidden {
  display: none !important;
}

.cropper-move {
  cursor: move;
}

.cropper-crop {
  cursor: crosshair;
}

.cropper-disabled .cropper-drag-box,
.cropper-disabled .cropper-face,
.cropper-disabled .cropper-line,
.cropper-disabled .cropper-point {
  cursor: not-allowed;
}
`,Lt={name:"cropperIndex",setup(){const a=jt({isShowDialog:!1,cropperImg:"",cropperImgBase64:""}),t=n=>{a.cropperImg=n,a.isShowDialog=!0,Ne(()=>{r()})},i=()=>{a.isShowDialog=!1},e=()=>{i()},o=()=>{},r=()=>{const n=document.querySelector(".cropper-warp-left-img"),s=new _e(n,{viewMode:1,dragMode:"none",initialAspectRatio:1,aspectRatio:1,preview:".before",background:!1,autoCropArea:.6,zoomOnWheel:!1,crop:()=>{a.cropperImgBase64=s.getCroppedCanvas().toDataURL("image/jpeg")}})};return bt({openDialog:t,closeDialog:i,onCancel:e,onSubmit:o,initCropper:r},Vt(a))}},Ui=`.cropper-warp[data-v-148484b5] {
  display: flex;
}
.cropper-warp .cropper-warp-left[data-v-148484b5] {
  position: relative;
  display: inline-block;
  height: 350px;
  flex: 1;
  border: 1px solid #ebeef5;
  background: #fff;
  overflow: hidden;
  background-repeat: no-repeat;
  cursor: move;
  border-radius: 3px;
}
.cropper-warp .cropper-warp-left .cropper-warp-left-img[data-v-148484b5] {
  width: 100%;
  height: 100%;
}
.cropper-warp .cropper-warp-right[data-v-148484b5] {
  width: 150px;
  height: 350px;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-title[data-v-148484b5] {
  text-align: center;
  height: 20px;
  line-height: 20px;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-item[data-v-148484b5] {
  margin: 15px 0;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-item .cropper-warp-right-value[data-v-148484b5] {
  display: flex;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-item .cropper-warp-right-value .cropper-warp-right-value-img[data-v-148484b5] {
  width: 100px;
  height: 100px;
  border-radius: 100%;
  margin: auto;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-item .cropper-warp-right-value .cropper-size[data-v-148484b5] {
  width: 50px;
  height: 50px;
}
.cropper-warp .cropper-warp-right .cropper-warp-right-item .cropper-warp-right-label[data-v-148484b5] {
  text-align: center;
  font-size: 12px;
  color: #666666;
  height: 30px;
  line-height: 30px;
}`;const pt=qt("data-v-148484b5");$t("data-v-148484b5");const xi={class:"cropper-warp"},yi={class:"cropper-warp-left"},Di={class:"cropper-warp-right"},Mi=x("div",{class:"cropper-warp-right-title"},"\u9884\u89C8",-1),Ei={class:"cropper-warp-right-item"},Ai={class:"cropper-warp-right-value"},Ci=x("div",{class:"cropper-warp-right-label"},"100 x 100",-1),_i={class:"cropper-warp-right-item"},Ti={class:"cropper-warp-right-value"},Oi=x("div",{class:"cropper-warp-right-label"},"50 x 50",-1),Ni={class:"dialog-footer"},Si=xt("\u53D6 \u6D88"),Ri=xt("\u66F4 \u6362");Gt();const Ii=pt((a,t,i,e,o,r)=>{const n=Z("el-button"),s=Z("el-dialog");return Ft(),Qt("div",null,[x(s,{title:"\u66F4\u6362\u5934\u50CF",modelValue:a.isShowDialog,"onUpdate:modelValue":t[1]||(t[1]=l=>a.isShowDialog=l),width:"769px"},{footer:pt(()=>[x("span",Ni,[x(n,{onClick:e.onCancel,size:"small"},{default:pt(()=>[Si]),_:1},8,["onClick"]),x(n,{type:"primary",onClick:e.onSubmit,size:"small"},{default:pt(()=>[Ri]),_:1},8,["onClick"])])]),default:pt(()=>[x("div",xi,[x("div",yi,[x("img",{src:a.cropperImg,class:"cropper-warp-left-img"},null,8,["src"])]),x("div",Di,[Mi,x("div",Ei,[x("div",Ai,[x("img",{src:a.cropperImgBase64,class:"cropper-warp-right-value-img"},null,8,["src"])]),Ci]),x("div",_i,[x("div",Ti,[x("img",{src:a.cropperImgBase64,class:"cropper-warp-right-value-img cropper-size"},null,8,["src"])]),Oi])])])]),_:1},8,["modelValue"])])});Lt.render=Ii,Lt.__scopeId="data-v-148484b5";var Ht={name:"cropper",components:{CropperDialog:Lt},setup(){const a=Se(),t=jt({cropperImg:"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg"});return bt({cropperDialogRef:a,onCropperDialogOpen:()=>{a.value.openDialog(t.cropperImg)}},Vt(t))}},ji=`.croppers-container .cropper-img-warp[data-v-0d9f5939] {
  text-align: center;
}
.croppers-container .cropper-img-warp .cropper-img[data-v-0d9f5939] {
  margin: auto;
  width: 150px;
  height: 150px;
  border-radius: 100%;
}`;const Xt=qt("data-v-0d9f5939");$t("data-v-0d9f5939");const ki={class:"croppers-container"},Bi={class:"cropper-img-warp"},zi={class:"mb15 mt15"},Li=xt("\u66F4\u6362\u5934\u50CF");Gt();const Hi=Xt((a,t,i,e,o,r)=>{const n=Z("el-alert"),s=Z("el-button"),l=Z("el-card"),p=Z("CropperDialog");return Ft(),Qt("div",ki,[x(l,{shadow:"hover",header:"cropper \u56FE\u7247\u88C1\u526A"},{default:Xt(()=>[x(n,{title:"\u611F\u8C22\u4F18\u79C0\u7684 `cropperjs`\uFF0C\u9879\u76EE\u5730\u5740\uFF1Ahttps://github.com/fengyuanchen/cropperjs",type:"success",closable:!1,class:"mb15"}),x("div",Bi,[x("div",zi,[x("img",{class:"cropper-img",src:a.cropperImg},null,8,["src"])]),x(s,{type:"primary",icon:"el-icon-crop",size:"small",onClick:e.onCropperDialogOpen},{default:Xt(()=>[Li]),_:1},8,["onClick"])])]),_:1}),x(p,{ref:"cropperDialogRef"},null,512)])});Ht.render=Hi,Ht.__scopeId="data-v-0d9f5939";export default Ht;
