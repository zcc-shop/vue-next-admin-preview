var I=Object.defineProperty;var L=Object.prototype.hasOwnProperty;var C=Object.getOwnPropertySymbols,T=Object.prototype.propertyIsEnumerable;var v=(t,e,r)=>e in t?I(t,e,{enumerable:!0,configurable:!0,writable:!0,value:r}):t[e]=r,_=(t,e)=>{for(var r in e||(e={}))L.call(e,r)&&v(t,r,e[r]);if(C)for(var r of C(e))T.call(e,r)&&v(t,r,e[r]);return t};import{u as w,x as F,R,i as M,c as N,k as V,b5 as j,t as z,b as D,p as U,l as $,r as y,Q as q,U as G,e as c,f as u,m as p,bb as Q,F as A,s as E,h as S,C as k,q as B,ba as H,w as J}from"./index.ef4a75e8.js";import"./vendor.5429c63c.js";var f={name:"layoutBreadcrumb",setup(){const{proxy:t}=D(),e=w(),r=F(),n=R(),a=M({breadcrumbList:[],routeSplit:[],routeSplitFirst:"",routeSplitIndex:1}),h=N(()=>e.state.themeConfig.themeConfig),d=s=>{const{redirect:i,path:g}=s;i?n.push(i):n.push(g)},m=()=>{t.mittBus.emit("onMenuClick"),e.state.themeConfig.themeConfig.isCollapse=!e.state.themeConfig.themeConfig.isCollapse},o=s=>{s.map(i=>{a.routeSplit.map((g,W,x)=>{a.routeSplitFirst===i.path&&(a.routeSplitFirst+=`/${x[a.routeSplitIndex]}`,a.breadcrumbList.push(i),a.routeSplitIndex++,i.children&&o(i.children))})})},l=s=>{if(!e.state.themeConfig.themeConfig.isBreadcrumb)return!1;a.breadcrumbList=[e.state.routesList.routesList[0]],a.routeSplit=s.split("/"),a.routeSplit.shift(),a.routeSplitFirst=`/${a.routeSplit[0]}`,a.routeSplitIndex=1,o(e.state.routesList.routesList)};return V(()=>{l(r.path)}),j(s=>{l(s.path)}),_({onThemeConfigChange:m,getThemeConfig:h,onBreadcrumbClick:d},z(a))}},ee=`.layout-navbars-breadcrumb[data-v-0e98bce0] {
  flex: 1;
  height: inherit;
  display: flex;
  align-items: center;
  padding-left: 15px;
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-icon[data-v-0e98bce0] {
  cursor: pointer;
  font-size: 18px;
  margin-right: 15px;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-span[data-v-0e98bce0] {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb .layout-navbars-breadcrumb-iconfont[data-v-0e98bce0] {
  font-size: 14px;
  margin-right: 5px;
}
.layout-navbars-breadcrumb[data-v-0e98bce0] .el-breadcrumb__separator {
  opacity: 0.7;
  color: var(--bg-topBarColor);
}`;const b=J();U("data-v-0e98bce0");const K={class:"layout-navbars-breadcrumb"},O={key:0,class:"layout-navbars-breadcrumb-span"};$();const P=b((t,e,r,n,a,h)=>{const d=y("el-breadcrumb-item"),m=y("el-breadcrumb");return q((c(),u("div",K,[p("i",{class:["layout-navbars-breadcrumb-icon",n.getThemeConfig.isCollapse?"el-icon-s-unfold":"el-icon-s-fold"],onClick:e[1]||(e[1]=(...o)=>n.onThemeConfigChange&&n.onThemeConfigChange(...o))},null,2),p(m,{class:"layout-navbars-breadcrumb-hide"},{default:b(()=>[p(Q,{name:"breadcrumb",mode:"out-in"},{default:b(()=>[(c(!0),u(A,null,E(t.breadcrumbList,(o,l)=>(c(),u(d,{key:o.meta.title},{default:b(()=>[l===t.breadcrumbList.length-1?(c(),u("span",O,[n.getThemeConfig.isBreadcrumbIcon?(c(),u("i",{key:0,class:[o.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):S("",!0),k(B(t.$t(o.meta.title)),1)])):(c(),u("a",{key:1,onClick:H(s=>n.onBreadcrumbClick(o),["prevent"])},[n.getThemeConfig.isBreadcrumbIcon?(c(),u("i",{key:0,class:[o.meta.icon,"layout-navbars-breadcrumb-iconfont"]},null,2)):S("",!0),k(B(t.$t(o.meta.title)),1)],8,["onClick"]))]),_:2},1024))),128))]),_:1})]),_:1})],512)),[[G,n.getThemeConfig.isBreadcrumb]])});f.render=P,f.__scopeId="data-v-0e98bce0";export default f;
