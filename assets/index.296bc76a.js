import{u as m}from"./index.89b93c97.js";import p from"./index.bfddc7d9.js";import _ from"./tagsView.98e942be.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as b,E as h,q as w}from"./vendor.f07206bb.js";import"./breadcrumb.c20fb10c.js";import"./user.0cb37820.js";import"./screenfull.e049c9c4.js";import"./userNews.90ee0029.js";import"./search.5e4ece8d.js";import"./index.5fafe8ae.js";import"./horizontal.be96dac2.js";import"./subItem.084b6175.js";import"./contextmenu.2380e8f0.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[b(i),e.setShowTagsView?(r(),n(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
