import{u as m}from"./index.ab445e4e.js";import p from"./index.b1f52a23.js";import _ from"./tagsView.39f3c5e7.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as h,E as w,q as b}from"./vendor.312d0526.js";import"./breadcrumb.58406c3b.js";import"./user.29eec9fb.js";import"./screenfull.23c35388.js";import"./userNews.6ff81046.js";import"./search.f7cca7e9.js";import"./index.0bd0d548.js";import"./horizontal.9972acd4.js";import"./subItem.f77ea040.js";import"./contextmenu.582f95ba.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=b();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
