var w=Object.defineProperty;var x=Object.prototype.hasOwnProperty;var b=Object.getOwnPropertySymbols,y=Object.prototype.propertyIsEnumerable;var v=(e,n,o)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,I=(e,n)=>{for(var o in n||(n={}))x.call(n,o)&&v(e,o,n[o]);if(b)for(var o of b(n))y.call(n,o)&&v(e,o,n[o]);return e};import{d as k,u as S,b as V,s as F,aB as $,aC as A,v as B,j as C}from"./index.d9a617f7.js";import{d as L,r as N,c as P,t as T,l as j,m as R,q as u,y as D,z as U,A as t,D as H,F as q}from"./vendor.3f76c63b.js";var f=L({name:"login",setup(){const{t:e}=k(),n=S(),o=V(),i=N({ruleForm:{userName:"admin",password:"123456",code:"1234"},loading:{signIn:!1}}),d=P(()=>C(new Date)),h=async()=>{i.loading.signIn=!0;let s=[],l=[],m=["admin"],p=["btn.add","btn.del","btn.edit","btn.link"],g=["test"],r=["btn.add","btn.link"];i.ruleForm.userName==="admin"?(s=m,l=p):(s=g,l=r);const _={userName:i.ruleForm.userName,photo:i.ruleForm.userName==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:s,authBtnList:l};F("token",Math.random().toString(36).substr(0)),F("userInfo",_),n.dispatch("userInfos/setUserInfos",_),n.state.themeConfig.themeConfig.isRequestRoutes?(await A(),c()):(await $(),c())},c=()=>{let s=d.value;o.push("/"),setTimeout(()=>{i.loading.signIn=!0;const l=e("message.signInText");B.success(`${s}\uFF0C${l}`)},300)};return I({currentTime:d,onSignIn:h},T(i))}}),Y=`.login-content-form[data-v-44b3c7e9] {
  margin-top: 20px;
}
.login-content-form .login-content-code[data-v-44b3c7e9] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-44b3c7e9] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
  user-select: none;
}
.login-content-form .login-content-code .login-content-code-img[data-v-44b3c7e9]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-44b3c7e9] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const a=q();j("data-v-44b3c7e9");const z=t("div",{class:"login-content-code"},[t("span",{class:"login-content-code-img"},"1234")],-1);R();const G=a((e,n,o,i,d,h)=>{const c=u("el-input"),s=u("el-form-item"),l=u("el-col"),m=u("el-row"),p=u("el-button"),g=u("el-form");return D(),U(g,{class:"login-content-form"},{default:a(()=>[t(s,null,{default:a(()=>[t(c,{type:"text",placeholder:e.$t("message.account.accountPlaceholder1"),"prefix-icon":"el-icon-user",modelValue:e.ruleForm.userName,"onUpdate:modelValue":n[1]||(n[1]=r=>e.ruleForm.userName=r),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(c,{type:"password",placeholder:e.$t("message.account.accountPlaceholder2"),"prefix-icon":"el-icon-lock",modelValue:e.ruleForm.password,"onUpdate:modelValue":n[2]||(n[2]=r=>e.ruleForm.password=r),autocomplete:"off","show-password":""},null,8,["placeholder","modelValue"])]),_:1}),t(s,null,{default:a(()=>[t(m,{gutter:15},{default:a(()=>[t(l,{span:16},{default:a(()=>[t(c,{type:"text",maxlength:"4",placeholder:e.$t("message.account.accountPlaceholder3"),"prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":n[3]||(n[3]=r=>e.ruleForm.code=r),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(l,{span:8},{default:a(()=>[z]),_:1})]),_:1})]),_:1}),t(s,null,{default:a(()=>[t(p,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:a(()=>[t("span",null,H(e.$t("message.account.accountBtnText")),1)]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});f.render=G,f.__scopeId="data-v-44b3c7e9";export default f;
