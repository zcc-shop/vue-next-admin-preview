var w=Object.assign;import{Q as k,u as x,A as B,i as N,c as C,t as U,M as S,N as F,a$ as I,C as D,b as E,p as A,l as T,r,e as P,f as j,m as e,T as L,P as R,S as O,B as c,q as H,w as M}from"./index.0859f0ef.js";import{s as y}from"./screenfull.77def122.js";import q from"./userNews.aa3e041f.js";import V from"./search.1d737a16.js";import"./vendor.8f964570.js";var p={name:"layoutBreadcrumbUser",components:{UserNews:q,Search:V},setup(){const{proxy:t}=E(),s=k(),m=x(),o=B(),d=N({isScreenfull:!1,isShowUserNewsPopover:!1}),b=C(()=>{let{isBreadcrumb:i,layout:u,isClassicSplitMenu:n}=m.state.themeConfig.themeConfig;return!i&&u!=="transverse"&&n?1:""}),f=C(()=>m.state.userInfos.userInfos);return w({setFlexAutoStyle:b,getUserInfos:f,onLayoutSetingClick:()=>{t.mittBus.emit("openSetingsDrawer")},onHandleCommandClick:i=>{i==="logOut"?F({closeOnClickModal:!1,closeOnPressEscape:!1,title:"\u63D0\u793A",message:"\u6B64\u64CD\u4F5C\u5C06\u9000\u51FA\u767B\u5F55, \u662F\u5426\u7EE7\u7EED?",showCancelButton:!0,confirmButtonText:"\u786E\u5B9A",cancelButtonText:"\u53D6\u6D88",beforeClose:(u,n,g)=>{u==="confirm"?(n.confirmButtonLoading=!0,n.confirmButtonText="\u9000\u51FA\u4E2D",setTimeout(()=>{g(),setTimeout(()=>{n.confirmButtonLoading=!1},300)},700)):g()}}).then(()=>{I(),D(),s.push("/login"),setTimeout(()=>{S.success("\u5B89\u5168\u9000\u51FA\u6210\u529F\uFF01")},300)}).catch(()=>{}):s.push(i)},onScreenfullClick:()=>{if(!y.isEnabled)return S.warning("\u6682\u4E0D\u4E0D\u652F\u6301\u5168\u5C4F"),!1;y.toggle(),d.isScreenfull=!d.isScreenfull},onSearchClick:()=>{o.value.openSearch()},searchRef:o},U(d))}},ie=`.layout-navbars-breadcrumb-user[data-v-6a277cf8] {
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.layout-navbars-breadcrumb-user-link[data-v-6a277cf8] {
  height: 100%;
  display: flex;
  align-items: center;
  white-space: nowrap;
}
.layout-navbars-breadcrumb-user-link-photo[data-v-6a277cf8] {
  width: 25px;
  height: 25px;
  border-radius: 100%;
}
.layout-navbars-breadcrumb-user-icon[data-v-6a277cf8] {
  padding: 0 10px;
  cursor: pointer;
  color: var(--bg-topBarColor);
  height: 50px;
  line-height: 50px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user-icon[data-v-6a277cf8]:hover {
  background: rgba(0, 0, 0, 0.04);
}
.layout-navbars-breadcrumb-user-icon:hover i[data-v-6a277cf8] {
  display: inline-block;
  animation: logoAnimation 0.3s ease-in-out;
}
.layout-navbars-breadcrumb-user[data-v-6a277cf8] .el-dropdown {
  color: var(--bg-topBarColor);
}
.layout-navbars-breadcrumb-user[data-v-6a277cf8] .el-badge {
  height: 40px;
  line-height: 40px;
  display: flex;
  align-items: center;
}
.layout-navbars-breadcrumb-user[data-v-6a277cf8] .el-badge__content.is-fixed {
  top: 12px;
}`;const a=M("data-v-6a277cf8");A("data-v-6a277cf8");const z=e("i",{class:"el-icon-search",title:"\u83DC\u5355\u641C\u7D22"},null,-1),Q=e("i",{class:"icon-skin iconfont",title:"\u5E03\u5C40\u914D\u7F6E"},null,-1),G={class:"layout-navbars-breadcrumb-user-icon"},J=e("i",{class:"el-icon-bell",title:"\u6D88\u606F"},null,-1),K={class:"layout-navbars-breadcrumb-user-link"},W=e("i",{class:"el-icon-arrow-down el-icon--right"},null,-1),X=c("\u9996\u9875"),Y=c("\u4E2A\u4EBA\u4E2D\u5FC3"),Z=c("404"),$=c("401"),ee=c("\u9000\u51FA\u767B\u5F55");T();const oe=a((t,s,m,o,d,b)=>{const f=r("el-badge"),v=r("UserNews"),_=r("el-popover"),l=r("el-dropdown-item"),h=r("el-dropdown-menu"),i=r("el-dropdown"),u=r("Search");return P(),j("div",{class:"layout-navbars-breadcrumb-user",style:{flex:o.setFlexAutoStyle}},[e("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[1]||(s[1]=(...n)=>o.onSearchClick&&o.onSearchClick(...n))},[z]),e("div",{class:"layout-navbars-breadcrumb-user-icon",onClick:s[2]||(s[2]=(...n)=>o.onLayoutSetingClick&&o.onLayoutSetingClick(...n))},[Q]),e("div",G,[e(_,{placement:"bottom",trigger:"click",visible:t.isShowUserNewsPopover,"onUpdate:visible":s[4]||(s[4]=n=>t.isShowUserNewsPopover=n),width:300,"popper-class":"el-popover-pupop-user-news"},{reference:a(()=>[e(f,{"is-dot":!0,onClick:s[3]||(s[3]=n=>t.isShowUserNewsPopover=!t.isShowUserNewsPopover)},{default:a(()=>[J]),_:1})]),default:a(()=>[e(L,{name:"el-zoom-in-top"},{default:a(()=>[R(e(v,null,null,512),[[O,t.isShowUserNewsPopover]])]),_:1})]),_:1},8,["visible"])]),e("div",{class:"layout-navbars-breadcrumb-user-icon mr10",onClick:s[5]||(s[5]=(...n)=>o.onScreenfullClick&&o.onScreenfullClick(...n))},[e("i",{class:["iconfont",t.isScreenfull?"icon-tuichuquanping":"icon-fullscreen"],title:t.isScreenfull?"\u5F00\u5168\u5C4F":"\u5173\u5168\u5C4F"},null,10,["title"])]),e(i,{"show-timeout":70,"hide-timeout":50,onCommand:o.onHandleCommandClick},{dropdown:a(()=>[e(h,null,{default:a(()=>[e(l,{command:"/home"},{default:a(()=>[X]),_:1}),e(l,{command:"/personal"},{default:a(()=>[Y]),_:1}),e(l,{command:"/404"},{default:a(()=>[Z]),_:1}),e(l,{command:"/401"},{default:a(()=>[$]),_:1}),e(l,{divided:"",command:"logOut"},{default:a(()=>[ee]),_:1})]),_:1})]),default:a(()=>[e("span",K,[e("img",{src:o.getUserInfos.photo,class:"layout-navbars-breadcrumb-user-link-photo mr5"},null,8,["src"]),c(" "+H(o.getUserInfos.userName===""?"test":o.getUserInfos.userName)+" ",1),W])]),_:1},8,["onCommand"]),e(u,{ref:"searchRef"},null,512)],4)});p.render=oe,p.__scopeId="data-v-6a277cf8";export default p;
