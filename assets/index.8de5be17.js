import{u as m,c as p,p as _,l as u,r as a,e as r,f as n,m as f,h as l,w as h}from"./index.51953939.js";import w from"./index.71869cd8.js";import v from"./tagsView.025872f5.js";import"./vendor.8f964570.js";import"./breadcrumb.11e40736.js";import"./user.0eea21ee.js";import"./screenfull.77def122.js";import"./userNews.9beb1bef.js";import"./search.e1453379.js";import"./index.2e95f263.js";import"./horizontal.46545f42.js";import"./subItem.a5a5d723.js";import"./contextmenu.9a7c8c37.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:w,TagsView:v},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=h("data-v-fde5c75e");_("data-v-fde5c75e");const x={class:"layout-navbars-container"};u();const g=j((t,c,o,e,b,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=g,s.__scopeId="data-v-fde5c75e";export default s;
