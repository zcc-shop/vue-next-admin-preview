var c=Object.assign;import{i as m,t as v,p as x,l as y,r as _,e as t,f as o,m as a,h as d,F as w,s as F,q as u,w as h}from"./index.51953939.js";import"./vendor.8f964570.js";var i={name:"layoutBreadcrumbUserNews",setup(){const e=m({newsList:[{label:"\u5173\u4E8E\u7248\u672C\u53D1\u5E03\u7684\u901A\u77E5",value:"vue-next-admin\uFF0C\u57FA\u4E8E vue3 + CompositionAPI + typescript + vite + element plus\uFF0C\u6B63\u5F0F\u53D1\u5E03\u65F6\u95F4\uFF1A2021\u5E7402\u670828\u65E5\uFF01",time:"2020-12-08"},{label:"\u5173\u4E8E\u5B66\u4E60\u4EA4\u6D41\u7684\u901A\u77E5",value:"QQ\u7FA4\u53F7\u7801 665452019\uFF0C\u6B22\u8FCE\u5C0F\u4F19\u4F34\u5165\u7FA4\u5B66\u4E60\u4EA4\u6D41\u63A2\u8BA8\uFF01",time:"2020-12-08"}]});return c({onAllReadClick:()=>{e.newsList=[]},onGoToGiteeClick:()=>{window.open("https://gitee.com/lyt-top/vue-next-admin")}},v(e))}},j=`.layout-navbars-breadcrumb-user-news .head-box[data-v-6269eeac] {
  display: flex;
  border-bottom: 1px solid #ebeef5;
  box-sizing: border-box;
  color: #333333;
  justify-content: space-between;
  height: 35px;
  align-items: center;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6269eeac] {
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
}
.layout-navbars-breadcrumb-user-news .head-box .head-box-btn[data-v-6269eeac]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news .content-box[data-v-6269eeac] {
  font-size: 13px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6269eeac] {
  padding-top: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item[data-v-6269eeac]:last-of-type {
  padding-bottom: 12px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-msg[data-v-6269eeac] {
  color: #999999;
  margin-top: 5px;
  margin-bottom: 5px;
}
.layout-navbars-breadcrumb-user-news .content-box .content-box-item .content-box-time[data-v-6269eeac] {
  color: #999999;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6269eeac] {
  height: 35px;
  color: var(--color-primary);
  font-size: 13px;
  cursor: pointer;
  opacity: 0.8;
  display: flex;
  align-items: center;
  justify-content: center;
  border-top: 1px solid #ebeef5;
}
.layout-navbars-breadcrumb-user-news .foot-box[data-v-6269eeac]:hover {
  opacity: 1;
}
.layout-navbars-breadcrumb-user-news[data-v-6269eeac] .el-empty__description p {
  font-size: 13px;
}`;const f=h("data-v-6269eeac");x("data-v-6269eeac");const C={class:"layout-navbars-breadcrumb-user-news"},g={class:"head-box"},k=a("div",{class:"head-box-title"},"\u901A\u77E5",-1),E={class:"content-box"},A={class:"content-box-msg"},B={class:"content-box-time"};y();const G=f((e,s,l,r,L,I)=>{const b=_("el-empty");return t(),o("div",C,[a("div",g,[k,e.newsList.length>0?(t(),o("div",{key:0,class:"head-box-btn",onClick:s[1]||(s[1]=(...n)=>r.onAllReadClick&&r.onAllReadClick(...n))},"\u5168\u90E8\u5DF2\u8BFB")):d("",!0)]),a("div",E,[e.newsList.length>0?(t(!0),o(w,{key:0},F(e.newsList,(n,p)=>(t(),o("div",{class:"content-box-item",key:p},[a("div",null,u(n.label),1),a("div",A,u(n.value),1),a("div",B,u(n.time),1)]))),128)):(t(),o(b,{key:1,description:"\u6682\u65E0\u901A\u77E5"}))]),e.newsList.length>0?(t(),o("div",{key:0,class:"foot-box",onClick:s[2]||(s[2]=(...n)=>r.onGoToGiteeClick&&r.onGoToGiteeClick(...n))},"\u524D\u5F80\u901A\u77E5\u4E2D\u5FC3")):d("",!0)])});i.render=G,i.__scopeId="data-v-6269eeac";export default i;
