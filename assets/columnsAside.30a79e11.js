var w=Object.defineProperty;var B=Object.prototype.hasOwnProperty;var v=Object.getOwnPropertySymbols,M=Object.prototype.propertyIsEnumerable;var S=(e,n,o)=>n in e?w(e,n,{enumerable:!0,configurable:!0,writable:!0,value:o}):e[n]=o,h=(e,n)=>{for(var o in n||(n={}))B.call(n,o)&&S(e,o,n[o]);if(v)for(var o of v(n))M.call(n,o)&&S(e,o,n[o]);return e};import{u as D,a as F,i as T,aA as j}from"./index.85d2a956.js";import{a as k,r as z,c as O,w as H,j as V,t as q,n as N,k as P,l as U,m as X,q as E,y as f,z as p,A as d,L as G,V as J,D as L,F as K}from"./vendor.69bd2edd.js";var C={name:"layoutColumnsAside",setup(){const e=k([]),n=k(),{proxy:o}=P(),r=D(),A=F(),y=T(),a=z({columnsAsideList:[],liIndex:0,difference:0,routeSplit:[]}),s=O(()=>r.state.themeConfig.themeConfig.columnsAsideStyle),c=t=>{a.liIndex=t,n.value.style.top=`${e.value[t].offsetTop+a.difference}px`},m=(t,l)=>{c(l);let{path:u,redirect:i}=t;i?y.push(i):y.push(u)},g=t=>{N(()=>{c(t)})},b=()=>{a.columnsAsideList=x(r.state.routesList.routesList);const t=_(A.path);g(t.item[0].k),o.mittBus.emit("setSendColumnsChildren",t)},_=t=>{const l=t.split("/");let u={};return a.columnsAsideList.map((i,I)=>{i.path===`/${l[1]}`&&(i.k=I,u.item=[h({},i)],u.children=[h({},i)],i.children&&(u.children=i.children))}),u},x=t=>t.filter(l=>!l.meta.isHide).map(l=>(l=Object.assign({},l),l.children&&(l.children=x(l.children)),l)),R=t=>{a.routeSplit=t.split("/"),a.routeSplit.shift();const l=`/${a.routeSplit[0]}`,u=a.columnsAsideList.find(i=>i.path===l);setTimeout(()=>{g(u.k)},0)};return H(r.state,t=>{if(t.themeConfig.themeConfig.columnsAsideStyle==="columnsRound"?a.difference=3:a.difference=0,t.routesList.routesList.length===a.columnsAsideList.length)return!1;b()}),V(()=>{b()}),j(t=>{R(t.path),o.mittBus.emit("setSendColumnsChildren",_(t.path))}),h({columnsAsideOffsetTopRefs:e,columnsAsideActiveRef:n,onColumnsAsideDown:g,setColumnsAsideStyle:s,onColumnsAsideMenuClick:m},q(a))}},ae=`.layout-columns-aside[data-v-65e2fa8d] {
  width: 64px;
  height: 100%;
  background: var(--bg-columnsMenuBar);
}
.layout-columns-aside ul[data-v-65e2fa8d] {
  position: relative;
}
.layout-columns-aside ul li[data-v-65e2fa8d] {
  color: var(--bg-columnsMenuBarColor);
  width: 100%;
  height: 50px;
  text-align: center;
  display: flex;
  cursor: pointer;
  position: relative;
  z-index: 1;
}
.layout-columns-aside ul li .layout-columns-aside-li-box[data-v-65e2fa8d] {
  margin: auto;
}
.layout-columns-aside ul li .layout-columns-aside-li-box .layout-columns-aside-li-box-title[data-v-65e2fa8d] {
  padding-top: 1px;
}
.layout-columns-aside ul li a[data-v-65e2fa8d] {
  text-decoration: none;
  color: var(--bg-columnsMenuBarColor);
}
.layout-columns-aside ul .layout-columns-active[data-v-65e2fa8d] {
  color: #ffffff;
  transition: 0.3s ease-in-out;
}
.layout-columns-aside ul .columns-round[data-v-65e2fa8d], .layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  background: var(--color-primary);
  color: #ffffff;
  position: absolute;
  left: 50%;
  top: 2px;
  height: 44px;
  width: 58px;
  transform: translateX(-50%);
  z-index: 0;
  transition: 0.3s ease-in-out;
  border-radius: 5px;
}
.layout-columns-aside ul .columns-card[data-v-65e2fa8d] {
  top: 0;
  height: 50px;
  width: 100%;
  border-radius: 0;
}`;const $=K();U("data-v-65e2fa8d");const Q={class:"layout-columns-aside"},W={key:0,class:"layout-columns-aside-li-box"},Y={class:"layout-columns-aside-li-box-title font12"},Z={key:1,class:"layout-columns-aside-li-box"},ee={class:"layout-columns-aside-li-box-title font12"};X();const te=$((e,n,o,r,A,y)=>{const a=E("el-scrollbar");return f(),p("div",Q,[d(a,null,{default:$(()=>[d("ul",null,[(f(!0),p(G,null,J(e.columnsAsideList,(s,c)=>(f(),p("li",{key:c,onClick:m=>r.onColumnsAsideMenuClick(s,c),ref:m=>{m&&(r.columnsAsideOffsetTopRefs[c]=m)},class:{"layout-columns-active":e.liIndex===c},title:e.$t(s.meta.title)},[!s.meta.isLink||s.meta.isLink&&s.meta.isIframe?(f(),p("div",W,[d("i",{class:s.meta.icon},null,2),d("div",Y,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)])):(f(),p("div",Z,[d("a",{href:s.meta.isLink,target:"_blank"},[d("i",{class:s.meta.icon},null,2),d("div",ee,L(e.$t(s.meta.title)&&e.$t(s.meta.title).length>=4?e.$t(s.meta.title).substr(0,4):e.$t(s.meta.title)),1)],8,["href"])]))],10,["onClick","title"]))),128)),d("div",{ref:"columnsAsideActiveRef",class:r.setColumnsAsideStyle},null,2)])]),_:1})])});C.render=te,C.__scopeId="data-v-65e2fa8d";export default C;
