import{u as m}from"./index.1414326c.js";import p from"./index.4a6da87c.js";import _ from"./tagsView.cc9d4533.js";import{z as u,p as f,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.9b51066d.js";import"./breadcrumb.c7521a5c.js";import"./user.5b78ff1a.js";import"./screenfull.75a325ae.js";import"./userNews.d597db99.js";import"./search.7e7f310e.js";import"./index.1f228d44.js";import"./horizontal.ce7466b5.js";import"./subItem.c2830d89.js";import"./contextmenu.fc2234cb.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const b=v();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=b((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
