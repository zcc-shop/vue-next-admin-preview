import{u as m}from"./index.581c0394.js";import p from"./index.909ea3f2.js";import f from"./tagsView.eb1cf162.js";import{z as _,p as u,d as l,e as a,f as r,h as n,i as h,E as w,q as v}from"./vendor.a5c5c942.js";import"./breadcrumb.b0494726.js";import"./user.c573e927.js";import"./screenfull.52ff4b74.js";import"./userNews.41103212.js";import"./search.f0f67e2f.js";import"./index.556ed2d3.js";import"./horizontal.f3686bfe.js";import"./subItem.ef5a92f9.js";import"./contextmenu.31d06cd8.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:f},setup(){const t=m();return{setShowTagsView:_(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},D=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const j=v();u("data-v-fde5c75e");const x={class:"layout-navbars-container"};l();const b=j((t,c,o,e,g,I)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",x,[h(i),e.setShowTagsView?(r(),n(d,{key:0})):w("",!0)])});s.render=b,s.__scopeId="data-v-fde5c75e";export default s;
