var m=Object.defineProperty;var u=Object.prototype.hasOwnProperty;var d=Object.getOwnPropertySymbols,b=Object.prototype.propertyIsEnumerable;var f=(e,o,n)=>o in e?m(e,o,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[o]=n,p=(e,o)=>{for(var n in o||(o={}))u.call(o,n)&&f(e,n,o[n]);if(d)for(var n of d(o))b.call(o,n)&&f(e,n,o[n]);return e};import{i as x}from"./getStyleSheets.48bc15f2.js";import{a as g,o as y,t as I,p as S,d as L,e as s,f as r,h as c,i as a,F as $,Y as k,l as j,q as B}from"./vendor.9b51066d.js";var i={name:"pagesAwesome",setup(){const e=g({sheetsIconList:[]}),o=()=>{x.awe().then(n=>e.sheetsIconList=n)};return y(()=>{o()}),p({},I(e))}},E=`.awesome-container .iconfont-row[data-v-dc474fae] {
  border-top: 1px solid #ebeef5;
  border-left: 1px solid #ebeef5;
}
.awesome-container .iconfont-row .el-col[data-v-dc474fae]:nth-child(-n+24) {
  display: none;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-dc474fae] {
  text-align: center;
  border-right: 1px solid #ebeef5;
  border-bottom: 1px solid #ebeef5;
  height: 120px;
  overflow: hidden;
  display: flex;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp[data-v-dc474fae]:hover {
  box-shadow: 0 2px 12px rgba(0, 0, 0, 0.1);
  cursor: pointer;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-value i[data-v-dc474fae] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp:hover .iconfont-warp-label[data-v-dc474fae] {
  color: var(--color-primary);
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-value i[data-v-dc474fae] {
  color: #606266;
  font-size: 32px;
  transition: all 0.3s ease;
}
.awesome-container .iconfont-row .iconfont-warp .iconfont-warp-label[data-v-dc474fae] {
  color: #99a9bf;
  transition: all 0.3s ease;
}`;const t=B();S("data-v-dc474fae");const F={class:"awesome-container"},q={class:"iconfont-warp"},z={class:"flex-margin"},A={class:"iconfont-warp-value"},C={class:"iconfont-warp-label mt10"};L();const D=t((e,o,n,G,M,N)=>{const w=s("el-col"),_=s("el-row"),h=s("el-card");return r(),c("div",F,[a(h,{shadow:"hover",header:`fontawesome \u5B57\u4F53\u56FE\u6807(\u81EA\u52A8\u8F7D\u5165)\uFF1A${e.sheetsIconList.length-24}\u4E2A`},{default:t(()=>[a(_,{class:"iconfont-row"},{default:t(()=>[(r(!0),c($,null,k(e.sheetsIconList,(l,v)=>(r(),c(w,{xs:12,sm:8,md:6,lg:4,xl:2,key:v},{default:t(()=>[a("div",q,[a("div",z,[a("div",A,[a("i",{class:[l,"fa"]},null,2)]),a("div",C,j(l),1)])])]),_:2},1024))),128))]),_:1})]),_:1},8,["header"])])});i.render=D,i.__scopeId="data-v-dc474fae";export default i;
