import{u as m}from"./index.0e1be16b.js";import p from"./index.14ad5e19.js";import _ from"./tagsView.4bd52dee.js";import{c as u,l as f,m as l,q as a,y as r,z as n,A as b,M as h,F as w}from"./vendor.c148b95a.js";import"./breadcrumb.f532370f.js";import"./user.1692b117.js";import"./screenfull.6c125c6a.js";import"./userNews.9b77f65d.js";import"./search.d6a138ec.js";import"./index.4fd5e9f1.js";import"./horizontal.9c5e36b8.js";import"./subItem.e85630f3.js";import"./contextmenu.7142e51b.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:p,TagsView:_},setup(){const t=m();return{setShowTagsView:u(()=>{let{layout:o,isTagsview:e}=t.state.themeConfig.themeConfig;return o!=="classic"&&e})}}},M=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=w();f("data-v-fde5c75e");const j={class:"layout-navbars-container"};l();const x=v((t,c,o,e,g,y)=>{const i=a("BreadcrumbIndex"),d=a("TagsView");return r(),n("div",j,[b(i),e.setShowTagsView?(r(),n(d,{key:0})):h("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
