import{u as m,c as p,p as _,l as u,r as o,e as r,f as n,m as f,h as l,w as b}from"./index.a57f978d.js";import h from"./index.a8f153a3.js";import w from"./tagsView.3297be48.js";import"./vendor.8f964570.js";import"./breadcrumb.9f7cdba4.js";import"./user.eb5ae140.js";import"./screenfull.77def122.js";import"./userNews.b31f20bb.js";import"./search.717a34d5.js";import"./index.695a5b98.js";import"./horizontal.606fee94.js";import"./subItem.7c3ced08.js";import"./contextmenu.ebad7063.js";var s={name:"layoutNavBars",components:{BreadcrumbIndex:h,TagsView:w},setup(){const t=m();return{setShowTagsView:p(()=>{let{layout:a,isTagsview:e}=t.state.themeConfig.themeConfig;return a!=="classic"&&e})}}},E=`.layout-navbars-container[data-v-fde5c75e] {
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
}`;const v=b("data-v-fde5c75e");_("data-v-fde5c75e");const j={class:"layout-navbars-container"};u();const x=v((t,c,a,e,g,I)=>{const i=o("BreadcrumbIndex"),d=o("TagsView");return r(),n("div",j,[f(i),e.setShowTagsView?(r(),n(d,{key:0})):l("",!0)])});s.render=x,s.__scopeId="data-v-fde5c75e";export default s;
